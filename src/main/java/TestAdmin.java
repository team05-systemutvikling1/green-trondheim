package main.java;

import main.java.data.Admin;
import main.java.db.AdminDAO;

import java.sql.SQLException;

public class TestAdmin {
    public static void main(String[] args) throws SQLException {
        // Testing for Admin and AdminDAO
        AdminDAO adminDAO = new AdminDAO();
        /*Admin a = new Admin("admin", "tilfeldigPassord", "tilfeldigSalt");
        Admin magne = new Admin("magne", "passord");
        Admin uncompleteAdmin = new Admin("navn", "");
        System.out.println(adminDAO.getAdmin("admin"));
        System.out.println("addAdmin(): " +
                            adminDAO.addAdmin(magne) + " " +
                            adminDAO.addAdmin(uncompleteAdmin));
        System.out.println("setPassword(): " + adminDAO.setPassword("magne", "passord123"));
        System.out.println("isPasswordCorrect(): " +
                            adminDAO.isPasswordCorrect("magne", "passord123") + " " +
                            adminDAO.isPasswordCorrect("magne", "passord"));
        for (Admin admin : adminDAO.getAllAdmins()) {
            System.out.println(admin);
        }
        System.out.println("delAdmin(): " + adminDAO.delAdmin("magne"));
        System.out.println("countAdmins(): " + adminDAO.countAdmins());
        System.out.println("delAdmin(): " + adminDAO.delAdmin("admin"));
        System.out.println("equals(): " + a.equals(a) + " " + a.equals(magne));*/

        // Used for adding the default admin account to the database
        //System.out.println(adminDAO.addAdminWithoutMail(new Admin("admin", "admin")));

        // Adding other users:
        //System.out.println(adminDAO.addAdmin("magneworren@gmail.com"));
        //System.out.println(adminDAO.addAdmin("janmariv@stud.ntnu.no"));
        //System.out.println(adminDAO.addAdmin("kevinah@stud.ntnu.no"));
        //System.out.println(adminDAO.addAdmin("runevedo@stud.ntnu.no"));
        //System.out.println(adminDAO.addAdmin("orjanbv@stud.ntnu.no"));
        System.out.println(adminDAO.isPasswordCorrect("admin", "admin"));
        System.out.println(adminDAO.isPasswordCorrect("ikkeAdmin", "admin"));
    }
}
