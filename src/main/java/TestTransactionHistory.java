package main.java;

import main.java.data.TransactionHistory;
import main.java.db.TransactionHistoryDAO;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class TestTransactionHistory {
    public static void main(String[] args) throws SQLException {

        // Test for TransactionHistory and TransactionHistoryDAO
        TransactionHistoryDAO transactionHistoryDAO = new TransactionHistoryDAO();
        TransactionHistory tH = new TransactionHistory(24129712345L, LocalDateTime.of(LocalDate.of(2018,2,20),LocalTime.of(19,0)), 11.31);
        TransactionHistory tH2 = new TransactionHistory(24129712345L,LocalDateTime.of(LocalDate.of(2018,2,20),LocalTime.of(21,0)),1234);

        System.out.println("\nTransactionHistory and TransactionHistoryDAO tests:");
        System.out.println(transactionHistoryDAO.getTransactionHistory(1));
        System.out.println("--------------------------------");
        System.out.println("addTransactionHistory(): " + transactionHistoryDAO.addTransactionHistory(tH));
        System.out.println(transactionHistoryDAO.getTransactionHistory(2));
        System.out.println("updateTransactionHistory(): " + transactionHistoryDAO.updateTransactionHistory(2, tH2));
        System.out.println("--------------------------------");
        for (TransactionHistory transactionHistory : transactionHistoryDAO.getAllTransactionHistory()) {
            System.out.println(transactionHistory);
        }
        System.out.println("--------------------------------");
        System.out.println("delTransactionHistory(): " + transactionHistoryDAO.delTransactionHistory(2));
        System.out.println("--------------------------------");
        System.out.println("equals(): " + tH.equals(tH) + " " + tH.equals(tH2));

    }
}
