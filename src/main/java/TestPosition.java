package main.java;

import main.java.data.Position;
import main.java.db.BikeDAO;
import main.java.db.TripDAO;

import java.sql.SQLException;

public class TestPosition {
    public static void main(String[] args) throws SQLException {
        BikeDAO bikeDAO = new BikeDAO();
        System.out.println(bikeDAO.getBike(1));
        System.out.println(bikeDAO.setCharge(1, 100));
        System.out.println(bikeDAO.getCharge(1));
        Position p = new Position(56.023233, 45.959595);
        System.out.println(bikeDAO.setPosition(1, p));
        System.out.println(bikeDAO.getPosition(1));
        System.out.println(bikeDAO.getBike(1));
        System.out.println(bikeDAO.getAmountBikesCharging("Jernbanestasjonen"));

        TripDAO tripDAO = new TripDAO();
        System.out.println(tripDAO.getAmountTrips(1));
        System.out.println(tripDAO.getAmountTrips(2));
        System.out.println(tripDAO.getTotalLengthTrips(1));
        System.out.println(tripDAO.getTotalLengthTrips(2));
    }
}
