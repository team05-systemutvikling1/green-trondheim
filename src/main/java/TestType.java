package main.java;

import main.java.data.Type;
import main.java.db.TypeDAO;

import java.sql.SQLException;

public class TestType {
    public static void main(String[] args) throws SQLException {

        // Test for Type and TypeDAO
        TypeDAO typeDAO = new TypeDAO();
        Type t1 = new Type("Adult");
        Type t2 = new Type("Children");
        Type t3 = new Type("TestBike");


        typeDAO.delType("Adult");
        typeDAO.delType("Children");
        typeDAO.delType("TestBike");
        typeDAO.delType("WithBasket");

        System.out.println("\nType and TypeDAO tests");
        System.out.println(typeDAO.getType("Adult"));
        System.out.println("--------------------------------");
        System.out.println("addType(): " + typeDAO.addType(t1));
        System.out.println(typeDAO.getType("Adult"));
        System.out.println("addType(): " + typeDAO.addType(t2));
        System.out.println(typeDAO.getType("Children"));
        System.out.println("addType(): " + typeDAO.addType(t3));
        System.out.println(typeDAO.getType("TestBike"));
        System.out.println("updateTypeDAO(): " + typeDAO.updateType("Adult","WithBasket"));
        System.out.println("--------------------------------");
        for (Type type: typeDAO.getAllTypes()) {
            System.out.println(type);
        }
        System.out.println("--------------------------------");
        System.out.println("delType(): " + typeDAO.delType("TestBike"));
        System.out.println("--------------------------------");
        System.out.println("equals(): " + t1.equals(t1) + " " + t1.equals(t2));
        System.out.println("getAllTypes(): " + typeDAO.getAllTypes());

        typeDAO.delType("Children");
        typeDAO.delType("WithBasket");



    }
}
