package main.java;        // Tests for CardInfo and CardInfoDAO

import main.java.data.CardInfo;
import main.java.db.CardInfoDAO;

import java.sql.SQLException;
import java.time.LocalDate;

public class TestCardInfo {
    public static void main(String[] args) throws SQLException {
        CardInfoDAO cardInfoDAO = new CardInfoDAO();
        CardInfo c1 = new CardInfo("Ola","Nordmann",12345678912L,234L, 234234);
        CardInfo c2 = new CardInfo("Rolf","Svenske",12345678912L,1234123412341234L, 234234);

        System.out.println("\nCardInfo and CardInfoDAO tests:");
        System.out.println(cardInfoDAO.getCardInfo(15106067890L));
        System.out.println("--------------------------------");
        for(CardInfo cardInfo : cardInfoDAO.getAllCardInfo()){
            System.out.println(cardInfo);
        }
        System.out.println("--------------------------------");
        System.out.println("addCardInfo():" + cardInfoDAO.addCardInfo(c1));
        for(CardInfo cardInfo : cardInfoDAO.getAllCardInfo()){
            System.out.println(cardInfo);
        }
        System.out.println("--------------------------------");
        System.out.println("updateCardInfo():" + cardInfoDAO.updateCardInfo(c2, 234L));
        System.out.println("--------------------------------");
        for(CardInfo cardInfo : cardInfoDAO.getAllCardInfo()){
            System.out.println(cardInfo);
        }
        System.out.println("equals():" + c1.equals(c1) +" "+ c1.equals(c2));
        System.out.println("delCardInfo(): " + cardInfoDAO.delCardInfo(12345678912L));
    }
}

