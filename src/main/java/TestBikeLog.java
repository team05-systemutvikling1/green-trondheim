package main.java;

import main.java.data.BikeLog;
import main.java.data.Position;
import main.java.db.BikeLogDAO;

import java.sql.SQLException;

public class TestBikeLog {
    public static void main(String[] args) throws SQLException {
        BikeLogDAO bikeLogDAO = new BikeLogDAO();
        System.out.println(bikeLogDAO.addBikeLog(6));
        System.out.println(bikeLogDAO.addBikeLog(6, 70, new Position(60.123456, 60.654321)));
        for (BikeLog bl : bikeLogDAO.getBikeLog(6)) {
            System.out.println(bl);
        }
    }
}
