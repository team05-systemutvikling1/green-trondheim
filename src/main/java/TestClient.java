package main.java;

import main.java.data.*;
import main.java.db.*;
import main.java.util.*;

import java.time.LocalDate;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class TestClient {
    public static void main(String[] args) throws SQLException {
        // Test for Maintenance and MaintenanceDAO:
        MaintenanceDAO mDAO = new MaintenanceDAO();
        Maintenance m1 = new Maintenance(3, 1, 3, 1000.0);
        Maintenance m2 = new Maintenance(2, 2, 2, 2000.0);

        System.out.println("\nMaintenance and MaintenanceDAO tests:");
        System.out.println(mDAO.getMaintenance(1));
        System.out.println("--------------------------------");
        System.out.println("addMaintenance(): " + mDAO.addMaintenance(m1));
        System.out.println("setDoc(): " + mDAO.setDoc(3, "Swapping tires"));
        System.out.println("setStartDate(): " + mDAO.setStartDate(3, LocalDate.of(2018, 4, 1)));
        System.out.println("setEndDate(): " + mDAO.setEndDate(3, LocalDate.of(2018, 4, 30)));
        System.out.println("--------------------------------");
        System.out.println("getAllMaintenances(): ");
        for (Maintenance m : mDAO.getAllMaintenances()) {
            System.out.println(m);
        }
        System.out.println("--------------------------------");
        System.out.println("getAllNotStarted(): ");
        for (Maintenance m : mDAO.getAllNotStarted()) {
            System.out.println(m);
        }
        System.out.println("--------------------------------");
        System.out.println("getAllStarted(): ");
        for (Maintenance m : mDAO.getAllStarted()) {
            System.out.println(m);
        }
        System.out.println("--------------------------------");
        System.out.println("getAllFinished(): ");
        for (Maintenance m : mDAO.getAllFinished()) {
            System.out.println(m);
        }
        System.out.println("--------------------------------");
        System.out.println("delMaintenance(): " + mDAO.delMaintenance(3));
        System.out.println("--------------------------------");
        System.out.println("equals(): " + m1.equals(m1) + " " + m1.equals(m2));

/*        // Test for DockingStation and DockingStationDAO:
        DockingStationDAO dsDAO = new DockingStationDAO();
        DockingStation s1 = new DockingStation("Hospitalskirka", "Kongensgate 80", 75);
        DockingStation s2 = new DockingStation("Lade", "Lademoen 1", 50);

        System.out.println("\nDockingStation and DockingStationDAO tests:");
        System.out.println(dsDAO.getDockingStation(2));
        System.out.println("--------------------------------");
        for (DockingStation ds : dsDAO.getAllDockingStations()){
            System.out.println(ds);
        }
        System.out.println("--------------------------------");
        System.out.println(dsDAO.getDocksHashMap().toString());
        System.out.println("--------------------------------");
        System.out.println("addDockingStation(): " + dsDAO.addDockingStation(s1));
        System.out.println(dsDAO.getDockingStation(3));
        System.out.println("updateDockingStation(): " + dsDAO.updateDockingStation(3, s2));
        System.out.println("--------------------------------");

        System.out.println(dsDAO.getDockingStation(3));
        System.out.println("delDockingStation(): " + dsDAO.delDockingStation(3));
        for (DockingStation ds : dsDAO.getAllDockingStations()){
            System.out.println(ds);
        }
        System.out.println("--------------------------------");
        System.out.println(dsDAO.getDockingBikes(1).toString());
        System.out.println(".equals(): " + s1.equals(s1) + " " + s1.equals(s2));
*/


        // Test for User and UserDAO:
        UserDAO userDAO = new UserDAO();
        User u = new User("per@live.no", 15106067890L, "Per", "Olsen", 43652132);
        User u2 = new User("per@live.no", 15106067890L, "Kåre", "Nilsen", 43652132);

        System.out.println("\nUser and UserDAO tests:");
        System.out.println(userDAO.getUser("olanordmann@gmail.com"));
        System.out.println("--------------------------------");
        for (User user : userDAO.getAllUsers()) {
            System.out.println(user);
        }
        System.out.println("--------------------------------");
        System.out.println("addUser(): " + userDAO.addUser(u));
        System.out.println("updateUser(): " + userDAO.updateUser(u2));
        System.out.println("--------------------------------");
        for (User user : userDAO.getAllUsers()) {
            System.out.println(user);
        }
        System.out.println("--------------------------------");
        System.out.println("delUser(): " + userDAO.delUser("per@live.no"));
        System.out.println("--------------------------------");
        System.out.println("equals(): " + u.equals(u) + " " + u.equals(u2));
/*
        // Test for Bike and BikeDAO:
        BikeDAO bikeDAO = new BikeDAO();
        Bike b = new Bike("Jernbanestasjonen", false, "Standard", 10000, LocalDate.of(2018, 01, 15));
        Bike b2 = new Bike("Jernbanestasjonen", false, "Terrain", 20000, LocalDate.of(2018, 01, 15));

        System.out.println("\nBike and BikeDAO tests:");
        System.out.println(bikeDAO.getBike(1));
        System.out.println("--------------------------------");
        for (Bike bike : bikeDAO.getAllBikes()) {
            System.out.println(bike);
        }
        System.out.println("--------------------------------");
        System.out.println("addBike(): " + bikeDAO.addBike(b));
        System.out.println(bikeDAO.getBike(4));
        System.out.println("updateBike(): " + bikeDAO.updateBike(4, b2));
        System.out.println("--------------------------------");
        for (Bike bike : bikeDAO.getAllBikes()) {
            System.out.println(bike);
        }
        System.out.println("--------------------------------");
        System.out.println("delBike(): " + bikeDAO.delBike(4));
        System.out.println("--------------------------------");
        for (Bike bike : bikeDAO.getAllBrokenBikes()) {
            System.out.println(bike);
        }
        System.out.println("--------------------------------");
        System.out.println("setName(): " + bikeDAO.setName(1, "Pirbadet"));
        System.out.println("getName(): " + bikeDAO.getName(1));
        System.out.println("setName(): " + bikeDAO.setName(1, "Jernbanestasjonen"));
        System.out.println("getName(): " + bikeDAO.getName(1));
        System.out.println("--------------------------------");
        System.out.println("equals(): " + b.equals(b) + " " + b.equals(b2));
*/
        // Test for Workshop and WorkshopDAO:
        WorkshopDAO workshopDAO = new WorkshopDAO();
        Workshop w = new Workshop(3, "Ready for deletion", 99887766, "del@me.no", "Bakgata 5");
        Workshop w2 = new Workshop(3, "RForD", 55887766, "del@me.no", "Bakgata 7");

        System.out.println("\nWorkshop and WorkshopDAO tests:");
        System.out.println(workshopDAO.getWorkshop(1));
        System.out.println("--------------------------------");
        for (Workshop workshop : workshopDAO.getAllWorkshops()) {
            System.out.println(workshop);
        }
        System.out.println("--------------------------------");
        System.out.println("addWorkshop(): " + workshopDAO.addWorkshop(w));
        System.out.println(workshopDAO.getWorkshop(3));
        System.out.println("updateWorkshop(): " + workshopDAO.updateWorkshop(w2));
        System.out.println("--------------------------------");
        for (Workshop workshop : workshopDAO.getAllWorkshops()) {
            System.out.println(workshop);
        }
        System.out.println("--------------------------------");
        System.out.println("delWorkshop(): " + workshopDAO.delWorkshop(3));
        System.out.println("--------------------------------");
        System.out.println("equals(): " + w.equals(w) + " " + w.equals(w2));

        // Test for Reservation and ReservationDAO:
        ReservationDAO reservationDAO = new ReservationDAO();
        Reservation r = new Reservation("karihansen@hotmail.com", 19, "Pirbadet", LocalDateTime.of(LocalDate.of(2018, 1, 20), LocalTime.of(20, 0)), null);
        Reservation r2 = new Reservation("karihansen@hotmail.com", 19, "Jernbanestasjonen", LocalDateTime.of(LocalDate.of(2018, 1, 20), LocalTime.of(20, 0)), null);

        System.out.println("\nReservation and ReservationDAO tests:");
        System.out.println(reservationDAO.getReservation(1));
        System.out.println("--------------------------------");
        System.out.println("addReservation(): " + reservationDAO.addReservation(r));
        System.out.println(reservationDAO.getReservation(2));
        System.out.println("endReservation(): " + reservationDAO.endReservation(2, LocalDateTime.of(LocalDate.of(2018, 1, 21), LocalTime.of(20, 0))));
        System.out.println("--------------------------------");
        for (Reservation reservation : reservationDAO.getAllReservations()) {
            System.out.println(reservation);
        }
        System.out.println("--------------------------------");
        System.out.println("delReservation(): " + reservationDAO.delReservation(2));
        System.out.println("--------------------------------");
        System.out.println("equals(): " + r.equals(r) + " " + r.equals(r2));
        System.out.println("--------------------------------");

        // Test for HashingUtil:
        System.out.println("\nTests for HashingUtil");
        String salt = HashingUtil.getRandomString();
        String salt2 = HashingUtil.getRandomString();
        String password = HashingUtil.getHash("testpassord", salt);
        System.out.println(HashingUtil.isCorrect("testpassord", salt, password));
        System.out.println(HashingUtil.isCorrect("testpassord", salt2, password));
        System.out.println(HashingUtil.isCorrect("feil", salt, password));

        // Test for hashing in User:
        System.out.println("\nTests for passwordhashing in User");
        System.out.println("setPassword(): " + userDAO.setPassword("karihansen@hotmail.com", "passord123"));
        System.out.println("isPasswordCorrect(): "
                            + userDAO.isPasswordCorrect("karihansen@hotmail.com", "passord123") + " "
                            + userDAO.isPasswordCorrect("karihansen@hotmail.com", "feilpassord"));

    }
}
