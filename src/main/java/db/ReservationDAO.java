package main.java.db;

import main.java.data.Reservation;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 * Data Access Object for Reservation
 *
 * @author Magne
 */
public class ReservationDAO {
    private Connection connection;

    /**
     * Creates a connection to the database for ReservationDAO
     *
     * @throws SQLException if connecting to DB fails.
     */
    public ReservationDAO() throws SQLException {
        connection = Db.instance().getConnection();
    }

    /**
     * Returns Reservation object with the email and tripId given as input
     *
     * @param reservationId of the Reservation object you want returned from the database
     * @return Reservation object
     * @throws SQLException if query fails
     */
    public Reservation getReservation(int reservationId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT * FROM reservation WHERE reservation_id=?");
            ps.setInt(1, reservationId);
            rs = ps.executeQuery();
            Reservation r = null;
            if (rs.next()) {
                r = new Reservation();
                r.setReservationId(rs.getInt("reservation_id"));
                r.setEmail(rs.getString("e_mail"));
                r.setTripId(rs.getInt("trip_id"));
                r.setName(rs.getString("name"));
                r.setFromTime(rs.getTimestamp("from_time"));
                if (rs.getTimestamp("to_time") != null) {
                    r.setToTime(rs.getTimestamp("to_time"));
                }
            }
            return r;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * Returns an Arraylist of all reservations registered in the system
     *
     * @return Arraylist of all reservations
     * @throws SQLException if query fails
     */
    public ArrayList<Reservation> getAllReservations() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT * FROM reservation");
            rs = ps.executeQuery();
            ArrayList<Reservation> reservations = new ArrayList<>();
            while (rs.next()) {
                Reservation r = new Reservation();
                r.setReservationId(rs.getInt("reservation_id"));
                r.setEmail(rs.getString("e_mail"));
                r.setTripId(rs.getInt("trip_id"));
                r.setName(rs.getString("name"));
                r.setFromTime(rs.getTimestamp("from_time"));
                if (rs.getTimestamp("to_time") != null) {
                    r.setToTime(rs.getTimestamp("to_time"));
                }
                reservations.add(r);
            }
            return reservations;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * Adds a Reservation to the system, given a Reservation object
     *
     * @param r A Reservation object that should be added
     * @return true if successfully added
     * @throws SQLException if query fails
     */
    public boolean addReservation(Reservation r) throws SQLException {
        PreparedStatement ps = null;
        try {
            autoIncrementFix();
            ps = connection.prepareStatement("INSERT INTO reservation (e_mail, trip_id, name, from_time, to_time) VALUES(?,?,?,?,?)");
            ps.setString(1, r.getEmail());
            ps.setInt(2, r.getTripId());
            ps.setString(3, r.getName());
            ps.setTimestamp(4, Timestamp.valueOf(r.getFromTime()));
            if (r.getToTime() != null) {
                ps.setTimestamp(5, Timestamp.valueOf(r.getToTime()));
            } else {
                ps.setTimestamp(5, null);
            }
            int res = ps.executeUpdate();
            return res == 1;
        } finally {
            if(ps != null){ps.close();}
        }
    }

    /**
     * Method to end a reservation by setting an toTime equal to the given toTime,
     * on the reservation with the given tripId
     *
     * @param reservationId of the reservation that should be ended
     * @param toTime that should be set as the toTime in the reservation
     * @return true if reservation is successfully ended
     * @throws SQLException if query fails
     */
    public boolean endReservation(int reservationId, LocalDateTime toTime) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("UPDATE reservation SET to_time=? WHERE reservation_id=?");
            ps.setTimestamp(1, Timestamp.valueOf(toTime));
            ps.setInt(2, reservationId);
            int res = ps.executeUpdate();
            return res == 1;
        } finally {
            if(ps != null){ps.close();}
        }
    }

    /**
     * Deletes a reservation from the system, with given email and tripId
     *
     * @param reservationId of the reservation that should be deleted
     * @return true if successfully deleted
     * @throws SQLException if query fails
     */
    public boolean delReservation(int reservationId) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("DELETE FROM reservation WHERE reservation_id=?");
            ps.setInt(1, reservationId);
            int res = ps.executeUpdate();
            return res == 1;
        } finally {
            if(ps != null){ps.close();}
        }
    }

    /**
     * Method preventing endless auto incrementing bike_id by setting the auto increment to chose the
     * max bike_id+1 for next bike
     *
     * @throws SQLException if failing
     */
    private void autoIncrementFix() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT MAX(reservation_id)+1 FROM reservation");
            rs = ps.executeQuery();
            if (rs.next()) {
                ps = connection.prepareStatement("ALTER TABLE reservation AUTO_INCREMENT=?");
                ps.setInt(1, rs.getInt("MAX(reservation_id)+1"));
            }
            ps.executeUpdate();
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }
}
