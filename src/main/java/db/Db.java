package main.java.db;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import java.sql.*;

/**
 * Class create Db connection
 *
 * @author Magne
 */
public class Db {
    private static Db db;
    private static ComboPooledDataSource cpds;

    private static String DB_URL = "jdbc:mysql://mysql.stud.iie.ntnu.no:3306/";
    private static String DB_USERNAME = "kevinah";
    private static String DB_PW = "TH0F2TdB";
    private static final String DB_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DBUSESSL = "?useSSL=false";

    /**
     * Sets up ComboPooledDataSource to handle multiple simultaneous queries to DB
     */
    private Db() {
        try {
            cpds = new ComboPooledDataSource();
            cpds.setDriverClass(DB_DRIVER); //loads the jdbc driver
            cpds.setJdbcUrl(DB_URL+DB_USERNAME+DBUSESSL);
            cpds.setUser(DB_USERNAME);
            cpds.setPassword(DB_PW);

            cpds.setMaxPoolSize(100);
            cpds.setMaxStatements(180);
        } catch (Exception e) {
            System.out.println("DB connection failed");
        }
    }

    /**
     * Checks if there is an existing instance of Db. If not, it creates one.
     *
     * @return An instance of Db, either the existing one, or a newly created one.
     */
    public static Db instance() {
        if (db == null) {
            db = new Db();
            return db;
        } else {
            return db;
        }
    }

    /**
     * Returns the connection from the Db object.
     *
     * @return A connection to the Db object.
     * @throws SQLException if failing to get connection
     */
    public Connection getConnection() throws SQLException {
        return cpds.getConnection();
    }

    public static void setNewDB(String dburl, String username, String password) {
        DB_URL = dburl;
        DB_USERNAME = username;
        DB_PW = password;
    }

    public static void setNewDB(String username, String password) {
        DB_USERNAME = username;
        DB_PW = password;
    }
}