package main.java.db;

import main.java.data.Admin;
import main.java.util.EmailUtil;
import main.java.util.HashingUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Data Access Object for Admin
 *
 * @author Magne
 */
public class AdminDAO {
    private Connection connection;

    /**
     * Creates a connection to the database for AdminDAO
     *
     * @throws SQLException if connection fails
     */
    public AdminDAO() throws SQLException {
        connection = Db.instance().getConnection();
    }

    /**
     * Method that gives access to an Admin object, given a username.
     *
     * @param username of Admin that should be returned
     * @return Admin object with the given username
     * @throws SQLException if failing to get admin
     */
    public Admin getAdmin(String username) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT * FROM admin WHERE username=?");
            ps.setString(1, username);
            rs = ps.executeQuery();
            Admin a = null;
            if (rs.next()) {
                a = new Admin();
                a.setUsername(rs.getString("username"));
                a.setPassword(rs.getString("password"));
                a.setSalt(rs.getString("salt"));
            }
            return a;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * Returns an ArrayList of all registered admins
     *
     * @return ArrayList of admins
     * @throws SQLException if query fails
     */
    public ArrayList<Admin> getAllAdmins() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT * FROM admin");
            rs = ps.executeQuery();
            ArrayList<Admin> admins = new ArrayList<>();
            while (rs.next()) {
                Admin a = new Admin();
                a.setUsername(rs.getString("username"));
                a.setPassword(rs.getString("password"));
                a.setSalt(rs.getString("salt"));
                admins.add(a);
            }
            return admins;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * Adds an admin to the database, given an email
     *
     * @param email Email that should be used as username for new user
     * @return true if successfully added
     * @throws SQLException if query fails
     */
    public boolean addAdmin(String email) throws SQLException {
        if (email.equals("")) {
            return false;
        }
        String salt = HashingUtil.getRandomString();
        String randomNewPassword = HashingUtil.getRandomString();
        String hashedPassword = HashingUtil.getHash(randomNewPassword, salt);
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("INSERT INTO admin (username, password, salt) VALUES (?,?,?)");
            ps.setString(1, email);
            ps.setString(2, hashedPassword);
            ps.setString(3, salt);
            int res = ps.executeUpdate();
            if (EmailUtil.sendNewAdminEmail(email, randomNewPassword)) {
                System.out.println("Email sendt");
            } else {
                System.out.println("Email ikke sendt");
            }
            return res == 1;
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }


    /**
     * Adds an admin without sending mail, given an Admin object
     *
     * @param a Admin object that should be used to make a new admin
     * @return true if successfully added
     * @throws SQLException if query fails
     */
    public boolean addAdminWithoutMail(Admin a) throws SQLException {
        if ((a.getPassword()).equals("") || (a.getUsername()).equals("")) {
            return false;
        }
        String username = a.getUsername();
        String salt = HashingUtil.getRandomString();
        String unhashedPassord = a.getPassword();
        String hashedPassword = HashingUtil.getHash(unhashedPassord, salt);
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("INSERT INTO admin (username, password, salt) VALUES (?,?,?)");
            ps.setString(1, username);
            ps.setString(2, hashedPassword);
            ps.setString(3, salt);
            int res = ps.executeUpdate();
            return res == 1;
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }

    /**
     * Deletes an admin from the database, given a valid username
     *
     * @param username of the admin you want to delete
     * @return true if successfully deleted
     * @throws SQLException if query fails
     */
    public boolean delAdmin(String username) throws SQLException {
        if (countAdmins() <= 1) {
            return false;
        }
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("DELETE FROM admin WHERE username=?");
            ps.setString(1, username);
            int res = ps.executeUpdate();
            return res == 1;
        } finally {
            if(ps != null){ps.close();}
        }
    }

    /**
     * Sets a password for the admin with given username
     *
     * @param username of the admin that should have a new password set
     * @param password that should be the new password for the admin
     * @return true if new password is successfully set
     * @throws SQLException if setting new password fails
     */
    public boolean setPassword(String username, String password) throws SQLException {
        String salt = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT salt FROM admin WHERE username=?");
            ps.setString(1, username);
            rs = ps.executeQuery();
            if (rs.next()) {
                salt = rs.getString("salt");
            }
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
        ps = null;
        try {
            ps = connection.prepareStatement("UPDATE admin SET password=? WHERE username=?");
            ps.setString(1, HashingUtil.getHash(password, salt));
            ps.setString(2, username);
            int res = ps.executeUpdate();
            return res == 1;
        } finally {
            if(ps != null){ps.close();}
        }
    }

    /**
     * Checks if given password is the correct password for the admin, with given username
     *
     * @param username of the admin that should have password checked
     * @param password that should be checked if is correct
     * @return true if correct password, false if not
     * @throws SQLException if query fails
     */
    public boolean isPasswordCorrect(String username, String password) throws SQLException {
        String salt = null;
        String hashed = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT salt, password FROM admin WHERE username=?");
            ps.setString(1, username);
            rs = ps.executeQuery();
            if (rs.next()) {
                salt = rs.getString("salt");
                hashed = rs.getString("password");
            } else {
                return false;
            }
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
        return HashingUtil.isCorrect(password, salt, hashed);
    }

    /**
     * Counts how many admins there are in the system/database
     *
     * @return int with number of admins in the database
     * @throws SQLException if query fails
     */
    public int countAdmins() throws SQLException {
        int adminCount = -1;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT COUNT(username) AS amount FROM admin");
            rs = ps.executeQuery();
            if (rs.next()) {
                adminCount = rs.getInt("amount");
            }
            return adminCount;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }
}
