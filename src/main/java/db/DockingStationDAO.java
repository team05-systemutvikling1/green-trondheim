package main.java.db;

import main.java.data.Bike;
import main.java.data.DockingStation;
import main.java.data.Position;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Data Access Object for DockingStation
 *
 * @author Ørjan
 */
public class DockingStationDAO {
    private Connection connection;

    /**
     * Creates a connection to database for DockingStationDAO
     *
     * @throws SQLException if connection to DB fails
     */
    public DockingStationDAO() throws SQLException{
        connection = Db.instance().getConnection();
    }

    /**
     * Returns docking station from the given docking station name.
     *
     * @param dockname Name of docking station to be returned.
     * @return Dockingstation from the given docking station name.
     * @throws SQLException if query fails
     */
    public DockingStation getDockingStation(String dockname) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            ps = connection.prepareStatement("SELECT * FROM docking_station WHERE name =?");
            ps.setString(1, dockname);
            rs = ps.executeQuery();
            DockingStation s = null;
            if(rs.next()){
                s = new DockingStation();
                s.setName(rs.getString("name"));
                s.setAddress(rs.getString("address"));
                s.setxCoordinate(rs.getDouble("x_coordinate"));
                s.setyCoordinate(rs.getDouble("y_coordinate"));
                s.setMaxBikes(rs.getInt("max_bikes"));
            }
            return s;
        }finally{
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * Returns all docking stations.
     *
     * @return An array of all docking stations.
     * @throws SQLException if query fails
     */
    public ArrayList<DockingStation> getAllDockingStations() throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            ps = connection.prepareStatement("SELECT * FROM  docking_station");
            rs = ps.executeQuery();
            ArrayList<DockingStation> dockingStations = new ArrayList<>();
            fillArrayList(dockingStations, rs);
            return dockingStations;

        }finally{
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * Get an Arraylist of all the dockingstation names
     *
     * @return ArrayList of all dockingstation names
     * @throws SQLException if query fails
     */
    public ArrayList<String> getAllDockingStationNames() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT name FROM docking_station");
            rs = ps.executeQuery();
            ArrayList<String> dockingStationNames = new ArrayList<>();
            while (rs.next()) {
                String name = rs.getString("name");
                dockingStationNames.add(name);
            }
            return dockingStationNames;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * Returns bikes at docking station given a docking station name.
     *
     * @param dockname Name of docking station.
     * @return An array of all bikes at chosen docking station.
     * @throws SQLException if query fails
     */
    public ArrayList<Bike> getDockingBikes(String dockname) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            ps = connection.prepareStatement("SELECT * FROM bike WHERE name=?");
            ps.setString(1, dockname);
            rs = ps.executeQuery();
            ArrayList<Bike> bikes = new ArrayList<>();
            fillBikeList(bikes, rs);
            return bikes;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }

    }

    /**
     * Adds docking station given a docking station object.
     *
     * @param s Is a docking station object.
     * @return True if added and false if not added.
     * @throws SQLException if update fails
     */
    public boolean addDockingStation(DockingStation s) throws SQLException{
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("INSERT INTO docking_station (name, address, x_coordinate, y_coordinate, max_bikes)" +
                    " VALUES (?,?,?,?,?)");
            ps.setString(1, s.getName());
            ps.setString(2, s.getAddress());
            ps.setDouble(3, s.getxCoordinate());
            ps.setDouble(4, s.getyCoordinate());
            ps.setInt(5, s.getMaxBikes());
            int res = ps.executeUpdate();
            return res == 1;
        }finally{
            if(ps != null){ps.close();}

        }
    }

    /**
     * Updates docking station given the name of the docking station and a docking station object.
     *
     * @param dockname Name of docking station.
     * @param s Updated docking station.
     * @return True if updated and false if not added.
     * @throws SQLException if update fails
     */
    public boolean updateDockingStation(String dockname, DockingStation s) throws SQLException{
        PreparedStatement ps = null;
        try{
            ps = connection.prepareStatement("UPDATE docking_station SET name=?, address=?, x_coordinate=?," +
                    " y_coordinate=?, max_bikes=? WHERE name=?");
            ps.setString(1, s.getName());
            ps.setString(2, s.getAddress());
            ps.setDouble(3, s.getxCoordinate());
            ps.setDouble(4, s.getyCoordinate());
            ps.setInt(5, s.getMaxBikes());
            ps.setString(6, dockname);
            int res = ps.executeUpdate();
            return res == 1;
        }finally {
            if(ps != null){ps.close();}

        }
    }


    /**
     * Deletes docking station given a docking station name.
     *
     * @param dockname Name of docking station.
     * @return True if deleted and false if not deleted.
     * @throws SQLException if update fails
     */
    public boolean delDockingStation(String dockname) throws SQLException{
        PreparedStatement ps = null;
        try{
            ps = connection.prepareStatement("DELETE FROM docking_station WHERE name=?");
            ps.setString(1, dockname);
            int res = ps.executeUpdate();
            return res == 1;
        }finally {
            if(ps != null){ps.close();}

        }
    }

    /**
     * Get the position of a dockingstation
     *
     * @param dockname name of dockingstation you want position from
     * @return Position object with position of the dockingstation
     * @throws SQLException
     */
    public Position getPosition(String dockname) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT x_coordinate, y_coordinate FROM docking_station WHERE name =?");
            ps.setString(1, dockname);
            rs = ps.executeQuery();
            Position p = null;
            if(rs.next()){
                p = new Position();
                p.setxCoordinate(rs.getDouble("x_coordinate"));
                p.setyCoordinate(rs.getDouble("y_coordinate"));
            }
            return p;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * Set positon of a dockingstation
     *
     * @param dockname of dockingstation
     * @param position new position for dockingstation
     * @return true if successfully updated
     * @throws SQLException if update fails
     */
    public boolean setPosition(String dockname, Position position) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("UPDATE docking_station SET x_coordinate=?, y_coordinate=? WHERE name=?");
            ps.setDouble(1, position.getxCoordinate());
            ps.setDouble(2, position.getyCoordinate());
            ps.setString(3, dockname);
            int res = ps.executeUpdate();
            return res == 1;
        } finally {
            if(ps != null){ps.close();}
        }
    }

    /**
     * Helping method to fill and arraylist with dockingstation objects
     *
     * @param dockingStations arraylist that should be filled
     * @param rs              ResultSet used to fill arraylist
     * @throws SQLException if query fails
     */
    private void fillArrayList(ArrayList<DockingStation> dockingStations, ResultSet rs) throws SQLException {
        while (rs.next()) {
            DockingStation s = new DockingStation();
            s.setName(rs.getString("name"));
            s.setAddress(rs.getString("address"));
            s.setxCoordinate(rs.getDouble("x_coordinate"));
            s.setyCoordinate(rs.getDouble("y_coordinate"));
            s.setMaxBikes(rs.getInt("max_bikes"));
            dockingStations.add(s);
        }
    }

    /**
     * Helping method to add bikes to arraylist
     *
     * @param bikes arraylist to be filled
     * @param rs ResultSet used to fill the arraylist
     * @throws SQLException if query fails
     */
    private void fillBikeList(ArrayList<Bike> bikes, ResultSet rs) throws SQLException {
        while (rs.next()) {
            Bike b = new Bike();
            b.setBikeId(rs.getInt("bike_id"));
            b.setName(rs.getString("name"));
            b.setNeedMaintenance(rs.getBoolean("need_maintenance"));
            b.setType(rs.getString("type"));
            b.setPrice(rs.getDouble("price"));
            b.setDateBought(rs.getDate("date_bought"));
            b.setCharge(rs.getInt("charge"));
            b.setxCoordinate(rs.getDouble("x_coordinate"));
            b.setyCoordinate(rs.getDouble("y_coordinate"));
            b.setMake(rs.getString("make"));
            bikes.add(b);
        }
    }

}
