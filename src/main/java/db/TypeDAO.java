package main.java.db;


import main.java.data.Type;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


/**
 * Data Access Object for Type
 *
 * @author Jan-Marius
 */
public class TypeDAO {
    private Connection connection;

    /**
     * Creates a connection to the database for TypeDAO
     *
     * @throws SQLException if connecting to DB fails
     */
    public TypeDAO() throws SQLException {
        connection = Db.instance().getConnection();
    }

    /**
     * Returns Type object with the type name given as input
     *
     * @param type name of the bike type that should be returned
     * @return Type object with given type name
     * @throws SQLException if query fails
     */
    public Type getType(String type) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT * FROM type WHERE type=?");
            ps.setString(1, type);
            rs = ps.executeQuery();
            Type t = null;
            if (rs.next()) {
                t = new Type();
                t.setType(rs.getString("type"));
            }
            return t;
        } finally {
            if(rs != null){rs.close();}
            if(rs != null){ps.close();}
        }
    }


    /**
     * Return an Arraylist of all the types registered in the system
     *
     * @return Arraylist of all types
     * @throws SQLException if query fails
     */
    public ArrayList<Type> getAllTypes() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT * FROM type");
            rs = ps.executeQuery();
            ArrayList<Type> types = new ArrayList<>();
            while (rs.next()) {
                Type t = new Type();
                t.setType(rs.getString("type"));
                types.add(t);
            }
            return types;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }


    /**
     * Adds a type to the system, given a type object
     *
     * @param t A Type object that should be added
     * @return true if successfully added
     * @throws SQLException if query fails
     */
    public boolean addType(Type t) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("INSERT INTO type (type) VALUES(?)");
            ps.setString(1, t.getType());
            int res = ps.executeUpdate();
            return res == 1;
        } finally {
            if(ps != null){ps.close();}
        }
    }


    /**
     * Deletes a type from the system, with given type name
     *
     * @param type name of the type you want to remove
     * @return true if successfully
     * @throws SQLException if query fails
     */
    public boolean delType(String type) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("DELETE FROM type WHERE type=?");
            ps.setString(1, type);
            int res = ps.executeUpdate();
            return res == 1;
        } finally {
            if(ps != null){ps.close();}
        }
    }

    /**
     * Update a type
     *
     * @param oldType old type that should be updated
     * @param newType the new type
     * @return true if successfully set
     * @throws SQLException if update fails
     */
    public boolean updateType(String oldType, String newType) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("UPDATE type SET type=? WHERE type=?");
            ps.setString(1, newType);
            ps.setString(2, oldType);
            int res = ps.executeUpdate();
            return res == 1;
        } finally {
            if(ps != null){ps.close();}
        }
    }






}
