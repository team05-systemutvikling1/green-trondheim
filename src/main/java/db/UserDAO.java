package main.java.db;

import main.java.data.User;
import main.java.util.HashingUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Data Access Object for User
 *
 * @author Magne
 */
public class UserDAO {
    private  Connection connection;

    /**
     * Creates a connection to the database for UserDAO
     *
     * @throws SQLException if connection fails
     */
    public UserDAO() throws SQLException {
        connection = Db.instance().getConnection();
    }

    /**
     * Method that gives access to a User object, given an e_mail
     *
     * @param eMail of User that should be returned
     * @return User object with the given e_mail
     * @throws SQLException if failing to get user
     */
    public User getUser(String eMail) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT * FROM user WHERE e_mail=?");
            ps.setString(1, eMail);
            rs = ps.executeQuery();
            User u = null;
            if (rs.next()) {
                u = new User();
                u.seteMail(rs.getString("e_mail"));
                u.setNationalId(rs.getLong("national_id"));
                u.setFirstName(rs.getString("first_name"));
                u.setLastName(rs.getString("last_name"));
                u.setPhoneNr(rs.getInt("phone_nr"));
            }
            return u;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * Returns an ArrayList of all registered users
     *
     * @return ArrayList of users
     * @throws SQLException if query fails
     */
    public ArrayList<User> getAllUsers() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT * FROM user");
            rs = ps.executeQuery();
            ArrayList<User> users = new ArrayList<>();
            while (rs.next()) {
                User u = new User();
                u.seteMail(rs.getString("e_mail"));
                u.setNationalId(rs.getLong("national_id"));
                u.setFirstName(rs.getString("first_name"));
                u.setLastName(rs.getString("last_name"));
                u.setPhoneNr(rs.getInt("phone_nr"));
                users.add(u);
            }
            return users;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * Adds a user to the database, given a User object.
     *
     * @param u An object of the User that should be added
     * @return true if successfully added
     * @throws SQLException if query fails
     */
    public boolean addUser(User u) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("INSERT INTO user (e_mail, national_id, first_name, last_name, phone_nr) VALUES(?,?,?,?,?)");
            ps.setString(1, u.geteMail());
            ps.setLong(2, u.getNationalId());
            ps.setString(3, u.getFirstName());
            ps.setString(4, u.getLastName());
            ps.setInt(5, u.getPhoneNr());
            int res = ps.executeUpdate();
            return res == 1;
        } finally {
            if(ps != null){ps.close();}
        }
    }

    /**
     * Deletes a user from the database, given a valid e_mail
     *
     * @param eMail of the user you want to delete
     * @return true if successfully deleted
     * @throws SQLException if query fails
     */
    public boolean delUser(String eMail) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("DELETE FROM user WHERE e_mail=?");
            ps.setString(1, eMail);
            int res = ps.executeUpdate();
            return res == 1;
        } finally {
            if(ps != null){ps.close();}
        }

    }

    /**
     * Updates the user with the same e_mail as the User object given as input
     *
     * @param u The user with the new and updated details
     * @return true if successfully added
     * @throws SQLException if query fails
     */
    public boolean updateUser(User u) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("UPDATE user SET first_name=?, last_name=?, phone_nr=? WHERE e_mail=?");
            ps.setString(1, u.getFirstName());
            ps.setString(2, u.getLastName());
            ps.setInt(3, u.getPhoneNr());
            ps.setString(4, u.geteMail());
            int res = ps.executeUpdate();
            return res == 1;
        } finally {
            if(ps != null){ps.close();}
        }
    }

    /**
     * Sets a password for the user with given email
     *
     * @param email of the user that should have a new password set
     * @param password that should be the new password for the user
     * @return true if new password is successfully set
     * @throws SQLException if setting new password fails
     */
    public boolean setPassword(String email, String password) throws SQLException {
        String salt = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT salt FROM user WHERE e_mail=?");
            ps.setString(1, email);
            rs = ps.executeQuery();
            if (rs.next()) {
                salt = rs.getString("salt");
            }
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
        if (salt == null) {
            salt = HashingUtil.getRandomString();
            try {
                ps = connection.prepareStatement("UPDATE user SET salt=? WHERE e_mail=?");
                ps.setString(1, salt);
                ps.setString(2, email);
                ps.executeUpdate();
            } finally {
                if(ps != null){ps.close();}
            }
        }
        String hashedPassword = HashingUtil.getHash(password, salt);
        try {
            ps = connection.prepareStatement("UPDATE user SET password=? WHERE e_mail=?");
            ps.setString(1, hashedPassword);
            ps.setString(2, email);
            int res = ps.executeUpdate();
            return res == 1;
        } finally {
            if(ps != null){ps.close();}
        }
    }

    /**
     * Checks if the given password is the correct password for the user, with given email
     *
     * @param email of the user that should have password checked
     * @param password that should be checked if is correct
     * @return true if correct password, false if not
     * @throws SQLException if query fails
     */
    public boolean isPasswordCorrect(String email, String password) throws SQLException {
        String salt = null;
        String hashed = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT salt, password FROM user WHERE e_mail=?");
            ps.setString(1, email);
            rs = ps.executeQuery();
            if (rs.next()) {
                salt = rs.getString("salt");
                hashed = rs.getString("password");
            }
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
        return HashingUtil.isCorrect(password, salt, hashed);
    }
}
