package main.java.db;

import main.java.data.ChargingLog;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 * Data Access Object for ChargingLog
 *
 * @author Magne
 */
public class ChargingLogDAO {
    private Connection connection;

    /**
     * Creates a connection to the database for ChargingLogDAO
     *
     * @throws SQLException if connection to database fails
     */
    public ChargingLogDAO() throws SQLException {
        this.connection = Db.instance().getConnection();
    }

    /**
     * Add a entry in the chargingLog
     *
     * @param name             of the dockingstation that should have a new entry in log
     * @param powerConsumption current powerconsumption on the given dockingstation
     * @return true if successfully added
     * @throws SQLException if update fails
     */
    public boolean addChargingLog(String name, int powerConsumption) throws SQLException {
        LocalDateTime time = LocalDateTime.now();
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("INSERT INTO charginglog (name, power_consumption, time) VALUES (?,?,?)");
            ps.setString(1, name);
            ps.setInt(2, powerConsumption);
            ps.setTimestamp(3, Timestamp.valueOf(time));
            int res = ps.executeUpdate();
            return res == 1;
        } finally {
            if(ps != null){ps.close();}
        }
    }

    /**
     * Get full charging log for a given dockingstation
     *
     * @param name dockingstation name that you want a log from
     * @return ArrayList with full charging log
     * @throws SQLException if query fails
     */
    public ArrayList<ChargingLog> getChargingLog(String name) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT * FROM charginglog WHERE name=?");
            ps.setString(1, name);
            rs = ps.executeQuery();
            ArrayList<ChargingLog> log = new ArrayList<>();
            while (rs.next()) {
                ChargingLog cl = new ChargingLog();
                cl.setClogId(rs.getInt("clog_id"));
                cl.setName(rs.getString("name"));
                cl.setPowerConsumption(rs.getInt("power_consumption"));
                cl.setTime(rs.getTimestamp("time"));
                log.add(cl);
            }
            return log;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }
}
