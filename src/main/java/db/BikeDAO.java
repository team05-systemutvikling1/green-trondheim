package main.java.db;

import main.java.data.Bike;
import main.java.data.Position;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Data Access Object for Bike
 *
 * @author Magne
 */
public class BikeDAO {
    private  Connection connection;

    /**
     * Creates a connection to the database for BikeDAO
     *
     * @throws SQLException if connecting to DB fails.
     */
    public BikeDAO() throws SQLException {
        connection = Db.instance().getConnection();
    }

    /**
     * Returns Bike object with the bike_id given as input
     *
     * @param bikeId ID of the bike that should be returned
     * @return Bike object with given bike_id
     * @throws SQLException if query fails
     */
    public Bike getBike(int bikeId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT * FROM bike WHERE bike_id=?");
            ps.setInt(1, bikeId);
            rs = ps.executeQuery();
            Bike b = null;
            if (rs.next()) {
                b = new Bike();
                b.setBikeId(rs.getInt("bike_id"));
                b.setName(rs.getString("name"));
                b.setNeedMaintenance(rs.getBoolean("need_maintenance"));
                b.setType(rs.getString("type"));
                b.setPrice(rs.getDouble("price"));
                b.setDateBought(rs.getDate("date_bought"));
                b.setCharge(rs.getInt("charge"));
                b.setxCoordinate(rs.getDouble("x_coordinate"));
                b.setyCoordinate(rs.getDouble("y_coordinate"));
                b.setMake(rs.getString("make"));
            }
            return b;
        } finally {
            if(rs != null){rs.close();}
            if(rs != null){ps.close();}
        }
    }

    /**
     * Returns an Arraylist of all the bikes registered in the system
     *
     * @return Arraylist of all bikes
     * @throws SQLException if query fails
     */
    public ArrayList<Bike> getAllBikes() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT * FROM bike");
            rs = ps.executeQuery();
            ArrayList<Bike> bikes = new ArrayList<>();
            fillArrayList(bikes, rs);
            return bikes;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * Returns an Arraylist with all the bikes where need_maintenance is true/1
     *
     * @return Arraylist of broken bikes
     * @throws SQLException query fails
     */
    public ArrayList<Bike> getAllBrokenBikes() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT * FROM bike WHERE need_maintenance=TRUE");
            rs = ps.executeQuery();
            ArrayList<Bike> bikes = new ArrayList<>();
            fillArrayList(bikes, rs);
            return bikes;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * Returns an ArrayList of all bikes that are placed at given dockingstation, and of correct biketype
     *
     * @param dockingname name of the dockingstation you want a bike from
     * @param type type of bike you wanted returned
     * @return ArrayList of bikes that meets the criteria
     * @throws SQLException if query fails
     */
    public ArrayList<Bike> getBikesAvailable(String dockingname, String type) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT * FROM bike WHERE name=? AND type=? AND charge>=25 AND need_maintenance=0");
            ps.setString(1, dockingname);
            ps.setString(2, type);
            rs = ps.executeQuery();
            ArrayList<Bike> bikes = new ArrayList<>();
            fillArrayList(bikes, rs);
            return bikes;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * Method used to get number of bikes charging at each dockingstation
     *
     * @param name Name of the dockingstation you want amount of charging bikes from
     * @return amount of bikes charging at dockingstation with given name, or -1 if something goes wrong
     * @throws SQLException if query fails
     */
    public int getAmountBikesCharging(String name) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT COUNT(name) AS amount FROM bike WHERE name=? AND charge<100");
            ps.setString(1, name);
            rs = ps.executeQuery();
            int amount = -1;
            if (rs.next()) {
                amount = rs.getInt("amount");
            }
            return amount;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * Charges all bikes 1%, unless they are fully charged
     *
     * @return true if successfully charged
     * @throws SQLException if update fails.
     */
    public boolean chargeAllBikesDocked() throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("UPDATE bike SET charge=charge+1 WHERE charge<100 AND name IS NOT NULL");
            int res = ps.executeUpdate();
            return res == 1;
        } finally {
            if(ps != null){ps.close();}
        }
    }

    /**
     * Adds a bike to the system, given a Bike object
     * Position is set to the dockingstation position
     * Charge is set to 100
     *
     * @param b A Bike object that should be added
     * @return true if successfully added
     * @throws SQLException if query fails
     */
    public boolean addBike(Bike b) throws SQLException {
        PreparedStatement ps = null;
        DockingStationDAO dockingStationDAO = new DockingStationDAO();
        Position p = dockingStationDAO.getPosition(b.getName());
        double x = p.getxCoordinate();
        double y = p.getyCoordinate();
        try {
            autoIncrementFix();
            ps = connection.prepareStatement("INSERT INTO bike (name, need_maintenance, type, price, date_bought, charge, x_coordinate, y_coordinate, make) VALUES(?,?,?,?,?,?,?,?,?)");
            ps.setString(1, b.getName());
            ps.setBoolean(2, b.isNeedMaintenance());
            ps.setString(3, b.getType());
            ps.setDouble(4, b.getPrice());
            ps.setDate(5, b.getDateBought());
            ps.setInt(6, 100);
            ps.setDouble(7, x);
            ps.setDouble(8, y);
            ps.setString(9, b.getMake());
            int res = ps.executeUpdate();
            return res == 1;
        } finally {
            if(ps != null){ps.close();}
        }
    }

    /**
     * Deletes a bike from the system, with given bike_id
     *
     * @param bikeId of the bike you want to remove
     * @return true if successfully
     * @throws SQLException if query fails
     */
    public boolean delBike(int bikeId) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("DELETE FROM bike WHERE bike_id=?");
            ps.setInt(1, bikeId);
            int res = ps.executeUpdate();
            return res == 1;
        } finally {
            if(ps != null){ps.close();}
        }
    }

    /**
     * Updates a bike, with given bike_id, setting it equal to the bike object given.
     *
     * @param bikeId of the bike that should be changed
     * @param b Bike object with new info
     * @return true if successfully updating bike
     * @throws SQLException if update fails
     */
    public boolean updateBike(int bikeId, Bike b) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("UPDATE bike SET name=?, need_maintenance=?, type=?, price=?, date_bought=?, make=? WHERE bike_id=?");
            ps.setString(1, b.getName());
            ps.setBoolean(2, b.isNeedMaintenance());
            ps.setString(3, b.getType());
            ps.setDouble(4, b.getPrice());
            ps.setDate(5, b.getDateBought());
            ps.setString(6, b.getMake());
            ps.setInt(7, bikeId);
            int res = ps.executeUpdate();
            return res == 1;
        } finally {
            if(ps != null){ps.close();}
        }
    }

    /**
     * Return the dock_name of the bike with given bike_id
     *
     * @param bikeId of the bike you want dock_name from
     * @return int dock_name of the bike
     * @throws SQLException if query fails
     */
    public String getName(int bikeId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT name FROM bike WHERE bike_id=?");
            ps.setInt(1, bikeId);
            rs = ps.executeQuery();
            String name = "";
            if (rs.next()) {
                name = rs.getString("name");
            }
            return name;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * Sets a new dock_name for a bike
     *
     * @param bikeId of the bike you want to change dock_name for
     * @param name The new docking station name for the bike
     * @return true if successfully setting a new dock_name
     * @throws SQLException if query fails
     */
    public boolean setName(int bikeId, String name) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("UPDATE bike SET name=? WHERE bike_id=?");
            ps.setString(1, name);
            ps.setInt(2, bikeId);
            int res = ps.executeUpdate();
            return res == 1;
        } finally {
            if(ps != null){ps.close();}
        }
    }

    /**
     * Method to get a bikes chargelevel, with given bikeId
     *
     * @param bikeId of the bike you want chargelevel from
     * @return int with chargelevel, between 0 and 100
     * @throws SQLException if query fails
     */
    public int getCharge(int bikeId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT charge FROM bike WHERE bike_id=?");
            ps.setInt(1,bikeId);
            rs = ps.executeQuery();
            int charge = -1;
            if (rs.next()) {
                charge = rs.getInt("charge");
            }
            return charge;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * Set chargelevel for the bike, with given bikeId
     *
     * @param bikeId of the bike that should have a new chargelevel set
     * @param charge the new chargelevel
     * @return true if new chargelevel is successfully set
     * @throws SQLException if update fails
     */
    public boolean setCharge(int bikeId, int charge) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("UPDATE bike SET charge=? WHERE bike_id=?");
            ps.setInt(1, charge);
            ps.setInt(2, bikeId);
            int res = ps.executeUpdate();
            return res == 1;
        } finally {
            if(ps != null){ps.close();}
        }
    }

    /**
     * Method to get the position of bike, with given bikeId
     *
     * @param bikeId of the bike you want position from
     * @return Positon object with position for requested bike
     * @throws SQLException if query fails
     */
    public Position getPosition(int bikeId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT x_coordinate, y_coordinate FROM bike WHERE bike_id=?");
            ps.setInt(1, bikeId);
            rs = ps.executeQuery();
            Position p = null;
            if(rs.next()){
                p = new Position();
                p.setxCoordinate(rs.getDouble("x_coordinate"));
                p.setyCoordinate(rs.getDouble("y_coordinate"));
            }
            return p;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }

    }

    public boolean setMaintenance(int bikeId, boolean needMaintenance) throws SQLException{
        PreparedStatement ps = null;
        try{
            ps =connection.prepareStatement("UPDATE bike SET need_maintenance =? WHERE bike_id=?");
            ps.setBoolean(1,needMaintenance);
            ps.setInt(2, bikeId);
            int res = ps.executeUpdate();
            return res ==1;
        }finally{
            if(ps != null){ps.close();}
        }
    }

    /**
     * Method to set new position for bike, with given bikeId
     *
     * @param bikeId of the the bike that should get a new position
     * @param position Position object with new position for bike
     * @return true if new position is successfully set
     * @throws SQLException if updating position fails
     */
    public boolean setPosition(int bikeId, Position position) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("UPDATE bike SET x_coordinate=?, y_coordinate=? WHERE bike_id=?");
            ps.setDouble(1, position.getxCoordinate());
            ps.setDouble(2, position.getyCoordinate());
            ps.setInt(3, bikeId);
            int res = ps.executeUpdate();
            return res == 1;
        } finally {
            if(ps != null){ps.close();}
        }
    }

    /**
     * Method to calculate total price of all bikes
     *
     * @return double with the total price of all bikes
     * @throws SQLException if query fails
     */
    public double getTotalPrice() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT CAST((SUM(price)) AS DECIMAL(21,2)) AS totalprice FROM bike");
            rs = ps.executeQuery();
            double totalPrice = -1;
            if (rs.next()) {
                totalPrice = rs.getDouble("totalprice");
            }
            return totalPrice;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * Method to calculate average price of all bikes
     *
     * @return double with the average price of all bikes
     * @throws SQLException if query fails
     */
    public double getAvgPrice() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT CAST((AVG(price)) AS DECIMAL(10,2)) AS averageprice FROM bike");
            rs = ps.executeQuery();
            double averagePrice = -1;
            if (rs.next()) {
                averagePrice = rs.getDouble("averageprice");
            }
            return averagePrice;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * Method to get total price of bikes bought between two dates
     *
     * @param from date that should be calculated from
     * @param to   date that should be calculated to
     * @return double with total price of bikes bought between the two dates
     * @throws SQLException if query fails
     */
    public double getTotalPriceBetween(LocalDate from, LocalDate to) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT CAST((SUM(price)) AS DECIMAL(21,2)) AS totalprice FROM bike WHERE date_bought BETWEEN ? and ?");
            ps.setDate(1, Date.valueOf(from));
            ps.setDate(2, Date.valueOf(to));
            rs = ps.executeQuery();
            double totalPrice = -1;
            if (rs.next()) {
                totalPrice = rs.getDouble("totalprice");
            }
            return totalPrice;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * Method to calculate average price of bikes bought between two dates
     *
     * @param from date to calculate from
     * @param to date to calculate to
     * @return double average price of all bikes bought between the two dates
     * @throws SQLException if query fails
     */
    public double getAvgPriceBetween(LocalDate from, LocalDate to) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT CAST((AVG(price)) AS DECIMAL(10,2)) AS averageprice FROM bike WHERE date_bought BETWEEN ? and ?");
            ps.setDate(1, Date.valueOf(from));
            ps.setDate(2, Date.valueOf(to));
            rs = ps.executeQuery();
            double averagePrice = -1;
            if (rs.next()) {
                averagePrice = rs.getDouble("averageprice");
            }
            return averagePrice;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * Method preventing endless auto incrementing bike_id by setting the auto increment to chose the
     * max bike_id+1 for next bike
     *
     * @throws SQLException if failing
     */
    private void autoIncrementFix() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT MAX(bike_id)+1 FROM bike");
            rs = ps.executeQuery();
            if (rs.next()) {
                ps = connection.prepareStatement("ALTER TABLE bike AUTO_INCREMENT=?");
                ps.setInt(1, rs.getInt("MAX(bike_id)+1"));
            }
            ps.executeUpdate();
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}

        }
    }

    /**
     * Method filling up arraylist of bikes with bike objects
     *
     * @param bikes the Arraylist of bikes that should be filled with bikes from the database
     * @throws SQLException if failing
     */
    private void fillArrayList(ArrayList<Bike> bikes, ResultSet rs) throws SQLException {
        while (rs.next()) {
            Bike b = new Bike();
            b.setBikeId(rs.getInt("bike_id"));
            b.setName(rs.getString("name"));
            b.setNeedMaintenance(rs.getBoolean("need_maintenance"));
            b.setType(rs.getString("type"));
            b.setPrice(rs.getDouble("price"));
            b.setDateBought(rs.getDate("date_bought"));
            b.setCharge(rs.getInt("charge"));
            b.setxCoordinate(rs.getDouble("x_coordinate"));
            b.setyCoordinate(rs.getDouble("y_coordinate"));
            b.setMake(rs.getString("make"));
            bikes.add(b);
        }
    }
}
