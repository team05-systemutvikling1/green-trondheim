package main.java.db;

import main.java.data.Workshop;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Data Access Object for Workshop
 *
 * @author Magne
 */
public class WorkshopDAO {
    private Connection connection;

    /**
     * Creates a connection to the database for WorkshopDAO
     *
     * @throws SQLException if connection fails
     */
    public WorkshopDAO() throws SQLException {
        connection = Db.instance().getConnection();
    }

    /**
     * Method that gives access to a Workshop object, given an org_nr
     *
     * @param orgNr of Workshop that should be returned
     * @return Workshop object with given org_nr
     * @throws SQLException if failing to get Workshop
     */
    public Workshop getWorkshop(int orgNr) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT * FROM workshop WHERE org_nr=?");
            ps.setInt(1, orgNr);
            rs = ps.executeQuery();
            Workshop w = null;
            if (rs.next()) {
                w = new Workshop();
                w.setOrgNr(rs.getInt("org_nr"));
                w.setOrgName(rs.getString("org_name"));
                w.setPhoneNr(rs.getInt("phone_nr"));
                w.seteMail(rs.getString("e_mail"));
                w.setAddress(rs.getString("address"));
            }
            return w;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * Returns an ArrayList of all registered workshops
     *
     * @return ArrayList of all workshops
     * @throws SQLException if query fails
     */
    public ArrayList<Workshop> getAllWorkshops() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT * FROM workshop");
            rs = ps.executeQuery();
            ArrayList<Workshop> workshops = new ArrayList<>();
            while (rs.next()) {
                Workshop w = new Workshop();
                w.setOrgNr(rs.getInt("org_nr"));
                w.setOrgName(rs.getString("org_name"));
                w.setPhoneNr(rs.getInt("phone_nr"));
                w.seteMail(rs.getString("e_mail"));
                w.setAddress(rs.getString("address"));
                workshops.add(w);
            }
            return workshops;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * Adds a workshop to the database, given a Workshop object
     *
     * @param w An object of the Workshop that should be added
     * @return true if successfully added
     * @throws SQLException if query fails
     */
    public boolean addWorkshop(Workshop w) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("INSERT INTO workshop (org_nr, org_name, phone_nr, e_mail, address) VALUES(?,?,?,?,?)");
            ps.setInt(1, w.getOrgNr());
            ps.setString(2, w.getOrgName());
            ps.setInt(3, w.getPhoneNr());
            ps.setString(4, w.geteMail());
            ps.setString(5, w.getAddress());
            int res = ps.executeUpdate();
            return res == 1;
        } finally {
            if(ps != null){ps.close();}

        }
    }

    /**
     * Deletes a workshop from the database, given a valid org_nr
     *
     * @param orgNr of the workshop you want to delete
     * @return true if successfully deleted
     * @throws SQLException if query fails
     */
    public boolean delWorkshop(int orgNr) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("DELETE FROM workshop WHERE org_nr=?");
            ps.setInt(1, orgNr);
            int res = ps.executeUpdate();
            return res == 1;
        } finally {
            if(ps != null){ps.close();}

        }
    }

    /**
     * Updates the workshop with the same org_nr as the Workshop object given as input
     *
     * @param w The workshop with new and updated details
     * @return true if successfully updated
     * @throws SQLException if query fails
     */
    public boolean updateWorkshop(Workshop w) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("UPDATE workshop SET org_name=?, phone_nr=?, e_mail=?, address=? WHERE org_nr=?");
            ps.setString(1, w.getOrgName());
            ps.setInt(2, w.getPhoneNr());
            ps.setString(3, w.geteMail());
            ps.setString(4, w.getAddress());
            ps.setInt(5, w.getOrgNr());
            int res = ps.executeUpdate();
            return res == 1;
        } finally {
            if(ps != null){ps.close();}
        }
    }
}
