package main.java.db;

import main.java.data.Bike;
import main.java.data.BikeLog;
import main.java.data.Position;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 * Data Access Object for BikeLog
 *
 * @author Magne
 */
public class BikeLogDAO {
    private Connection connection;
    private BikeDAO bikeDAO;

    /**
     * Creates a connection to the database for BikeLogDAO
     *
     * @throws SQLException if connecting to DB fails.
     */
    public BikeLogDAO() throws SQLException {
        connection = Db.instance().getConnection();
    }

    /**
     * Adds a bikelog entry, given a bikeId
     *
     * @param bikeId of the bike that should have a new entry in bikeLog
     * @return true if BikeLog successfully added
     * @throws SQLException if query fails
     */
    public boolean addBikeLog(int bikeId) throws SQLException {
        bikeDAO = new BikeDAO();
        Bike b = bikeDAO.getBike(bikeId);
        int charge = b.getCharge();
        double xPos = b.getxCoordinate();
        double yPos = b.getyCoordinate();
        return addBikeLogEntry(bikeId, charge, xPos, yPos);
    }

    /**
     * Manually add bikeLog entry
     *
     * @param bikeId   of the bike that should get a new entry in the log
     * @param charge   from bike that should get new entry
     * @param position from bike that should get new entry
     * @return true if successfully added
     * @throws SQLException
     */
    public boolean addBikeLog(int bikeId, int charge, Position position) throws SQLException {
        return addBikeLogEntry(bikeId, charge, position.getxCoordinate(), position.getyCoordinate());
    }

    /**
     * Return a bikelog for the for the bike with given bikeId
     *
     * @param bikeId of the bike you want bikeLog from
     * @return ArrayList of bikelogs for selected bike
     * @throws SQLException if query fails
     */
    public ArrayList<BikeLog> getBikeLog(int bikeId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT * FROM bikelog WHERE bike_id=?");
            ps.setInt(1, bikeId);
            rs = ps.executeQuery();
            ArrayList<BikeLog> log = new ArrayList<>();
            while (rs.next()) {
                BikeLog bl = new BikeLog();
                bl.setLogId(rs.getInt("log_id"));
                bl.setBikeId(rs.getInt("bike_id"));
                bl.setCharge(rs.getInt("charge"));
                bl.setxPos(rs.getDouble("x_coordinate"));
                bl.setyPos(rs.getDouble("y_coordinate"));
                bl.setTime(rs.getTimestamp("time"));
                log.add(bl);
            }
            return log;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * Helping method to add bikelogEntry
     *
     * @param bikeId of the bike
     * @param charge current charge on bike
     * @param xPos current position for bike
     * @param yPos current position for bike
     * @return true if successfully added
     * @throws SQLException if update fails
     */
    private boolean addBikeLogEntry(int bikeId, int charge, double xPos, double yPos) throws SQLException {
        LocalDateTime time = LocalDateTime.now();
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("INSERT INTO bikelog (bike_id, charge, x_coordinate, y_coordinate, time) VALUES (?,?,?,?,?)");
            ps.setInt(1, bikeId);
            ps.setInt(2, charge);
            ps.setDouble(3, xPos);
            ps.setDouble(4, yPos);
            ps.setTimestamp(5, Timestamp.valueOf(time));
            int res = ps.executeUpdate();
            return res == 1;
        } finally {
            if(ps != null){ps.close();}
        }
    }
}
