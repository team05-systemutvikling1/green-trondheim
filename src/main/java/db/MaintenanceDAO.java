package main.java.db;

import main.java.data.Maintenance;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Data Access Object for Maintenance
 *
 * @author Magne
 */
public class MaintenanceDAO {
    private  Connection connection;

    /**
     * Creates a connection to the database for MaintenanceDAO
     *
     * @throws SQLException if connection fails
     */
    public MaintenanceDAO() throws SQLException {
        connection = Db.instance().getConnection();
    }

    /**
     * Method that gives access to a Maintenance object, given a repOrder
     *
     * @param repOrder of Maintenance that should be returned
     * @return Maintenance object with given repOrder
     * @throws SQLException if failing to get maintenance
     */
    public Maintenance getMaintenance(int repOrder) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT * FROM maintenance WHERE rep_order=?");
            ps.setInt(1, repOrder);
            rs = ps.executeQuery();
            return createMaintenance(rs);
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * Returns an ArrayList of all registered maintenances
     *
     * @return ArrayList of maintenances
     * @throws SQLException if query fails
     */
    public ArrayList<Maintenance> getAllMaintenances() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT * FROM maintenance");
            rs = ps.executeQuery();
            ArrayList<Maintenance> maintenances = new ArrayList<>();
            fillArrayList(maintenances, rs);
            return maintenances;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * Returns an ArrayList of all maintenances where startDate and endDate is not set
     *
     * @return ArrayList of all maintenances not started
     * @throws SQLException if query fails
     */
    public ArrayList<Maintenance> getAllNotStarted() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT * FROM maintenance WHERE (startDate IS NULL) AND (endDate IS NULL)");
            rs = ps.executeQuery();
            ArrayList<Maintenance> maintenances = new ArrayList<>();
            fillArrayList(maintenances, rs);
            return maintenances;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}

        }
    }

    /**
     * Returns an ArrayList of all maintenances where startDate is set, and endDate is not set
     *
     * @return ArrayList of all maintenances that are started
     * @throws SQLException if query fails
     */
    public ArrayList<Maintenance> getAllStarted() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT * FROM maintenance WHERE (startDate IS NOT NULL AND endDate IS NULL)");
            rs = ps.executeQuery();
            ArrayList<Maintenance> maintenances = new ArrayList<>();
            fillArrayList(maintenances, rs);
            return maintenances;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * Returns an ArrayList of all maintenances startDate and endDate are set
     *
     * @return Arraylist of all finished maintenances
     * @throws SQLException if query fails
     */
    public ArrayList<Maintenance> getAllFinished() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT * FROM maintenance WHERE (startDate AND endDate) IS NOT NULL");
            rs = ps.executeQuery();
            ArrayList<Maintenance> maintenances = new ArrayList<>();
            fillArrayList(maintenances, rs);
            return maintenances;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * Get total price of all maintenances
     *
     * @return total price of all maintenance
     * @throws SQLException if query fails
     */
    public double getTotalPrice() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT CAST((SUM(price)) AS DECIMAL(21,2)) AS totalprice FROM maintenance");
            rs = ps.executeQuery();
            double totalPrice = -1;
            if (rs.next()) {
                totalPrice = rs.getDouble("totalprice");
            }
            return totalPrice;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * Get average price of all maintenances
     *
     * @return average price of all maintenances
     * @throws SQLException if query fails
     */
    public double getAvgPrice() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT CAST((AVG(price)) AS DECIMAL(10,2)) AS averageprice FROM maintenance");
            rs = ps.executeQuery();
            double averagePrice = -1;
            if (rs.next()) {
                averagePrice = rs.getDouble("averageprice");
            }
            return averagePrice;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * Get total price of maintenances between two dates
     *
     * @param from date to calculate from
     * @param to   date to calculate to
     * @return total price of maintenances between given dates
     * @throws SQLException if query fails
     */
    public double getTotalPriceBetween(LocalDate from, LocalDate to) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT CAST((SUM(price)) AS DECIMAL(21,2)) AS totalprice FROM maintenance WHERE endDate BETWEEN ? and ?");
            ps.setDate(1, Date.valueOf(from));
            ps.setDate(2, Date.valueOf(to));
            rs = ps.executeQuery();
            double totalPrice = -1;
            if (rs.next()) {
                totalPrice = rs.getDouble("totalprice");
            }
            return totalPrice;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * Calculate average price between two dates
     *
     * @param from date to calculate from
     * @param to date to calculate to
     * @return average price used on maintenenaces between the given dates
     * @throws SQLException
     */
    public double getAvgPriceBetween(LocalDate from, LocalDate to) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT CAST((AVG(price)) AS DECIMAL(10,2)) AS averageprice FROM maintenance WHERE endDate BETWEEN ? and ?");
            ps.setDate(1, Date.valueOf(from));
            ps.setDate(2, Date.valueOf(to));
            rs = ps.executeQuery();
            double averagePrice = -1;
            if (rs.next()) {
                averagePrice = rs.getDouble("averageprice");
            }
            return averagePrice;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * Adds a maintenance to the database, given a Maintenance object
     *
     * @param m An object of the Maintenance that should be added
     * @return true if successfully added
     * @throws SQLException if query fails
     */
    public boolean addMaintenance(Maintenance m) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("INSERT INTO maintenance (rep_order, org_nr, bike_id, price, startDate) VALUES (?,?,?,?,?)");
            ps.setInt(1, m.getRepOrder());
            ps.setInt(2, m.getOrgNr());
            ps.setInt(3, m.getBikeId());
            ps.setDouble(4, m.getPrice());
            ps.setDate(5, m.getStartDate());
            int res = ps.executeUpdate();
            return res == 1;
        } finally {
            if(ps != null){ps.close();}
        }
    }

    /**
     * Deletes a maintenance from the database, given a valid repOrder
     *
     * @param repOrder of the maintenance that should be deleted
     * @return true if successfully deleted
     * @throws SQLException if query fails
     */
    public boolean delMaintenance(int repOrder) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("DELETE FROM maintenance WHERE rep_order=?");
            ps.setInt(1, repOrder);
            int res = ps.executeUpdate();
            return res == 1;
        } finally {
            if(ps != null){ps.close();}
        }
    }

    /**
     * Sets documentation for the maintenance with given repOrder
     *
     * @param repOrder of the maintenance you want to add new documentation to
     * @param doc the new documentation that should be added
     * @return true if documentation is successfully added
     * @throws SQLException if query fails
     */
    public boolean setDoc(int repOrder, String doc) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("UPDATE maintenance SET documentation=? WHERE rep_order=?");
            ps.setString(1, doc);
            ps.setInt(2, repOrder);
            int res = ps.executeUpdate();
            return res == 1;
        } finally {
            if(ps != null){ps.close();}
        }
    }

    /**
     * Sets a start date for the maintenance with given repOrder
     *
     * @param repOrder of the maintenance that should have a start date set
     * @param startDate date that should be set as start date
     * @return true if startDate successfully set
     * @throws SQLException if query fails
     */
    public boolean setStartDate(int repOrder, LocalDate startDate) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("UPDATE maintenance SET startDate=? WHERE rep_order=?");
            ps.setDate(1, Date.valueOf(startDate));
            ps.setInt(2, repOrder);
            int res = ps.executeUpdate();
            return res == 1;
        } finally {
            if(ps != null){ps.close();}
        }
    }

    /**
     * Sets a end date for the maintenance with given repOrder
     *
     * @param repOrder of the maintenance that should have a end date set
     * @param endDate date that should be set as end date
     * @return true if endDate successfully set
     * @throws SQLException if query fails
     */
    public boolean setEndDate(int repOrder, LocalDate endDate) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("UPDATE maintenance SET endDate=? WHERE rep_order=?");
            ps.setDate(1, Date.valueOf(endDate));
            ps.setInt(2, repOrder);
            int res = ps.executeUpdate();
            return res == 1;
        } finally {
            if(ps != null){ps.close();}
        }
    }

    /**
     * Method to easily create a Maintenance object from a ResultSet
     *
     * @param rs that should be used to create a Maintenance object
     * @return the created Maintenance object
     */
    private Maintenance createMaintenance(ResultSet rs) {
        try {
            Maintenance m = new Maintenance();
            if (rs.next()) {
                m.setRepOrder(rs.getInt("rep_order"));
                m.setOrgNr(rs.getInt("org_nr"));
                m.setBikeId(rs.getInt("bike_id"));
                m.setPrice(rs.getDouble("price"));
                if (rs.getString("documentation") != null) m.setDoc(rs.getString("documentation"));
                if (rs.getDate("startDate") != null) m.setStartDate(rs.getDate("startDate"));
                if (rs.getDate("endDate") != null) m.setEndDate(rs.getDate("endDate"));
            }
            return m;
        } catch (SQLException e) {
            System.out.println("Could not find requested maintenance entry");
            return null;
        }
    }

    /**
     * Method to fill an ArrayList with maintenance objects
     *
     * @param maintenances the ArrayList that should be filled
     * @throws SQLException if ResultSet is faulty
     */
    private void fillArrayList(ArrayList<Maintenance> maintenances, ResultSet rs) throws SQLException {
        while (rs.next()) {
            Maintenance m = new Maintenance();
            m.setRepOrder(rs.getInt("rep_order"));
            m.setOrgNr(rs.getInt("org_nr"));
            m.setBikeId(rs.getInt("bike_id"));
            m.setPrice(rs.getDouble("price"));
            if (rs.getString("documentation") != null) m.setDoc(rs.getString("documentation"));
            if (rs.getDate("startDate") != null) m.setStartDate(rs.getDate("startDate"));
            if (rs.getDate("endDate") != null) m.setEndDate(rs.getDate("endDate"));
            maintenances.add(m);
        }
    }
}
