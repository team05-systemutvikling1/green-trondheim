package main.java.db;

import main.java.data.CardInfo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Data Access Object for CardInfo
 *
 * @author Rune Vedøy
 */
public class CardInfoDAO {
    private Connection connection;

    /**
     * Creates a connection to the database for CardInfoDAO.
     *
     * @throws SQLException if connection fails.
     */
    public CardInfoDAO() throws SQLException{
        connection = Db.instance().getConnection();
    }

    /**
     * Returns card information based on the given national id
     *
     * @param nationalId of cardholder to be returned.
     * @return Card information given nationalId
     * @throws SQLException if failing to get card information.
     */
    public CardInfo getCardInfo(long nationalId) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            ps = connection.prepareStatement("SELECT * FROM card_info WHERE national_id=?");
            ps.setLong(1,nationalId);
            rs = ps.executeQuery();
            CardInfo cardInfo = null;
            if (rs.next()){
                cardInfo = new CardInfo();
                cardInfo.setFirstName(rs.getString("cardholder_firstname"));
                cardInfo.setLastName(rs.getString("cardholder_lastname"));
                cardInfo.setNationalId(rs.getLong("national_id"));
                cardInfo.setCardNr(rs.getLong("card_nr"));
                cardInfo.setCardExpirationDate(rs.getInt("expiration_date"));
            }
            return cardInfo;
        }finally {
            if(rs != null){rs.close();}
            if(rs != null){ps.close();}
        }
    }

    /**
     * Returns all cardinfo in the database.
     *
     * @return an arraylist of all the cardinfo.
     * @throws SQLException if query fails.
     */
    public ArrayList<CardInfo> getAllCardInfo()throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            ps = connection.prepareStatement("SELECT * FROM card_info");
            rs = ps.executeQuery();
            ArrayList<CardInfo> cardInfos = new ArrayList<>();
            while (rs.next()){
                CardInfo cardInfo = new CardInfo();
                cardInfo.setFirstName(rs.getString("cardholder_firstname"));
                cardInfo.setLastName(rs.getString("cardholder_lastname"));
                cardInfo.setNationalId(rs.getLong("national_id"));
                cardInfo.setCardNr(rs.getLong("card_nr"));
                cardInfo.setCardExpirationDate(rs.getInt("expiration_date"));
                cardInfos.add(cardInfo);
            }
            return cardInfos;
        }finally {
            if(rs != null){rs.close();}
            if(rs != null){ps.close();}
        }
    }

    /**
     * Adds card details to the database given a CardInfo object.
     *
     * @param cardInfo An object of CardInfo that is to be added.
     * @return true if successfully added, false if not added.
     * @throws SQLException if query fails.
     */
    public boolean addCardInfo( CardInfo cardInfo)throws SQLException{
        PreparedStatement ps = null;
        try{
            ps = connection.prepareStatement("INSERT INTO card_info(national_id, card_nr, cardholder_firstname, cardholder_lastname, expiration_date) VALUES(?,?,?,?,?)");
            ps.setLong(1,cardInfo.getNationalId());
            ps.setLong(2,cardInfo.getCardNr());
            ps.setString(3,cardInfo.getFirstName());
            ps.setString(4,cardInfo.getLastName());
            ps.setInt(5,cardInfo.getCardExpirationDate());
            int res = ps.executeUpdate();
            return res == 1;
        }finally {
            if (ps != null){
                ps.close();
            }
        }
    }

    /**
     * Deletes card details from the database given the national id
     *
     * @param nationalId The national id of the card holder whose details are to be deleted.
     * @return true if deleted, false if not deleted.
     * @throws SQLException if the query fails.
     */
    public boolean delCardInfo(long nationalId) throws SQLException{
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("DELETE FROM card_info WHERE national_id=?");
            ps.setLong(1,nationalId);
            int res = ps.executeUpdate();
            return res ==1;
        }finally {
            if (ps != null){
                ps.close();
            }
        }
    }

    /**
     * Updates card information given a CardInfo object, an already existing cardNr
     *
     * @param cardInfo CardInfo object with new cardInfo
     * @param nationalId of the old CardInfo object
     * @return true if successfully updated
     * @throws SQLException if update fails
     */
    public boolean updateCardInfo(CardInfo cardInfo, long nationalId) throws SQLException{
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("UPDATE kevinah.card_info SET  card_nr=?, cardholder_firstname=?, cardholder_lastname=?, expiration_date=? WHERE national_id=?");
            ps.setLong(1,cardInfo.getCardNr());
            ps.setString(2,cardInfo.getFirstName());
            ps.setString(3,cardInfo.getLastName());
            ps.setInt(4,cardInfo.getCardExpirationDate());
            ps.setLong(5,nationalId);
            int res = ps.executeUpdate();
            return res == 1;
        }finally {
            if (ps != null){
                ps.close();
            }
        }
    }

}
