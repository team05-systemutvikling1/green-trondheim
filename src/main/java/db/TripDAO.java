package main.java.db;

import main.java.data.Trip;

import java.sql.*;
import java.util.ArrayList;

/**
 * Data Access Object for Trip object
 *
 * @author Kevin
 * @author Magne
 */
public class TripDAO {
    private  Connection connection;

    /**
     *Creating a connection
     *
     * @throws SQLException if connection fails
     */
    public TripDAO() throws SQLException{
        connection = Db.instance().getConnection();

    }

    /**
     * Method that will get one specific trip from the database
     *
     * @param tripID Is the ID of the trip you want to get
     *
     * @return a trip object that has the same attributes as it does in the database. Returns NULL if no objects were found
     *
     * @throws SQLException if connection issues were to occur
     */
    public Trip getTrip(int tripID)throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT * FROM trip WHERE trip_id = ?;");
            ps.setInt(1, tripID);
            rs = ps.executeQuery();
            Trip t = null;
            if(rs.next()){
                t = createTrip(rs);
            }
            return t;

        }finally{
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}

        }
    }

    /**
     * This is a method that will add a trip to the database
     * @param newTrip is the trip object we want to add
     * @return true if it worked
     * @throws SQLException if the connection failed
     */
    public boolean addTrip(Trip newTrip) throws SQLException{
        PreparedStatement ps = null;
        try{
            ps = connection.prepareStatement("INSERT INTO trip(e_mail, bike_id, length, time_start, time_end) VALUES(?, ?, ?, ?, ?);");
            ps.setString(1, newTrip.geteMail());
            ps.setInt(2, newTrip.getBikeId());
            ps.setDouble(3, newTrip.getLength());
            ps.setTimestamp(4, Timestamp.valueOf(newTrip.getTimeStart()));
            if (newTrip.getTimeEnd() != null) {
                ps.setTimestamp(5, Timestamp.valueOf(newTrip.getTimeEnd()));
            } else {
                ps.setTimestamp(5, null);
            }
            int res = ps.executeUpdate();
            return res == 1;
        }finally {
            if(ps != null){ps.close();}
        }

    }

    /**
     * This method allows us to delete trips
     *
     * @param tripId is the ID of the trip you want to delete
     * @return true if everything works as expected
     * @throws SQLException If we have a connection issue
     */

     public boolean delTrip(int tripId) throws SQLException{
         PreparedStatement ps = null;
         try{
            ps = connection.prepareStatement("DELETE FROM trip WHERE trip_id =?");
            ps.setInt(1, tripId);
            int res = ps.executeUpdate();
            return res ==1;

         }finally{
            if(ps != null){ps.close();}

         }

     }

    /**
     * Method that returns all Trips in database
     * @return an ArrayList containing all trips registered in database
     * @throws SQLException If a connection issue should occur
     */
    public ArrayList<Trip> getAllTrips() throws SQLException {
        ArrayList<Trip> tripList = new ArrayList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT * FROM trip;");
            rs = ps.executeQuery();

            while (rs.next()) {
                tripList.add(createTrip(rs));

            }
            return tripList;

        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * Method to get number of trips for a bike, given the bikeId
     *
     * @param bikeId of the bike you want to get amount of trips from
     * @return amount of trips done with a bike, with given bikeId, -1 if something goes wrong
     * @throws SQLException if query fails
     */
    public int getAmountTrips(int bikeId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT COUNT(trip_id) AS amount FROM trip WHERE bike_id=?");
            ps.setInt(1, bikeId);
            rs = ps.executeQuery();
            int amount = -1;
            if (rs.next()) {
                amount = rs.getInt("amount");
            }
            return amount;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * Method to return total km a bike has been used, given a bikeId
     *
     * @param bikeId of the bike you want total length of all trips from
     * @return total length of all trips, with given bikeId
     * @throws SQLException if query fails
     */
    public double getTotalLengthTrips(int bikeId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT CAST((SUM(length)) AS DECIMAL(10,1)) AS amount FROM trip WHERE bike_id=?");
            ps.setInt(1, bikeId);
            rs = ps.executeQuery();
            double totalKm = -1;
            if (rs.next()) {
                totalKm = rs.getDouble("amount");
            }
            return totalKm;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * Return average length of trips for given bikeid
     *
     * @param bikeId of bike that you want average length from
     * @return average length per trip for given bike
     * @throws SQLException if query fails
     */
    public double getAvgLengthTrips(int bikeId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT CAST((AVG(length)) AS DECIMAL(10,1)) AS amount FROM trip WHERE bike_id=?");
            ps.setInt(1, bikeId);
            rs = ps.executeQuery();
            double totalKm = -1;
            if (rs.next()) {
                totalKm = rs.getDouble("amount");
            }
            return totalKm;
        } finally {
            if(rs != null){rs.close();}
            if(ps != null){ps.close();}
        }
    }

    /**
     * A helping method used to create a trip object based on the query result
     *
     * @param rs is the ResultSet that you want to crate an object through
     *
     * @return a Trip object that represents the result set from the database
     *
     * @throws SQLException If a connection issue were to occur
     */
    private Trip createTrip(ResultSet rs) throws SQLException{
        try {
            Trip t = new Trip();
            t.setTripId(rs.getInt("trip_id"));
            t.seteMail(rs.getString("e_mail"));
            t.setBikeId(rs.getInt("bike_id"));
            t.setLength(rs.getDouble("length"));
            t.setTimeStart(rs.getTimestamp("time_start"));

            if (rs.getTimestamp("time_end") != null) {
                t.setTimeEnd(rs.getTimestamp("time_end"));
            }

            return t;
        }catch(SQLException e){
            throw new SQLException("Connection issues..." + e);

        }

    }

}
