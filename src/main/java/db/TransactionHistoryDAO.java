package main.java.db;

import main.java.data.TransactionHistory;

import java.sql.*;
import java.util.ArrayList;


/**
 * Data Access Object for TransactionHistory
 *
 * @author Jan-Marius
 */
public class TransactionHistoryDAO {
    private Connection connection;

    /**
     * Creates a connection to the database for TransactionHistoryDAO
     *
     * @throws SQLException if connection to DB fails.
     */
    public TransactionHistoryDAO() throws SQLException {
        connection = Db.instance().getConnection();
    }

    /**
     * Method that gives access to a TransactionHistory object, given a transactionNr
     *
     * @param transactionNr the transactionNr for object you want returned
     * @return Transaction History object with given transactionNr
     * @throws SQLException if query fails to get TransactionHistory
     */
    public TransactionHistory getTransactionHistory(int transactionNr) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT * FROM transaction_history WHERE transaction_nr=?"); // PS: transaction_nr may be changed in the future.
            ps.setInt(1, transactionNr);
            rs = ps.executeQuery();
            TransactionHistory tH = null;
            if (rs.next()) {
                tH = new TransactionHistory();
                tH.setTransactionNr(rs.getInt("transaction_nr"));
                tH.setNationalId(rs.getLong("national_id"));
                tH.setTranTime(rs.getTimestamp("tran_time"));
                tH.setAmount(rs.getDouble("amount"));
            }
            return tH;
        } finally {
            if(rs != null){rs.close();}
            if(rs != null){ps.close();}
        }
    }

    /**
     * Returns an Arraylist of all the transaction histories registered in the system.
     *
     * @return Arraylist of all transaction histories
     * @throws SQLException if query fails
     */

    public ArrayList<TransactionHistory> getAllTransactionHistory() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT * FROM transaction_history");
            rs = ps.executeQuery();
            ArrayList<TransactionHistory> transactionHistories = new ArrayList<>();
            fillArrayList(transactionHistories, rs);
            return transactionHistories;
        } finally {
            if(rs != null){rs.close();}
            if(rs != null){ps.close();}
        }
    }

    /**
     * Adds a transaction history to the system, given a transaction history object.
     *
     * @param tH Transaction History object that should be added
     * @return true if successfully added
     * @throws SQLException if query fails
     */
    public boolean addTransactionHistory(TransactionHistory tH) throws SQLException {
        PreparedStatement ps = null;
        try {
            autoIncrementFix();
            ps = connection.prepareStatement("INSERT INTO transaction_history (national_id, tran_time, amount) VALUES(?,?,CAST(? AS DECIMAL(10,2)))");
            ps.setLong(1, tH.getNationalId());
            ps.setTimestamp(2, Timestamp.valueOf(tH.getTranTime()));
            ps.setDouble(3, tH.getAmount());
            int res = ps.executeUpdate();
            return res == 1;
        } finally {
            if(ps != null){ps.close();}
        }
    }


    /**
     * Deletes Transaction History from the system, with given transactionNr.
     *
     * @param transactionNr of the transaction history you want to delete.
     * @return true if successfully
     * @throws SQLException if query fails
     */
    public boolean delTransactionHistory(int transactionNr) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("DELETE FROM transaction_history WHERE transaction_nr=?");
            ps.setInt(1, transactionNr);
            int res = ps.executeUpdate();
            return res == 1;
        } finally {
            if(ps != null){ps.close();}
        }
    }

    /**
     * Updates the transaction history
     *
     * @param transactionId of the transactionHistory that should be updated
     * @param tH TransactionHistory object with new details
     * @return true if successfully added
     * @throws SQLException if query fails
     */

    public boolean updateTransactionHistory(int transactionId, TransactionHistory tH) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("UPDATE transaction_history SET national_id=?, tran_time=?, amount=? WHERE transaction_nr=?");
            ps.setLong(1, tH.getNationalId());
            ps.setTimestamp(2, Timestamp.valueOf(tH.getTranTime()));
            ps.setDouble(3, tH.getAmount());
            ps.setInt(4, transactionId);
            int res = ps.executeUpdate();
            return res == 1;
        } finally {
            if(ps != null){ps.close();}
        }
    }


    /**
     * Method preventing endless auto incrementing transactionNr by setting the auto increment to chose the
     * max transactionNr+1 for next transaction
     *
     * @throws SQLException if failing
     */

    private void autoIncrementFix() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("SELECT MAX(transaction_nr)+1 FROM transaction_history");
            rs = ps.executeQuery();
            if (rs.next()) {
                ps = connection.prepareStatement("ALTER TABLE transaction_history AUTO_INCREMENT=?");
                ps.setInt(1, rs.getInt("MAX(transaction_nr)+1"));
            }
            ps.executeUpdate();
        } finally {
            if(rs != null){rs.close();}
            if(rs != null){ps.close();}
        }
    }

    /**
     * Method filling up arraylist of transaction histories with transaction history objects
     *
     * @param transactionHistories Arraylist that should be filled
     * @param rs ResultSet containing the data needed to fill ArrayList
     * @throws SQLException if failing
     */

    private void fillArrayList(ArrayList<TransactionHistory> transactionHistories, ResultSet rs) throws SQLException {
        while (rs.next()) {
            TransactionHistory tH = new TransactionHistory();
            tH.setTransactionNr(rs.getInt("transaction_nr"));
            tH.setNationalId(rs.getLong("national_id"));
            tH.setTranTime(rs.getTimestamp("tran_time"));
            tH.setAmount(rs.getDouble("amount"));
            transactionHistories.add(tH);
        }
    }
}



