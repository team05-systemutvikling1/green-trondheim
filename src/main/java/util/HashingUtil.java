package main.java.util;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.util.Random;

/**
 * Helping methods used for hashing
 *
 * @author Magne
 */
public class HashingUtil {

    /**
     * Method to hash a string, with a given salt
     *
     * @param unhashed Unhashed String that should be hashed
     * @param salt Salt that should be used for the hashing
     * @return
     */
    public static String getHash(String unhashed, String salt) {
        if (salt == null) {
            return null;
        }
        String hashed;
        try {
            // Possible algorithms: MD2, MD5, SHA-1, SHA-224, SHA-256, SHA-384, SHA-512
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-224");
            String pwWithSalt = unhashed + salt;
            messageDigest.update(pwWithSalt.getBytes());
            byte[] digestedBytes = messageDigest.digest();
            hashed = DatatypeConverter.printHexBinary(digestedBytes).toLowerCase(); // Hashing happens here
        } catch (Exception e) {
            System.out.println("Hashing failed");
            return null;
        }
        return hashed;
    }

    /**
     * Method to check if a unhashed password is the correct password, given the salt to be used, and expected hash result
     *
     * @param unhashed Unhashed String to be checked if correct
     * @param salt That should be used when hashing
     * @param hashed Hashed String that is expected result after hashing
     * @return true if unhashed string matches the hashed one after hashing, false if not
     */
    public static boolean isCorrect(String unhashed, String salt, String hashed) {
        return getHash(unhashed, salt).equals(hashed);
    }

    /**
     * Method to make a random String, with length 8
     *
     * @return random String
     */
    public static String getRandomString() {
        final String SALTCHARS = "abcdefghijklmnopqrstuvwxyz1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 8) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        return salt.toString();
    }
}
