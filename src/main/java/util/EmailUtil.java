package main.java.util;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Properties;

public class EmailUtil {
    public static boolean sendNewAdminEmail(String toEmail, String newPassword) {
        try {
            String host = "smtp.gmail.com";
            String user = "team05ntnu@gmail.com";
            String pass = "p4550rd321";
            String to = toEmail;
            String from = "team05ntnu@gmail.com";
            String subject = "Green Trondheim, New Admin";
            String messageText = "You have been given an admin account for the Green Trondheim bike system.\n\n" +
                                "Login details:\n" +
                                "Username: " + toEmail + "\n" +
                                "Password: " + newPassword + "\n\n" +
                                "Please make sure to change your password";
            boolean sessionDebug = false;

            Properties props = System.getProperties();

            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", host);
            props.put("mail.smtp.port", 587);
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.required", "true");

            java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
            Session mailSession = Session.getDefaultInstance(props, null);
            mailSession.setDebug(sessionDebug);
            Message msg = new MimeMessage(mailSession);
            msg.setFrom(new InternetAddress(from));
            InternetAddress address = new InternetAddress(to);
            msg.setRecipient(Message.RecipientType.TO, address);
            msg.setSubject(subject);
            msg.setSentDate(new Date());
            msg.setText(messageText);

            Transport transport = mailSession.getTransport("smtp");
            transport.connect(host, user, pass);
            transport.sendMessage(msg, msg.getAllRecipients());
            transport.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
