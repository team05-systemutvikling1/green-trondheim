package main.java.sim;

import main.java.data.Position;
import main.java.db.*;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Client used to run the simulation with Threads
 *
 * @author Magne
 */
public class SimulationClient {
    public static void main(String[] args) throws SQLException {
        UserDAO userDAO = new UserDAO();
        BikeDAO bikeDAO = new BikeDAO();
        TripDAO tripDAO = new TripDAO();
        BikeLogDAO bikeLogDAO = new BikeLogDAO();
        ChargingLogDAO chargingLogDAO = new ChargingLogDAO();
        DockingStationDAO dockingStationDAO = new DockingStationDAO();
        TransactionHistoryDAO transactionHistoryDAO = new TransactionHistoryDAO();

        ArrayList<Position> positions = new ArrayList<>();
        positions.add(new Position(61.123456, 51.123456));
        positions.add(new Position(62.123456, 52.123456));
        positions.add(new Position(63.123456, 53.123456));
        positions.add(new Position(64.123456, 54.123456));
        positions.add(new Position(56.023233, 45.959595));
        positions.add(new Position(61.123456, 51.123456));
        positions.add(new Position(62.123456, 52.123456));
        positions.add(new Position(63.123456, 53.123456));
        positions.add(new Position(64.123456, 54.123456));
        positions.add(new Position(56.023233, 45.959595));

        // Complete trip simulation:
        new Thread(new SimTripThread(userDAO, bikeDAO, bikeLogDAO, tripDAO, transactionHistoryDAO, dockingStationDAO, "karihansen@hotmail.com",
                "Jernbanestasjonen", "Jernbanestasjonen","Adult", 5, 1.1, positions)).start();
        new Thread(new SimTripThread(userDAO, bikeDAO, bikeLogDAO, tripDAO, transactionHistoryDAO, dockingStationDAO, "karihansen@hotmail.com",
                "Jernbanestasjonen", "Jernbanestasjonen","Adult", 5, 1.1, positions)).start();

        new Thread(new SimTripThread(userDAO, bikeDAO, bikeLogDAO, tripDAO, transactionHistoryDAO, dockingStationDAO, "olanordmann@gmail.com",
                "Jernbanestasjonen", "Jernbanestasjonen","Adult", 5, 1.1, positions)).start();
        new Thread(new SimTripThread(userDAO, bikeDAO, bikeLogDAO, tripDAO, transactionHistoryDAO, dockingStationDAO, "olanordmann@gmail.com",
                "Jernbanestasjonen", "Jernbanestasjonen","Adult", 5, 1.1, positions)).start();

        new Thread(new SimTripThread(userDAO, bikeDAO, bikeLogDAO, tripDAO, transactionHistoryDAO, dockingStationDAO, "karihansen@hotmail.com",
                "Pirbadet", "Jernbanestasjonen","Adult", 5, 1.1, positions)).start();
        new Thread(new SimTripThread(userDAO, bikeDAO, bikeLogDAO, tripDAO, transactionHistoryDAO, dockingStationDAO, "olanordmann@gmail.com",
                "Pirbadet", "Jernbanestasjonen","Adult", 5, 1.1, positions)).start();

        new Thread(new SimTripThread(userDAO, bikeDAO, bikeLogDAO, tripDAO, transactionHistoryDAO, dockingStationDAO, "karihansen@hotmail.com",
                "Jernbanestasjonen", "Pirbadet","Adult", 5, 1.1, positions)).start();
        new Thread(new SimTripThread(userDAO, bikeDAO, bikeLogDAO, tripDAO, transactionHistoryDAO, dockingStationDAO, "olanordmann@gmail.com",
                "Jernbanestasjonen", "Pirbadet","Adult", 10, 2.3, positions)).start();

        new Thread(new SimTripThread(userDAO, bikeDAO, bikeLogDAO, tripDAO, transactionHistoryDAO, dockingStationDAO, "karihansen@hotmail.com",
                "Jernbanestasjonen", "Jernbanestasjonen","Lady", 5, 1.1, positions)).start();
        new Thread(new SimTripThread(userDAO, bikeDAO, bikeLogDAO, tripDAO, transactionHistoryDAO, dockingStationDAO, "karihansen@hotmail.com",
                "Jernbanestasjonen", "Jernbanestasjonen","Child", 5, 1.1, positions)).start();

        // Complete docking station simulation:
        new Thread(new SimDockingStationThread(dockingStationDAO, bikeDAO, chargingLogDAO, 11)).start();
    }
}
