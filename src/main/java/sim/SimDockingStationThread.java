package main.java.sim;

import main.java.data.DockingStation;
import main.java.db.BikeDAO;
import main.java.db.ChargingLogDAO;
import main.java.db.DockingStationDAO;

import java.util.ArrayList;

/**
 * Thread used to simulate charging and power consumption on docking stations
 *
 * @author Magne
 */
public class SimDockingStationThread implements Runnable {

    private DockingStationDAO dockingStationDAO;
    private ChargingLogDAO chargingLogDAO;
    private BikeDAO bikeDAO;

    private int simDuration;

    private final int MINUTE = 6000; // Simulerte minutter

    public SimDockingStationThread(DockingStationDAO dockingStationDAO,
                                   BikeDAO bikeDAO,
                                   ChargingLogDAO chargingLogDAO,
                                   int simDuration) {
        this.dockingStationDAO = dockingStationDAO;
        this.bikeDAO = bikeDAO;
        this.chargingLogDAO = chargingLogDAO;

        this.simDuration = simDuration;
    }

    @Override
    public void run() {
        try {
            System.out.println("Dockingstation simulation started");
            ArrayList<String> dockingStationNames = dockingStationDAO.getAllDockingStationNames();
            for (int i = 0; i < simDuration; i++) {
                System.out.println("-------------------------------------");
                //ArrayList<DockingStation> dockingStations = dockingStationDAO.getAllDockingStations();
                for (String dockingname : dockingStationNames) {
                    //String dockingname = d.getName();
                    int bikesCharging = bikeDAO.getAmountBikesCharging(dockingname);
                    final int POWER_PER_BIKE = 200; // Watts
                    int powerConsumption = bikesCharging*POWER_PER_BIKE;
                    chargingLogDAO.addChargingLog(dockingname, powerConsumption);
                    System.out.println("Dockingstation: " + dockingname + ", Bikes charging: " + bikesCharging +
                            ", Power consumption: " + powerConsumption + "W");
                }
                bikeDAO.chargeAllBikesDocked();
                System.out.println("All docked bikes charged 1%");
                System.out.println("-------------------------------------");
                Thread.sleep(MINUTE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
