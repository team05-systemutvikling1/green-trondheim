package main.java.sim;

import main.java.data.*;
import main.java.db.*;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Random;

/**
 * Thread used to simulate a complete trip
 *
 * @author Magne
 */
public class SimTripThread implements Runnable {

    private UserDAO userDAO;
    private BikeDAO bikeDAO;
    private BikeLogDAO bikeLogDAO;
    private TripDAO tripDAO;
    private TransactionHistoryDAO transactionHistoryDAO;
    private DockingStationDAO dockingStationDAO;

    private String userEmail, dockingname, dockingTo, type;
    private int tripDuration;
    private double tripLength;
    private ArrayList<Position> positions;

    private Random random = new Random();
    private int timeBeforeStart;
    private final int MINUTE = 6000; // Simulated minutes

    public SimTripThread(UserDAO userDAO,
                         BikeDAO bikeDAO,
                         BikeLogDAO bikeLogDAO,
                         TripDAO tripDAO,
                         TransactionHistoryDAO transactionHistoryDAO,
                         DockingStationDAO dockingStationDAO,
                         String userEmail,
                         String dockingname,
                         String dockingTo,
                         String type,
                         int tripDuration,
                         double tripLength,
                         ArrayList<Position> positions) {
        this.userDAO = userDAO;
        this.bikeDAO = bikeDAO;
        this.bikeLogDAO = bikeLogDAO;
        this.tripDAO = tripDAO;
        this.transactionHistoryDAO = transactionHistoryDAO;
        this.dockingStationDAO = dockingStationDAO;

        this.userEmail = userEmail;
        this.dockingname = dockingname;
        this.dockingTo = dockingTo;
        this.type = type;
        this.tripDuration = tripDuration;
        this.tripLength = tripLength;
        this.positions = positions;

        timeBeforeStart = random.nextInt(5000); // = 5 seconds
    }

    @Override
    public void run() {
        try {
            Thread.sleep(timeBeforeStart); // Makes thread start after 0 to 5 seconds
            System.out.println("Trip started after " + timeBeforeStart + " ms.");

            //Saving stuff used for creating trip at end of trip:
            User user = userDAO.getUser(userEmail);
            ArrayList<Bike> bikesAvailable = bikeDAO.getBikesAvailable(dockingname, type);
            int nrOfAvailibleBikes = bikesAvailable.size();
            int selectedBikeId = bikesAvailable.get(random.nextInt(nrOfAvailibleBikes)).getBikeId();
            LocalDateTime startTime = LocalDateTime.now();
            System.out.println("Bike selected: " + selectedBikeId + ", Startime: " + startTime + ", User: " + user);

            //Removing bike from dockingstation, and starting the biketrip
            bikeDAO.setName(selectedBikeId, null);
            int currentCharge = bikeDAO.getCharge(selectedBikeId);
            System.out.println("Bike " + selectedBikeId + " removed from dockingstation");

            //Updating position and charge every minute during trip, and saving to BikeLog
            for (int i = 0; i < tripDuration; i++) {
                Thread.sleep(MINUTE);
                currentCharge--;
                bikeDAO.setCharge(selectedBikeId, currentCharge);
                bikeDAO.setPosition(selectedBikeId, positions.get(i));
                bikeLogDAO.addBikeLog(selectedBikeId, currentCharge, positions.get(i));
                System.out.println("Bike: " + selectedBikeId + ", New position set to: " + positions.get(i) + ", New chargelvl set to: " + currentCharge);
            }

            //Placing bike back into dockingstation:
            bikeDAO.setName(selectedBikeId, dockingTo);
            bikeDAO.setPosition(selectedBikeId, dockingStationDAO.getPosition(dockingTo));
            System.out.println("Bike " + selectedBikeId + " placed back in dockingstation");

            //Saving the trip:
            LocalDateTime endTime = LocalDateTime.now();
            Trip t = new Trip();
            t.seteMail(userEmail);
            t.setBikeId(selectedBikeId);
            t.setLength(tripLength);
            t.setTimeStart(startTime);
            t.setTimeEnd(endTime);
            tripDAO.addTrip(t);
            System.out.println("Trip was created:\n" + t);

            //Payment information saved:
            TransactionHistory tH = new TransactionHistory();
            tH.setNationalId(user.getNationalId());
            tH.setTranTime(Timestamp.valueOf(LocalDateTime.now()));
            long tTime = ChronoUnit.SECONDS.between(startTime, endTime);
            double tripTime = (double) tTime/60;
            tH.setAmount(tripTime*0.5);
            transactionHistoryDAO.addTransactionHistory(tH);
            System.out.println("Transaction added: " + tH);

            //Use if-else statement, and constructor with less parameters to be able to stress test?
            //System.out.println(userDAO.getUser(userEmail) + " " + bikeDAO.getBikesAvailable(dockingname, type).get(0));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
