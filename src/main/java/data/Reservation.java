package main.java.data;

import java.time.LocalDateTime;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Class for Reservation object
 *
 * @author Magne
 */
public class Reservation {
    private int reservationId, tripId;
    private String email, name;
    private LocalDateTime fromTime, toTime;

    public Reservation(String email, int tripId, String name, LocalDateTime fromTime, LocalDateTime toTime) {
        this.email = email;
        this.tripId = tripId;
        this.name = name;
        this.fromTime = fromTime;
        this.toTime = toTime;
    }

    public Reservation() {}

    public int getReservationId() {
        return reservationId;
    }

    public void setReservationId(int reservationId) {
        this.reservationId = reservationId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getTripId() {
        return tripId;
    }

    public void setTripId(int tripId) {
        this.tripId = tripId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getFromTime() {
        return fromTime;
    }

    public void setFromTime(Timestamp fromTime) {
        this.fromTime = fromTime.toLocalDateTime();
    }

    public LocalDateTime getToTime() {
        return toTime;
    }

    public void setToTime(Timestamp toTime) {
        this.toTime = toTime.toLocalDateTime();
    }

    /**
     * Equals method to check if a reservation is equal to another object
     *
     * @param rescheck Object that should be checked if equal
     * @return true if equal, and false if not
     */
    @Override
    public boolean equals(Object rescheck) {
        if (this == rescheck) return true;
        if (!(rescheck instanceof Reservation)) return false;
        Reservation r = (Reservation) rescheck;
        return getReservationId() == r.getReservationId() &&
                getTripId() == r.getTripId() &&
                Objects.equals(getEmail(), r.getEmail()) &&
                Objects.equals(getName(), r.getName()) &&
                Objects.equals(getFromTime(), r.getFromTime()) &&
                Objects.equals(getToTime(), r.getToTime());
    }

    @Override
    public String toString() {
        return "ResID: " + reservationId + ", e-mail: " + email + ", tripId: " + tripId + ", name: " + name +
                ", fromTime: " + fromTime + ", toTime: " + toTime;
    }
}
