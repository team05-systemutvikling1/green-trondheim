package main.java.data;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class BikeLog {
    private int logId, bikeId, charge;
    private double xPos, yPos;
    private LocalDateTime time;

    public BikeLog(int bikeId, int charge, double xPos, double yPos, LocalDateTime time) {
        this.bikeId = bikeId;
        this.charge = charge;
        this.xPos = xPos;
        this.yPos = yPos;
        this.time = time;
    }

    public BikeLog() {}

    public int getLogId() {
        return logId;
    }

    public void setLogId(int logId) {
        this.logId = logId;
    }

    public int getBikeId() {
        return bikeId;
    }

    public void setBikeId(int bikeId) {
        this.bikeId = bikeId;
    }

    public int getCharge() {
        return charge;
    }

    public void setCharge(int charge) {
        this.charge = charge;
    }

    public double getxPos() {
        return xPos;
    }

    public void setxPos(double xPos) {
        this.xPos = xPos;
    }

    public double getyPos() {
        return yPos;
    }

    public void setyPos(double yPos) {
        this.yPos = yPos;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time.toLocalDateTime();
    }

    @Override
    public String toString() {
        return "LogId: " + logId + ", BikeId: " + bikeId + ", Charge: " + charge + ", xPos: " + xPos + ", yPos: " + yPos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BikeLog)) return false;
        BikeLog bikeLog = (BikeLog) o;
        return bikeId == bikeLog.bikeId &&
                charge == bikeLog.charge &&
                Double.compare(bikeLog.xPos, xPos) == 0 &&
                Double.compare(bikeLog.yPos, yPos) == 0;
    }
}
