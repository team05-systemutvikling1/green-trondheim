package main.java.data;

import java.time.LocalDate;
import java.sql.Date;

/**
 * Class for Bike object
 *
 * @author Magne
 */
public class Bike {
    private int bikeId, charge;
    private String name, type, make;
    private boolean needMaintenance;
    private double price, xCoordinate, yCoordinate;
    private LocalDate dateBought;

    public Bike(String name, boolean needMaintenance, String type, double price, LocalDate dateBought, int charge, double x, double y, String make) {
        this.name = name;
        this.needMaintenance = needMaintenance;
        this.type = type;
        this.price = price;
        this.dateBought = dateBought;
        this.charge = charge;
        this.xCoordinate = x;
        this.yCoordinate = y;
        this.make = make;
    }

    public Bike(String name, boolean needMaintenance, String type, double price, LocalDate dateBought, int charge, String make) {
        this.name = name;
        this.needMaintenance = needMaintenance;
        this.charge = charge;
        this.type = type;
        this.make = make;
        this.price = price;
        this.dateBought = dateBought;
    }

    public Bike() {}

    public int getBikeId() {
        return bikeId;
    }

    public void setBikeId(int bikeId) {
        this.bikeId = bikeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isNeedMaintenance() {
        return needMaintenance;
    }

    public void setNeedMaintenance(boolean needMaintenance) {
        this.needMaintenance = needMaintenance;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getDateBought() {
        return Date.valueOf(dateBought);
    }

    public void setDateBought(Date dateBought) {
        this.dateBought = dateBought.toLocalDate();
    }

    public int getCharge() {
        return charge;
    }

    public void setCharge(int charge) {
        this.charge = charge;
    }

    public double getxCoordinate() {
        return xCoordinate;
    }

    public void setxCoordinate(double xCoordinate) {
        this.xCoordinate = xCoordinate;
    }

    public double getyCoordinate() {
        return yCoordinate;
    }

    public void setyCoordinate(double yCoordinate) {
        this.yCoordinate = yCoordinate;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    /**
     * Equals method to check if one object is equal to a Bike object
     *
     * @param bikecheck Object that should be checked if equals
     * @return true if equals, and false if not
     */

    @Override
    public boolean equals(Object bikecheck) {
        if (bikecheck instanceof Bike) {
            Bike b = (Bike) bikecheck;
            return bikeId == b.getBikeId() && name.equals(b.getName()) &&
                    needMaintenance == b.isNeedMaintenance() && type.equals(b.getType()) &&
                    price == b.getPrice() && dateBought.equals(b.dateBought);
        }
        return false;
    }

    @Override
    public String toString() {
        return "Bike id: " + bikeId + ", Name: " + name + ", Need maintenance: " + needMaintenance +
                ", Type: " + type + ", Price: " + price + ", Date purchased: " + dateBought +
                ", Charge: " + charge + ", X-pos: " + xCoordinate + ", Y-pos: " + yCoordinate;
    }
}
