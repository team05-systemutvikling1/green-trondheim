package main.java.data;

/**
 * Class for DockingStation object
 *
 * @author Ørjan
 */
public class DockingStation {
    private String name, address;
    private double xCoordinate, yCoordinate;
    private int maxBikes;

    public DockingStation(String name, String address, double xCoordinate, double yCoordinate, int maxBikes) {
        this.name = name;
        this.address = address;
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
        this.maxBikes = maxBikes;

    }


    public DockingStation() {}


    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getAddress(){
        return address;
    }

    public void setAddress(String address){
        this.address = address;
    }

    public double getxCoordinate() {
        return xCoordinate;
    }

    public void setxCoordinate(double xCoordinate) {
        this.xCoordinate = xCoordinate;
    }

    public double getyCoordinate() {
        return yCoordinate;
    }

    public void setyCoordinate(double yCoordinate) {
        this.yCoordinate = yCoordinate;
    }

    public int getMaxBikes() {
        return maxBikes;
    }

    public void setMaxBikes(int maxBikes) {
        this.maxBikes = maxBikes;
    }

    @Override
    public boolean equals(Object stationcheck){
        if (stationcheck instanceof DockingStation){
            DockingStation s = (DockingStation) stationcheck;
            return name.equals(s.getName()) && address.equals(s.getAddress())
                    && xCoordinate == s.getxCoordinate()
                    && yCoordinate == s.getyCoordinate()
                    && maxBikes == s.getMaxBikes();
        }
        return false;

    }

    @Override
    public String toString(){
        return "Name: " + name + ", address: " + address +
                ", x-coordinate: " + xCoordinate +
                ", y-coordinate: " + yCoordinate +
                ", maximum bike capacity: " + maxBikes;
    }


}
