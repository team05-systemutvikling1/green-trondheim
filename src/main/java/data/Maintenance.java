package main.java.data;

import java.time.LocalDate;
import java.sql.Date;
import java.util.Objects;

/**
 * Class for Maintenance object
 *
 * @author Magne
 */
public class Maintenance {
    private int repOrder, orgNr, bikeId;
    private double price;
    private String doc;
    private LocalDate startDate, endDate;

    public Maintenance(int repOrder, int orgNr, int bikeId, double price, String doc, LocalDate startDate, LocalDate endDate) {
        this.repOrder = repOrder;
        this.orgNr = orgNr;
        this.bikeId = bikeId;
        this.price = price;
        this.doc = doc;
        this.startDate = startDate;
        this.endDate = endDate;

    }

    public Maintenance(int repOrder, int orgNr, int bikeId, double price) {
        this.repOrder = repOrder;
        this.orgNr = orgNr;
        this.bikeId = bikeId;
        this.price = price;
        this.startDate = LocalDate.now();
    }

    public Maintenance() {}

    public int getRepOrder() {
        return repOrder;
    }

    public void setRepOrder(int repOrder) {
        this.repOrder = repOrder;
    }

    public int getOrgNr() {
        return orgNr;
    }

    public void setOrgNr(int orgNr) {
        this.orgNr = orgNr;
    }

    public int getBikeId() {
        return bikeId;
    }

    public void setBikeId(int bikeId) {
        this.bikeId = bikeId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDoc() {
        return doc;
    }

    public void setDoc(String doc) {
        this.doc = doc;
    }

    public Date getStartDate() {
        return Date.valueOf(startDate);
    }

    public LocalDate getLocalEndDate(){
        return endDate;
    }

    public LocalDate getLocalStartDate(){
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate.toLocalDate();
    }

    public Date getEndDate() {
        return Date.valueOf(endDate);
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate.toLocalDate();
    }

    /**
     * Equals method to check if one object is equal to a Maintenance object
     *
     * @param o Object that should be checked if equals
     * @return true if equals, and false if not
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Maintenance)) return false;
        Maintenance that = (Maintenance) o;
        return repOrder == that.repOrder &&
                orgNr == that.orgNr &&
                bikeId == that.bikeId &&
                Double.compare(that.price, price) == 0 &&
                Objects.equals(doc, that.doc) &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(endDate, that.endDate);
    }

    @Override
    public String toString() {
        return "Maintenance{" +
                "repOrder=" + repOrder +
                ", orgNr=" + orgNr +
                ", bikeId=" + bikeId +
                ", price=" + price +
                ", doc='" + doc + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }
}
