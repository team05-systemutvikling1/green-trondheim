package main.java.data;


import java.time.LocalDateTime;
import java.sql.Timestamp;

/**
 * Class for TransactionHistory object
 *
 * @author Jan-Marius
 */
public class TransactionHistory {
    private int transactionNr;
    private long nationalId;
    private LocalDateTime tranTime; //
    private double amount;

    public TransactionHistory(long national_id, LocalDateTime tran_time, double amount){
        this.nationalId = national_id;
        this.tranTime = tran_time;
        this.amount = amount;
    }

    public TransactionHistory() {}


    public int getTransactionNr() {
        return transactionNr;
    }

    public void setTransactionNr(int transactionNr) {
        this.transactionNr = transactionNr;
    }

    public long getNationalId() {
        return nationalId;
    }

    public void setNationalId(long nationalId) {
        this.nationalId = nationalId;
    }

    public LocalDateTime getTranTime() {
        return tranTime;
    }

    public void setTranTime(Timestamp tranTime) {
        this.tranTime = tranTime.toLocalDateTime();
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * Equals method to check if one object is equal to another transaction history.
     *
     * @param transactionHistoryCheck Object that should be checked if equals
     * @return true if equals, and false if not
     */
    @Override
    public boolean equals(Object transactionHistoryCheck) {
        if (transactionHistoryCheck instanceof  TransactionHistory) {
            TransactionHistory th = (TransactionHistory) transactionHistoryCheck;
            return transactionNr == th.getTransactionNr() && nationalId == th.getNationalId() &&
                    tranTime.equals(th.tranTime) && amount == th.getAmount();
        }
        return false;
    }


    @Override
    public String toString() {
        return  "Transaction nr: " + transactionNr +
                ", National id: " + nationalId +
                ", Tran time: " + tranTime +
                ", Amount: " + amount;
    }
}
