package main.java.data;

/**
 * Class for User object
 *
 * @author Magne
 */
public class User {
    private String eMail, firstName, lastName, password, salt;
    private long nationalId;
    private int phoneNr;

    public User(String eMail, long nationalId, String firstName, String lastName, int phoneNr) {
        this.eMail = eMail;
        this.nationalId = nationalId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNr = phoneNr;
    }

    public User() {}

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public long getNationalId() {
        return nationalId;
    }

    public void setNationalId(long nationalId) {
        this.nationalId = nationalId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getPhoneNr() {
        return phoneNr;
    }

    public void setPhoneNr(int phoneNr) {
        this.phoneNr = phoneNr;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    /**
     * Equals method to check if one object is equal to a User object
     *
     * @param usrchck Object that should be checked if equals
     * @return true if equals, and false if not
     */
    @Override
    public boolean equals(Object usrchck) {
        if (usrchck instanceof User) {
            User usr = (User) usrchck;
            return eMail.equals(usr.geteMail()) && nationalId == usr.getNationalId() &&
                    firstName.equals(usr.getFirstName()) && lastName.equals(usr.getLastName()) &&
                    phoneNr == usr.getPhoneNr();

        }
        return false;
    }

    @Override
    public String toString() {
        return eMail + ", " + nationalId + ", " + firstName + ", " + lastName + ", " + phoneNr;
    }
}