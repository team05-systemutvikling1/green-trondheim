package main.java.data;



import java.sql.Timestamp;
import java.time.LocalDateTime;

public class Trip {

    private int tripId, bikeId;
    private LocalDateTime timeStart, timeEnd;
    private String eMail;
    private double length;

    public Trip(int tripId, String eMail, int bikeId, double length, LocalDateTime timeStart, LocalDateTime timeEnd) {
        this.tripId = tripId;
        this.eMail = eMail;
        this.bikeId = bikeId;
        this.length = length;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;

    }
    public Trip(){}

    // Getters start...
    public int getBikeId() {
        return bikeId;
    }

    public int getTripId() {
        return tripId;
    }

    public LocalDateTime getTimeStart() {
        return timeStart;
    }

    public LocalDateTime getTimeEnd() {
        return timeEnd;
    }

    public String geteMail() {
        return eMail;
    }

    public double getLength() {
        return length;
    }
    // ...Getters end

    // Setters begin...
    public void setTripId(int tripId) {
        this.tripId = tripId;
    }

    public void setBikeId(int bikeId) {
        this.bikeId = bikeId;
    }

    public void setTimeStart(Timestamp timeStart) {
        this.timeStart = timeStart.toLocalDateTime();
    }

    public void setTimeEnd(Timestamp timeEnd) {
        this.timeEnd = timeEnd.toLocalDateTime();
    }

    public void setTimeStart(LocalDateTime timeStart) {
        this.timeStart = timeStart;
    }

    public void setTimeEnd(LocalDateTime timeEnd) {
        this.timeEnd = timeEnd;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public void setLength(double length) {
        this.length = length;
    }
    // ...Setters end

    //Creating a standard equals method
    @Override
    public boolean equals(Object chckTrip){
        if(chckTrip instanceof Trip){
            Trip trp = (Trip)chckTrip;
            return toString().equals(trp.toString());

        }

        return false;
    }

    //Creating a toString() method
    @Override
    public String toString(){
        return ("-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*trip toString()-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*" +
                "\nE-mail: " + geteMail() +
                "\nTrip ID: " + getTripId() +
                "\nBike ID: " + getBikeId() +
                "\nStart time: " + getTimeStart() +
                "\nFinish time: " + getTimeEnd() +
                "\nLength: " + getLength());
    }
}
