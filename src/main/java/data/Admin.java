package main.java.data;

import java.util.Objects;

/**
 * Class for Admin object
 *
 * @author Magne
 */
public class Admin {
    private String username, password, salt;

    public Admin(String username, String password, String salt) {
        this.username = username;
        this.password = password;
        this.salt = salt;
    }

    public Admin(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public Admin() {}

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }


    /**
     * Equals method to check if one object is equal to an Admin object
     *
     * @param o Object that should be checked if equals
     * @return true if equals, and false if not
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Admin)) return false;
        Admin admin = (Admin) o;
        return Objects.equals(getUsername(), admin.getUsername()) &&
                Objects.equals(getPassword(), admin.getPassword()) &&
                Objects.equals(getSalt(), admin.getSalt());
    }

    @Override
    public String toString() {
        return "Username: " + username + ", password(hashed): " +
                password + ", salt: " + salt;
    }
}
