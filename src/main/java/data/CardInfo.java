package main.java.data;

import java.time.LocalDate;
import java.sql.Date;
/**
 * Class for CardInfo object
 *
 * @author Rune Vedøy
 */
public class CardInfo {
    private String firstName, lastName;
    private long nationalId, cardNr;
    private int cardExpirationDate;

    public CardInfo(String firstName, String lastName, long nationalId,long cardNr, int cardExpirationDate){
        this.firstName = firstName;
        this.lastName = lastName;
        this.nationalId = nationalId;
        this.cardNr = cardNr;
        this.cardExpirationDate = cardExpirationDate;
    }

    public CardInfo(){}

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lasName) {
        this.lastName = lasName;
    }

    public long getNationalId() {
        return nationalId;
    }

    public void setNationalId(long nationalId) {
        this.nationalId = nationalId;
    }


    public long getCardNr(){
        return cardNr;
    }

    public void setCardNr(long cardNr){
        this.cardNr = cardNr;
    }

    public int getCardExpirationDate() {
        return cardExpirationDate;
    }

    public void setCardExpirationDate(int cardExpirationDate) {
        this.cardExpirationDate = cardExpirationDate;
    }

    @Override
    public boolean equals(Object cardInfoCheck) {
        if(cardInfoCheck instanceof CardInfo){
            CardInfo cardInfo = (CardInfo) cardInfoCheck;
            return getFirstName().equals(cardInfo.getFirstName()) && getLastName().equals(cardInfo.getLastName())
                    && getNationalId() == cardInfo.getNationalId()
                    && getCardExpirationDate() == (cardInfo.getCardExpirationDate());
        }
        return false;
    }

    public String toString() {
        return nationalId + ", " + firstName + ", " + lastName + ", " + "card number hashed" + ", " + cardExpirationDate;
    }
}
