package main.java.data;

/**
 * Object Position, used to retrieve Position objects for bike and dockingstation
 *
 * @author Magne
 */
public class Position {
    private double xCoordinate;
    private double yCoordinate;

    public Position(double xCoordinate, double yCoordinate) {
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
    }

    public Position() {}

    public double getxCoordinate() {
        return xCoordinate;
    }

    public void setxCoordinate(double xCoordinate) {
        this.xCoordinate = xCoordinate;
    }

    public double getyCoordinate() {
        return yCoordinate;
    }

    public void setyCoordinate(double yCoordinate) {
        this.yCoordinate = yCoordinate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Position)) return false;
        Position position = (Position) o;
        return Double.compare(position.xCoordinate, xCoordinate) == 0 &&
                Double.compare(position.yCoordinate, yCoordinate) == 0;
    }

    @Override
    public String toString() {
        return "X-pos: " + xCoordinate + ", Y-pos: " + yCoordinate;
    }
}
