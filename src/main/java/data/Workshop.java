package main.java.data;

/**
 * Class for Workshop object
 *
 * @author Magne
 */
public class Workshop {
    private int orgNr, phoneNr;
    private String orgName, eMail, address;

    public Workshop(int orgNr, String orgName, int phoneNr, String eMail, String address) {
        this.orgNr = orgNr;
        this.orgName = orgName;
        this.phoneNr = phoneNr;
        this.eMail = eMail;
        this.address = address;
    }

    public Workshop() {}

    public int getOrgNr() {
        return orgNr;
    }

    public void setOrgNr(int orgNr) {
        this.orgNr = orgNr;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public int getPhoneNr() {
        return phoneNr;
    }

    public void setPhoneNr(int phoneNr) {
        this.phoneNr = phoneNr;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Equals method to check if one object is equal to a Workshop object
     *
     * @param workshopcheck Object that should be checked if equals
     * @return true if equals, and false if not
     */
    @Override
    public boolean equals(Object workshopcheck) {
        if (workshopcheck instanceof Workshop) {
            Workshop w = (Workshop) workshopcheck;
            return orgNr == w.getOrgNr() && orgName.equals(w.getOrgName()) && phoneNr == w.getPhoneNr() &&
                    eMail.equals(w.geteMail()) && address.equals(w.getAddress());
        }
        return false;
    }
    @Override
    public String toString() {
        return "Org nr: " + orgNr + ", Org name: " + orgName + ", Phone nr: " + phoneNr +
                ", E-mail: " + eMail + ", Address: " + address;
    }
}
