package main.java.data;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Objects;

public class ChargingLog {
    private int clogId, powerConsumption;
    private String name;
    private LocalDateTime time;

    public ChargingLog(int powerConsumption, String name, LocalDateTime time) {
        this.powerConsumption = powerConsumption;
        this.name = name;
        this.time = time;
    }

    public ChargingLog() {}

    public int getClogId() {
        return clogId;
    }

    public void setClogId(int clogId) {
        this.clogId = clogId;
    }

    public int getPowerConsumption() {
        return powerConsumption;
    }

    public void setPowerConsumption(int powerConsumption) {
        this.powerConsumption = powerConsumption;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time.toLocalDateTime();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ChargingLog)) return false;
        ChargingLog that = (ChargingLog) o;
        return powerConsumption == that.powerConsumption &&
                Objects.equals(name, that.name);
    }
}
