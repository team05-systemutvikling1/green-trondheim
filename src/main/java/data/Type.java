package main.java.data;

/**
 * Class for Type object
 *
 * @author Jan-Marius
 */
public class Type {
    private String type;

    public Type(String type){
        this.type = type;
    }

    public Type() {}

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    /**
     * Equals method to check if one object is equal to a Type object
     *
     * @param typecheck Object that should be checked if equals
     * @return true if equals, and false if not
     */
    @Override
    public boolean equals(Object typecheck) {
        if(typecheck instanceof Type) {
            Type t = (Type) typecheck;
            return type.equals(t.getType());
        }
        return false;
    }

    @Override
    public String toString() {
        return "Type: " + type;
    }
}
