package main.javaFX.controller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import main.java.data.Maintenance;
import main.java.db.BikeDAO;
import main.java.db.MaintenanceDAO;
import main.javaFX.MainApp;
import main.javaFX.model.BikeModel;
import main.javaFX.model.WorkshopModel;
import main.javaFX.resource.AlertBox;

import java.sql.SQLException;
import java.util.ArrayList;

public class MaintenanceAddDialogController {
    private MainApp mainapp;
    private Stage dialogStage;
    private ObservableList<String> bikeList = FXCollections.observableArrayList();
    private ObservableList<String> workshopList = FXCollections.observableArrayList();

    @FXML
    private TextField priceField;

    @FXML
    private TextField repOrderField;

    @FXML
    private Spinner<String> bikeSpin;

    @FXML
    private ChoiceBox<String> workshopChoice;

    @FXML
    private void initialize(){
        priceField.setPromptText("Price");
        repOrderField.setPromptText("Maintenance order");
        bikeSpin.setEditable(true);
    }

    public MaintenanceAddDialogController(){}

    public void setDialogStage(Stage dialogStage){
        this.dialogStage = dialogStage;
    }

    public void setMainApp(MainApp mainApp){
        this.mainapp = mainApp;
    }


    public void setBikeList(ObservableList<BikeModel> bikeModels){
        for (BikeModel bikeModel : bikeModels) {

            bikeList.add(Integer.toString(bikeModel.getBike_id()));
        }
        SpinnerValueFactory<String> valueFactory= new SpinnerValueFactory.ListSpinnerValueFactory<>(bikeList);
        bikeSpin.setValueFactory(valueFactory);
    }

    public void setWorkshopList(ObservableList<WorkshopModel> workshopList){
        for(WorkshopModel workshop : workshopList) {
            this.workshopList.add(workshop.getOrgName());
        }
        workshopChoice.setItems(this.workshopList);
    }

    @FXML
    private void handleOK(){
        if(isInputValid()){
            //int repOrder, int orgNr, int bikeId, double price
            int orgNr = -1;
            for(int i = 0; i < mainapp.getWsData().size(); i++) {
                if (mainapp.getWsData().get(i).getOrgName().equals(workshopChoice.getSelectionModel().getSelectedItem())) {
                    orgNr = mainapp.getWsData().get(i).getOrgNr();
                }
            }
            int bikeIndex = -1;
            for(int i = 0; i< mainapp.getBikeData().size(); i++){
                if(Integer.parseInt(bikeSpin.getValue()) == mainapp.getBikeData().get(i).getBike_id()){
                    bikeIndex = i;
                }
            }
            if(orgNr != -1) {
                Maintenance maintenance = new Maintenance(Integer.parseInt(repOrderField.getText()),
                        orgNr, Integer.parseInt(bikeSpin.getValue()),
                        Double.parseDouble(priceField.getText()));
                try{
                    MaintenanceDAO maintenanceDAO = new MaintenanceDAO();
                    maintenanceDAO.addMaintenance(maintenance);
                    BikeDAO bikeDAO = new BikeDAO();
                    bikeDAO.setMaintenance(mainapp.getBikeData().get(bikeIndex).getBike_id(), true);
                    bikeDAO.setName(mainapp.getBikeData().get(bikeIndex).getBike_id(), null);
                    AlertBox.showInformationBox(dialogStage, "new maintenance",
                            "Adding maintenance successful!", "Maintenance added successfully.");
                    dialogStage.close();
                }catch(SQLException e){
                    AlertBox.showAlertBox(dialogStage, "Connection failed",
                            "Database connection failed",
                            "Something went wrong when connecting to database.");
                }

            }else{
                AlertBox.showAlertBox(dialogStage,"Workshop not found!", "New maintenance not accepted",
                        "Could not find workshop");
            }
        }
    }

    @FXML
    private void handleCancel(){
        dialogStage.close();

    }

    private boolean isInputValid(){
        String error = "";
        if(priceField.getText() == null || priceField.getText().trim().length() == 0){
            error += "No price inserted!\n";
        }else{
            try{
                Double.parseDouble(priceField.getText().trim());
            }catch(NumberFormatException e){
                error += "Invalid input! Price must be a decimal.\n";
            }
        }
        if(repOrderField.getText() == null || repOrderField.getText().trim().length() == 0){
            error += "No maintenance order added!\n";
        }else{
            try {
                Integer.parseInt(repOrderField.getText().trim());
            }catch(NumberFormatException e){
                error += "Invalid Input! Maintenance order must be an integer.\n";
            }
        }

        if(bikeSpin == null){
            error += "No bike selected!\n";
        }
        try{
            BikeDAO bike = new BikeDAO();
            System.out.println(bikeSpin.getValue());
            if(bike.getBike(Integer.parseInt(bikeSpin.getValue())) == null){
                error += "Bike not found in database!\n";
            }
        }catch(SQLException e){
            error += "Bike not found in database!\n";
        }

        if(workshopChoice.getSelectionModel().getSelectedItem() == null){
            error += "No workshop selected\n";
        }
        if(error.length() == 0){
            return true;
        }else {
            AlertBox.showAlertBox(dialogStage,
                    "Input error!",
                    "Invalid input!",
                    error);
            return false;
        }

    }

}
