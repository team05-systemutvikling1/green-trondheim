package main.javaFX.controller;

import com.lynden.gmapsfx.javascript.object.LatLong;
import javafx.beans.property.IntegerProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import main.java.data.Maintenance;
import javafx.fxml.FXML;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import main.java.data.Workshop;
import main.javaFX.MainApp;
import main.javaFX.model.BikeModel;
import main.javaFX.model.MaintenanceModel;
import main.javaFX.model.WorkshopModel;
import main.javaFX.resource.AlertBox;

public class MaintenanceOverviewController {

   private MainApp mainApp;
   private Stage dialogstage;
   private ObservableList<WorkshopModel> workshopModels;
    @FXML
    private TableView<MaintenanceModel> maintenanceTable;


    @FXML
    private TableColumn<MaintenanceModel, String> orgName;

    @FXML
    private TableColumn<MaintenanceModel, Integer> repNr;

    @FXML
    private Label repOrderLabel;

    @FXML
    private Label bikeIdLabel;

    @FXML
    private Label priceLabel;

    @FXML
    private Label startDateLabel;

    @FXML
    private Label endDateLabel;

    @FXML
    private Label orgNameLabel;

    public MaintenanceOverviewController(){
    }

    @FXML
    private void initialize(){
        orgName.setCellValueFactory(
                cellData -> cellData.getValue().orgNameProperty()
        );
        repNr.setCellValueFactory(
                cellData -> cellData.getValue().repOrderProperty().asObject()
        );

        showMaintenanceDetails(null);

        maintenanceTable.getSelectionModel().selectedItemProperty().addListener(
                ((observable, oldValue, newValue) -> showMaintenanceDetails(newValue))
        );
    }

    public void setDialogstage(Stage dialogStage){
        this.dialogstage = dialogStage;
    }
    public Stage getDialogstage(){
        return dialogstage;
    }

    public void setMainApp(MainApp mainApp){
        this.mainApp = mainApp;

        maintenanceTable.setItems(mainApp.getRepData());
    }
    private void showMaintenanceDetails(MaintenanceModel maintenanceModel){
        //workshopModels = mainApp.getWsData();


        /*for(int i = 0; i < workshopModels.size(); i++){
            if(maintenanceModel.getOrgNr() == workshopModels.get(i).getOrgNr()){
                wsName = workshopModels.get(i).getOrgName();
            }
        }*/
        if(maintenanceModel != null){
            orgNameLabel.setText(maintenanceModel.getOrgName());
            repOrderLabel.setText(Integer.toString(maintenanceModel.getRepOrder()));
            bikeIdLabel.setText(Integer.toString(maintenanceModel.getBikeId()));
            priceLabel.setText(Double.toString(maintenanceModel.getPrice()));
            startDateLabel.setText(maintenanceModel.getStartDate().toString());
            if(maintenanceModel.getEndDate() !=null){
                endDateLabel.setText(maintenanceModel.getEndDate().toString());
            }else{
                endDateLabel.setText("");
            }
        }else{
            orgNameLabel.setText("");
            repOrderLabel.setText("");
            bikeIdLabel.setText("");
            priceLabel.setText("");
            startDateLabel.setText("");
            endDateLabel.setText("");
        }
    }

    @FXML
    private void handleGetDocumentation(){
        if(maintenanceTable.getSelectionModel().getSelectedItem() != null) {
            if(maintenanceTable.getSelectionModel().getSelectedItem().getDocumentation() != null) {
                AlertBox.showInformationBox(dialogstage, "Maintenance documentation",
                        "Document #" + maintenanceTable.getSelectionModel().getSelectedItem().getRepOrder(),
                        maintenanceTable.getSelectionModel().getSelectedItem().getDocumentation());
            }else{
                AlertBox.showInformationBox(dialogstage, "Maintenance documentation",
                        "Document #" + maintenanceTable.getSelectionModel().getSelectedItem().getRepOrder(),
                        "No available documentation.");
            }
        }else{
            AlertBox.showAlertBox(dialogstage, "Selection error!",
                    "No maintenance selected!",
                    "Please select a maintenance to view.");
        }
    }
    @FXML
    private void handleEditMaintenance(){
        if(maintenanceTable.getSelectionModel().getSelectedItem() != null) {
            mainApp.showMaintenanceEditDialog(dialogstage,
                    maintenanceTable.getSelectionModel().getSelectedItem().getRepOrder(),
                    maintenanceTable.getSelectionModel().getSelectedItem().getBikeId(),
                    maintenanceTable.getSelectionModel().getSelectedItem().getDocumentation());
            mainApp.updateWSData();
            mainApp.updateRepData();
        }else {
            AlertBox.showAlertBox(dialogstage,
                    "No maintenance selected",
                    "No maintenance selected",
                    "Please select a maintenance to edit");
        }
    }

    @FXML
    private void handleAddMaintenance(){
       mainApp.showMaintenanceAddDialog(dialogstage);
       mainApp.updateWSData();
       mainApp.updateRepData();
       maintenanceTable.refresh();

    }





}
