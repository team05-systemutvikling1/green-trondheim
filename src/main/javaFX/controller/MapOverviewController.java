package main.javaFX.controller;

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.event.UIEventType;
import com.lynden.gmapsfx.javascript.object.*;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import main.javaFX.MainApp;
import netscape.javascript.JSObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class MapOverviewController implements Initializable, MapComponentInitializedListener{
    @FXML
    private GoogleMapView mapView;
    GoogleMap map;

    ArrayList<Marker> dockingMarkers = new ArrayList<>();
    ArrayList<InfoWindow> dockingInfo = new ArrayList<>();


    private MainApp mainApp;

    public MapOverviewController(){
    }

    @Override
    public void initialize(URL url, ResourceBundle rb){
        mapView.addMapInializedListener(this);
    }

    @Override
    public void mapInitialized(){
        MapOptions mapOptions = new MapOptions();
        mapOptions.center(new LatLong(63.4325, 10.3978))
                .zoomControl(true)
                .zoom(14)
                .overviewMapControl(true)
                .mapType(MapTypeIdEnum.ROADMAP)
                .panControl(true)
                .rotateControl(false)
                .streetViewControl(false);


        map = mapView.createMap(mapOptions);

        for(int i = 0; i<mainApp.getDockingData().size(); i++) {
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(new LatLong(mainApp.getDockingData().get(i).getX_coordinate(), mainApp.getDockingData().get(i).getY_coordinate()))
                    .title(mainApp.getDockingData().get(i).getName())
                    .visible(true)
                    .label("D");
            dockingMarkers.add(new Marker(markerOptions));

            InfoWindowOptions infoWindow = new InfoWindowOptions();
            infoWindow.maxWidth(250)
                    .content("<!DOCTYPE html>" +
                            "\n<HTML><body><h3>Station name: " +mainApp.getDockingData().get(i).getName() +
                    "</h3>\n<p>Address: " + mainApp.getDockingData().get(i).getAddress() +
                    "</p>\n<p>X-position: " + mainApp.getDockingData().get(i).getX_coordinate() +
                    "</p>\n<p>Y-position: " +mainApp.getDockingData().get(i).getY_coordinate() +
                    "</p>\n<p>Bike capacity: " + mainApp.getDockingData().get(i).getMax_bikes() +
                    "</p>\n<p>Currently holding " + mainApp.getDockingData().get(i).getCurrentBikes() + " bikes</p></body></HTML>")
                    .position(new LatLong(mainApp.getDockingData().get(i).getX_coordinate(), mainApp.getDockingData().get(i).getY_coordinate()));

            dockingInfo.add(new InfoWindow(infoWindow));
            final int seq = i;
            map.addMarker(dockingMarkers.get(i));
            map.addUIEventHandler(dockingMarkers.get(i), UIEventType.click, (JSObject obj) ->{
                for(int j = 0; j < dockingInfo.size(); j++) {
                    dockingInfo.get(j).close();
                }
                dockingInfo.get(seq).open(map, dockingMarkers.get(seq));

            });
        }

    }


    public void setMainApp(MainApp main){
        this.mainApp = main;

    }

}
