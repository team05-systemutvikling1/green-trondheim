package main.javaFX.controller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import main.java.data.Bike;
import main.java.data.ChargingLog;
import main.java.data.DockingStation;
import main.java.db.*;
import main.javaFX.MainApp;
import main.javaFX.resource.AlertBox;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class StatsOverviewController {

    private MainApp mainApp;
    private ObservableList<String> dockingNames = FXCollections.observableArrayList();

    @FXML
    private BarChart<String, Integer> barChartInteger;
    @FXML
    private BarChart<String, Double> barChartDouble;
    @FXML
    private LineChart<String, Integer> lineChart;
    @FXML
    private Button button1, button2, button3, button4, button5, button6, button7, button8;
    @FXML
    private Pane paneBikes, paneRepairs;
    @FXML
    private SplitPane panePower;
    @FXML
    private Label bikesTotalPrice, bikesAvgPrice, repairsTotalPrice, repairsAvgPrice;
    @FXML
    private DatePicker dateFrom, dateTo, dateFrom1, dateTo1;
    @FXML
    private ChoiceBox<String> choiceName;

    private TripDAO tripDAO;
    private BikeDAO bikeDAO;
    private MaintenanceDAO maintenanceDAO;
    private DockingStationDAO dockingStationDAO;
    private ChargingLogDAO chargingLogDAO;

    public StatsOverviewController(){
    }

    @FXML
    private void initialize() throws SQLException {
        barChartDouble.setVisible(false);
        barChartInteger.setVisible(false);

        if (tripDAO == null) {tripDAO = new TripDAO();} //Sjekke om den allerede er satt?
        if (bikeDAO == null) {bikeDAO = new BikeDAO();}
        if (maintenanceDAO == null) {maintenanceDAO = new MaintenanceDAO();}
        if (dockingStationDAO == null) {dockingStationDAO = new DockingStationDAO();}
        if (chargingLogDAO == null) {chargingLogDAO = new ChargingLogDAO();}

        choiceName.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                System.out.println("Selected docking " + newValue);
                lineChart.getData().clear();
                XYChart.Series<String, Integer> series = new XYChart.Series<>();
                try {
                    for (ChargingLog cl : chargingLogDAO.getChargingLog(newValue)) {
                        String s = Integer.toString(cl.getClogId());
                        series.getData().add(new XYChart.Data<>(s, cl.getPowerConsumption()));
                    }
                    series.setName("Power consumption, in Watt");
                    lineChart.getData().add(series);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @FXML
    private void handleLogout(){
        mainApp.showAdminLogin();
    }

    @FXML
    private void handleAmountTrips() {
        disableButtons();
        System.out.println("# Trips clicked");
        XYChart.Series<String, Integer> series = new XYChart.Series<>();
        paneBikes.setVisible(false);
        paneRepairs.setVisible(false);
        panePower.setVisible(false);
        barChartDouble.setVisible(false);
        barChartInteger.setVisible(true);
        barChartInteger.getData().clear();
        try {
            ArrayList<Bike> bikes = bikeDAO.getAllBikes();
            ArrayList<Integer> bikeIds = new ArrayList<>();
            // Getting all bikeIds:
            for (Bike b : bikes) {
                bikeIds.add(b.getBikeId());
            }

            ArrayList<Integer> amounts = new ArrayList<>();
            // Getting amount of trips for each bikeId:
            for (int i : bikeIds) {
                amounts.add(tripDAO.getAmountTrips(i));
            }

            if (bikeIds.size() == amounts.size()) {
                for (int i = 0; i < bikeIds.size(); i++) {
                    series.getData().add(new XYChart.Data<>(bikeIds.get(i).toString(), amounts.get(i)));
                }
            }

            series.setName("# Trips");
            barChartInteger.getData().add(series);
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    @FXML
    private void handleTotalKm() {
        disableButtons();
        System.out.println("Total km clicked");
        XYChart.Series<String, Double> series = new XYChart.Series<>();
        paneBikes.setVisible(false);
        paneRepairs.setVisible(false);
        panePower.setVisible(false);
        barChartInteger.setVisible(false);
        barChartDouble.setVisible(true);
        barChartDouble.getData().clear();
        try {
            ArrayList<Bike> bikes = bikeDAO.getAllBikes();
            ArrayList<Integer> bikeIds = new ArrayList<>();
            // Getting all bikeIds:
            for (Bike b : bikes) {
                bikeIds.add(b.getBikeId());
            }

            ArrayList<Double> amounts = new ArrayList<>();
            // Getting amount of trips for each bikeId:
            for (int i : bikeIds) {
                amounts.add(tripDAO.getTotalLengthTrips(i));
            }

            if (bikeIds.size() == amounts.size()) {
                for (int i = 0; i < bikeIds.size(); i++) {
                    series.getData().add(new XYChart.Data<>(bikeIds.get(i).toString(), amounts.get(i)));
                }
            }

            series.setName("Total km");
            barChartDouble.getData().add(series);
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    @FXML
    private void handleAvgKm() {
        disableButtons();
        System.out.println("Avg km clicked");
        XYChart.Series<String, Double> series = new XYChart.Series<>();
        paneBikes.setVisible(false);
        paneRepairs.setVisible(false);
        panePower.setVisible(false);
        barChartInteger.setVisible(false);
        barChartDouble.setVisible(true);
        barChartDouble.getData().clear();
        try {
            ArrayList<Bike> bikes = bikeDAO.getAllBikes();
            ArrayList<Integer> bikeIds = new ArrayList<>();
            // Getting all bikeIds:
            for (Bike b : bikes) {
                bikeIds.add(b.getBikeId());
            }

            ArrayList<Double> amounts = new ArrayList<>();
            // Getting amount of trips for each bikeId:
            for (int i : bikeIds) {
                amounts.add(tripDAO.getAvgLengthTrips(i));
            }

            if (bikeIds.size() == amounts.size()) {
                for (int i = 0; i < bikeIds.size(); i++) {
                    series.getData().add(new XYChart.Data<>(bikeIds.get(i).toString(), amounts.get(i)));
                }
            }

            series.setName("Average km/Trip");
            barChartDouble.getData().add(series);
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    @FXML
    private void handleBikes() {
        disableButtons();
        barChartInteger.setVisible(false);
        barChartDouble.setVisible(false);
        paneRepairs.setVisible(false);
        panePower.setVisible(false);
        dateFrom.getEditor().clear();
        dateTo.getEditor().clear();
        paneBikes.setVisible(true);
        try {
            double totalPrice = bikeDAO.getTotalPrice();
            double avgPrice = bikeDAO.getAvgPrice();
            String totalPriceString = Double.toString(totalPrice);
            String avgPriceString = Double.toString(avgPrice);
            bikesTotalPrice.setText(totalPriceString);
            bikesAvgPrice.setText(avgPriceString);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void calculateBikesBetween() {
        disableButtons();
        LocalDate dateFromLD = dateFrom.getValue();
        LocalDate dateToLD = dateTo.getValue();
        try {
            if (dateFromLD == null || dateToLD == null) {
                AlertBox.showAlertBox(mainApp.getPrimaryStage(), "Dates", "Both dates must be set", "");
            } else if (dateFromLD.isBefore(dateToLD)) {
                double totalPrice = bikeDAO.getTotalPriceBetween(dateFromLD, dateToLD);
                double avgPrice = bikeDAO.getAvgPriceBetween(dateFromLD, dateToLD);
                String totalPriceString = Double.toString(totalPrice);
                String avgPriceString = Double.toString(avgPrice);
                bikesTotalPrice.setText(totalPriceString);
                bikesAvgPrice.setText(avgPriceString);
            } else {
                AlertBox.showAlertBox(mainApp.getPrimaryStage(), "Dates", "From date must be before To date", "");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void handleRepairs() {
        disableButtons();
        barChartInteger.setVisible(false);
        barChartDouble.setVisible(false);
        paneBikes.setVisible(false);
        panePower.setVisible(false);
        dateFrom1.getEditor().clear();
        dateTo1.getEditor().clear();
        paneRepairs.setVisible(true);
        try {
            double totalPrice = maintenanceDAO.getTotalPrice();
            double avgPrice = maintenanceDAO.getAvgPrice();
            String totalPriceString = Double.toString(totalPrice);
            String avgPriceString = Double.toString(avgPrice);
            repairsTotalPrice.setText(totalPriceString);
            repairsAvgPrice.setText(avgPriceString);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void calculateRepairsBetween() {
        disableButtons();
        LocalDate dateFromLD;
        LocalDate dateToLD;
        dateFromLD = dateFrom1.getValue();
        dateToLD = dateTo1.getValue();
        try {
            if (dateFromLD == null || dateToLD == null) {
                AlertBox.showAlertBox(mainApp.getPrimaryStage(), "Dates", "Both dates must be set", "");
            } else if (dateFromLD.isBefore(dateToLD)) {
                double totalPrice = maintenanceDAO.getTotalPriceBetween(dateFromLD, dateToLD);
                double avgPrice = maintenanceDAO.getAvgPriceBetween(dateFromLD, dateToLD);
                String totalPriceString = Double.toString(totalPrice);
                String avgPriceString = Double.toString(avgPrice);
                repairsTotalPrice.setText(totalPriceString);
                repairsAvgPrice.setText(avgPriceString);
            } else {
                AlertBox.showAlertBox(mainApp.getPrimaryStage(), "Dates", "From date must be before To date", "");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void handlePowerUsage() {
        barChartInteger.setVisible(false);
        barChartDouble.setVisible(false);
        paneBikes.setVisible(false);
        paneRepairs.setVisible(false);
        panePower.setVisible(true);
        choiceName.getItems().clear();
        try {
            for (DockingStation d : dockingStationDAO.getAllDockingStations()) {
                dockingNames.add(d.getName());
            }
            choiceName.setItems(dockingNames);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    private void disableButtons(){
        new Thread() {
            public void run() {
                button1.setDisable(true);
                button2.setDisable(true);
                button3.setDisable(true);
                button4.setDisable(true);
                button5.setDisable(true);
                button6.setDisable(true);
                button7.setDisable(true);
                button8.setDisable(true);

                try {
                    Thread.sleep(1000); //5 seconds, obviously replace with your chosen time
                }
                catch(InterruptedException ex) {
                    ex.printStackTrace();
                }

                button1.setDisable(false);
                button2.setDisable(false);
                button3.setDisable(false);
                button4.setDisable(false);
                button5.setDisable(false);
                button6.setDisable(false);
                button7.setDisable(false);
                button8.setDisable(false);
            }
        }.start();
    }
}
