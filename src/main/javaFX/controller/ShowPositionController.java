package main.javaFX.controller;

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.object.*;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import main.javaFX.MainApp;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ShowPositionController implements Initializable, MapComponentInitializedListener{
    @FXML
    private GoogleMapView mapView;

    GoogleMap map;

    ArrayList<Marker> dockingMarkers = new ArrayList<>();
    ArrayList<InfoWindow> dockingInfo = new ArrayList<>();

    private double latitude;
    private double longitude;
    private String title;
    private MainApp mainApp;

    public ShowPositionController(double latitude, double longitude, String title){
        this.latitude = latitude;
        this.longitude = longitude;
        this.title = title;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb){
        mapView.addMapInializedListener(this);
    }

    @Override
    public void mapInitialized(){
        MapOptions mapOptions = new MapOptions();
        mapOptions.center(new LatLong(latitude, longitude))
                .zoomControl(true)
                .zoom(14)
                .overviewMapControl(true)
                .mapType(MapTypeIdEnum.ROADMAP)
                .panControl(true)
                .rotateControl(false)
                .streetViewControl(false);


        map = mapView.createMap(mapOptions);

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(new LatLong(latitude, longitude))
                .title(title)
                .visible(true)
                .label("");

        map.addMarker(new Marker(markerOptions));

    }


    public void setMainApp(MainApp main){
        this.mainApp = main;

    }

}