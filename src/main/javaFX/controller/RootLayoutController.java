package main.javaFX.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.layout.AnchorPane;
import main.javaFX.MainApp;

import java.io.IOException;


public class RootLayoutController {

    private MainApp mainApp;

    @FXML
    public MenuBar menuBar;
    @FXML
    public Menu logout;

    public RootLayoutController() {}

    public void showMenu(boolean b){
        menuBar.setVisible(b);
    }


    @FXML
    private void handleBikeOverview(){
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/main/javaFX/view/BikeOverview.fxml"));
            AnchorPane BikeOverviewLayout = (AnchorPane) loader.load();

            BikeOverviewController controller = loader.getController();
            controller.setMainApp(mainApp);
            mainApp.getRootLayout().setCenter(BikeOverviewLayout);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void newBike(){
        mainApp.showBikeAddDialog();
    }

    @FXML
    private void newBikeType(){
        mainApp.showAddBikeType();
    }

    @FXML
    private void handleDockingOverview(){
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/main/javaFX/view/DockingOverview.fxml"));
            AnchorPane DockingOverviewLayout = (AnchorPane) loader.load();

            DockingOverviewController controller = loader.getController();
            controller.setMainApp(mainApp);
            mainApp.getRootLayout().setCenter(DockingOverviewLayout);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @FXML
    private void newDockingStation(){
        mainApp.showDockingAddDialog();
    }

    @FXML
    private void handleUserOverview(){
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/main/javaFX/view/UserOverview.fxml"));
            AnchorPane UserOverviewLayout = loader.load();

            UserOverviewController controller = loader.getController();
            controller.setMainApp(mainApp);
            mainApp.getRootLayout().setCenter(UserOverviewLayout);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void handleAdministrator(){
        mainApp.showAdminWindow();
    }

    @FXML
    private void newUser(){
        mainApp.showUserAddDialog();
    }

    @FXML
    public void handleMapOverview(){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/main/javaFX/view/MapOverview.fxml"));
            AnchorPane mapOverviewLayout = loader.load();

            MapOverviewController controller = loader.getController();
            controller.setMainApp(mainApp);
            mainApp.getRootLayout().setCenter(mapOverviewLayout);

        }catch (IOException e){
            e.printStackTrace();

        }
    }

    @FXML
    private void handleWorkshopAddOverview(){
        mainApp.showWorkshopAddDialog();
        mainApp.updateWSData();
    }

    @FXML
    private void handleWorkshopEditOverview(){
        mainApp.showWorkshopEditDialog();
        mainApp.updateWSData();
    }

    @FXML
    private void handleStatsOverview(){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/main/javaFX/view/StatsOverview.fxml"));
            AnchorPane statsOverviewLayout = loader.load();

            StatsOverviewController controller = loader.getController();
            controller.setMainApp(mainApp);
            mainApp.getRootLayout().setCenter(statsOverviewLayout);

        }catch (IOException e){
            e.printStackTrace();

        }
    }

    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }
}
