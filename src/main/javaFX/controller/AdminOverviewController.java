package main.javaFX.controller;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import main.java.db.AdminDAO;
import main.javaFX.MainApp;
import main.javaFX.resource.AlertBox;

import java.io.IOException;
import java.sql.SQLException;

public class AdminOverviewController {
    private MainApp mainApp;

    @FXML
    private ChoiceBox<String> changeUsernameCBox;
    @FXML
    private PasswordField changeCurPassword;
    @FXML
    private PasswordField newPassword1;
    @FXML
    private PasswordField newPassword2;

    @FXML
    private ChoiceBox<String> usernameCBox;

    @FXML
    private TextField newUsername;
    @FXML
    private PasswordField adminPassword;

    public AdminOverviewController(){}

    public void setMainApp(MainApp mainApp){
        this.mainApp = mainApp;
    }

    public void setUsernames(ObservableList<String> usernames){
        usernameCBox.setItems(usernames);
        changeUsernameCBox.setItems(usernames);
    }

    @FXML
    private void changePassword(){
        String errorMessage = "";
        try {
            AdminDAO adminDAO = new AdminDAO();
            if(!(adminDAO.isPasswordCorrect(changeUsernameCBox.getSelectionModel().getSelectedItem(),
                    changeCurPassword.getText()))){
                errorMessage += "Wrong current password!";
            }

            if(!(newPassword1.getText().equals(newPassword2.getText()))){
                errorMessage += "New passwords does not match!";
            }

            if(errorMessage.length() == 0){
                try {
                    if(adminDAO.setPassword(changeUsernameCBox.getSelectionModel().getSelectedItem(), newPassword1.getText())) {
                        AlertBox.showConfirmationBox(mainApp.getPrimaryStage(), "Password changed!",
                                "Password changed!", "");
                        clearChangeFields();
                        mainApp.updateUsernames();
                    }
                } catch (SQLException b) {
                    throw b;
                }
            } else {
                AlertBox.showAlertBox(mainApp.getPrimaryStage(), "Invalid fields!",
                        "Please correct invalid fields!", errorMessage);
                clearChangeFields();
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    @FXML
    private void handleDelete(){
        if(usernameCBox.getSelectionModel().getSelectedItem().toLowerCase().equals("admin")){
            AlertBox.showAlertBox(mainApp.getPrimaryStage(), "Cannot delete!",
                    "User: 'admin' can never be deleted!", "");
            return;
        }
        if(usernameCBox.getSelectionModel().getSelectedItem().equals(mainApp.getLoginName())){
            AlertBox.showAlertBox(mainApp.getPrimaryStage(), "Cannot delete!",
                    "Cannot delete admin!",
                    "Cannot delete administrator that is currently logged in.");
            return;
        }
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/main/javaFX/view/AdminDelete.fxml"));
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Delete admin!");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(mainApp.getPrimaryStage());
            AdminDeleteController adminDeleteController = new AdminDeleteController(usernameCBox.getSelectionModel().getSelectedItem());
            adminDeleteController.setDialogStage(dialogStage);
            loader.setController(adminDeleteController);
            AnchorPane showAdminDelete = loader.load();


            Scene scene = new Scene(showAdminDelete);
            dialogStage.setScene(scene);

            dialogStage.showAndWait();

            mainApp.updateUsernames();
        }catch (IOException e){
            e.printStackTrace();

        }
    }

    @FXML
    private void handleNew(){
        if(isInputValid()){
            try {
                AdminDAO adminDAO = new AdminDAO();
                adminDAO.addAdmin(newUsername.getText().trim().toLowerCase());

                clearNewFields();

                mainApp.updateUsernames();
            } catch (SQLException e){
                e.printStackTrace();
            }
        }
    }

    @FXML
    private void clearNewFields(){
        newUsername.setText("");
        adminPassword.setText("");
    }

    @FXML
    private void clearChangeFields(){
        changeCurPassword.setText("");
        newPassword1.setText("");
        newPassword2.setText("");
    }

    private boolean isInputValid(){
        try{
            AdminDAO adminDAO = new AdminDAO();
            String errorMesage = "";

            if(newUsername.getText() == null || newUsername.getText().trim().length() == 0){
                errorMesage += "Invalid username!\n";
            }else if(!(usernameValid())){
                errorMesage += "Username already exists!\n";
            }


            if(!(adminDAO.isPasswordCorrect("admin", adminPassword.getText()))){
                errorMesage += "Incorrect admin password!\n";
            }

            if(errorMesage.length() == 0){
                return true;
            } else {
                return AlertBox.showAlertBox(mainApp.getPrimaryStage(), "Invalid fields!", "Please correct invalid fields!", errorMesage);
            }

        } catch (SQLException e){
            e.printStackTrace();
            return false;
        }


    }

    private boolean usernameValid(){
        String name = newUsername.getText().trim().toLowerCase();
        for (String n : mainApp.getUsernames()){
            if (n.equals(name)){
                return false;
            }
        }
        return true;
    }


}
