package main.javaFX.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import main.java.data.Workshop;
import main.java.db.WorkshopDAO;
import main.javaFX.MainApp;
import main.javaFX.model.WorkshopModel;
import main.javaFX.resource.AlertBox;

import java.sql.SQLException;

public class WorkshopEditController {
    private Stage dialogStage;
    private MainApp mainApp;
    private ObservableList<String> wsStringList = FXCollections.observableArrayList();

    @FXML
    private ChoiceBox<String> wsChoice;

    @FXML
    private TextField phoneNrField;

    @FXML
    private TextField eMailField;

    @FXML
    private TextField addressField;

    @FXML
    private void initialize(){

    }

    public WorkshopEditController(){

    }

    public void setDialogstage(Stage dialogStage){
        this.dialogStage = dialogStage;
    }
    public void setMainApp(MainApp mainApp){
        this.mainApp = mainApp;
    }
    public void setWsList(ObservableList<WorkshopModel> wsList){
        for(int i = 0; i < wsList.size(); i++){
            wsStringList.add(wsList.get(i).getOrgName());
        }
        wsChoice.setItems(wsStringList);

    }
    @FXML
    private void handleConfirm(){
        if(isValueOK()){
            int workshopIndex = -1;
            for (int i = 0; i<mainApp.getWsData().size(); i++){
                if (wsChoice.getSelectionModel().getSelectedItem().equals(
                        mainApp.getWsData().get(i).getOrgName())
                        ) {
                    workshopIndex = i;
                }
            }
            if(workshopIndex < 0){
                AlertBox.showAlertBox(dialogStage,
                        "Workshop not found",
                        "Selected workshop not found",
                        "Could not find selected workshop");
            }
            try{
                WorkshopDAO workshopDAO = new WorkshopDAO();
                //int orgNr, String orgName, int phoneNr, String eMail, String address
                workshopDAO.updateWorkshop(new Workshop(
                        mainApp.getWsData().get(workshopIndex).getOrgNr(),
                        wsChoice.getSelectionModel().getSelectedItem(),
                        Integer.parseInt(phoneNrField.getText()),
                        eMailField.getText(),
                        addressField.getText()));
                AlertBox.showInformationBox(dialogStage,
                        "Workshop edit",
                        "Workshop edit successful",
                        "Workshop was successfully edited");
                dialogStage.close();
            }catch(SQLException e){
                AlertBox.showAlertBox(dialogStage,
                        "Database error",
                        "Database error!",
                        "Something went wrong during the database connection");
            }
        }
    }

    @FXML
    private void handleCancel(){
        dialogStage.close();
    }

    private boolean isValueOK(){
        String error = "";
        if(wsChoice.getSelectionModel().getSelectedItem() == null){
            error += "No workshop selected!\n";
        }
        if(phoneNrField.getText() == null
                || phoneNrField.getText().trim().length() == 0){
            error += "No phone number found!";
        }else{
            try{
                Integer.parseInt(phoneNrField.getText());
            }catch(NumberFormatException e){
                error += "Phone number must be an integer!";
            }
        }

        if (eMailField.getText() == null ||
                eMailField.getText().trim().length() == 0){
            error += "No email address detected!";
        }

        if(addressField.getText() == null ||
                eMailField.getText().trim().length() == 0){
            error += "No address detected!";
        }

        if(error.trim().length() == 0){
            return true;
        }else{
            AlertBox.showAlertBox(dialogStage,
                    "Illegal arguments!",
                    "The following input is illegal!",
                    error);
            return false;
        }

    }


}
