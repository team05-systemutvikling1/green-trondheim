package main.javaFX.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import main.java.data.Bike;
import main.java.data.Workshop;
import main.java.db.BikeDAO;
import main.javaFX.MainApp;
import main.javaFX.model.BikeModel;
import main.javaFX.model.DockingModel;
import main.javaFX.resource.AlertBox;

import java.time.LocalDate;

public class BikeAddDialogController {

    MainApp mainApp;
    Stage dialogStage;
    ObservableList<String> dockingNames = FXCollections.observableArrayList();
    boolean okClicked;
    BikeModel bikeModel;

    @FXML
    private ChoiceBox<String> addDockingName;
    @FXML
    private ChoiceBox<String> addType;
    @FXML
    private TextField addPrice;
    @FXML
    private DatePicker addDate;
    @FXML
    private TextField addMake;

    public BikeAddDialogController(){}

    public void setMainApp(MainApp mainApp){
        this.mainApp = mainApp;
    }

    public void setDialogStage(Stage dialogStage){
        this.dialogStage = dialogStage;
    }

    public void setDockingNames(ObservableList<DockingModel> dockingStations){
        for (DockingModel dm : dockingStations){
            dockingNames.add(dm.getName());
        }
        addDockingName.setItems(dockingNames);

    }


    public void setTypes(ObservableList<String> types){
        addType.setItems((types));
    }

    @FXML
    public void initialize(){

    }

    @FXML
    private void handleOk(){
        if(isInputValid()) {
            Bike bike = new Bike(addDockingName.getSelectionModel().getSelectedItem(), false,
                    addType.getSelectionModel().getSelectedItem(), Double.parseDouble(addPrice.getText()), addDate.getValue(),
                    100, addMake.getText());
            try {
                BikeDAO bikeDAO = new BikeDAO();
                if(bikeDAO.addBike(bike)){
                    okClicked = true;
                } else {
                    okClicked = AlertBox.showAlertBox(dialogStage, "Database error!", "An error occured!", "Sorry, something went wrong when trying to add the user to the database.");
                }
            } catch (Exception e){
                e.printStackTrace();
                okClicked = AlertBox.showAlertBox(dialogStage, "Database error!", "An error occured!", "Sorry, something went wrong when trying to add the user to the database.");
            }
            dialogStage.close();
        }
    }

    @FXML
    private void handleCancel(){
        dialogStage.close();
    }

    public boolean isOkClicked(){
        return okClicked;
    }

    public boolean isInputValid(){
        String errorMessage = "";

        if(addType.getSelectionModel().getSelectedItem() == null || addType.getSelectionModel().getSelectedItem().trim().length() == 0 ||
                addType.getSelectionModel().getSelectedItem().equals("New type")){
            errorMessage += "Invalid type!\n";
        }

        if(addMake.getText() == null || addMake.getText().trim().length() == 0 ||
                addType.getSelectionModel().getSelectedItem().equals("New type")){
            errorMessage += "Invalid make!\n";
        }

        if(addPrice.getText() == null || addPrice.getText().trim().length() == 0){
            errorMessage += "Invalid price!\n";
        }else {
            try{
                Double.parseDouble(addPrice.getText());
            } catch (NumberFormatException e){
                errorMessage += "Invalid price (must be an integer, or decimal separated with '.')!\n";
                e.printStackTrace();
            }
        }

        if(addDate.getValue() == null || addDate.getValue().isAfter(LocalDate.now())){
            errorMessage += "Invalid buy-date!\n";
        }



        if(errorMessage.length() == 0) {
            return true;
        }else {
            AlertBox.showAlertBox(dialogStage, "Invalid Fields!", "Please correct invalid fields", errorMessage);

            return false;
        }
    }

}
