package main.javaFX.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import main.java.db.DockingStationDAO;
import main.javaFX.MainApp;
import main.javaFX.model.DockingModel;
import main.javaFX.resource.AlertBox;

import javax.swing.*;
import java.sql.SQLException;


public class DockingOverviewController {
    private MainApp mainApp;
    private RootLayoutController rootLayoutController;

    @FXML
    private TableView<DockingModel> dockingTable;
    @FXML
    private TableColumn<DockingModel, String> nameColumn;
    @FXML
    private TableColumn<DockingModel, String> addressColumn;

    @FXML
    private Label name;
    @FXML
    private Label address;
    @FXML
    private Label x_coordinate;
    @FXML
    private Label y_coordinate;
    @FXML
    private Label capacity;



    public DockingOverviewController(){}

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        // Initialize the docking table with the two columns.
        nameColumn.setCellValueFactory(
                cellData -> cellData.getValue().nameProperty());
        addressColumn.setCellValueFactory(
                cellData -> cellData.getValue().addressProperty());

        // Clear bike details.
        showDockingDetails(null);

        // Listen for selection changes and update bike details when changed.
        dockingTable.getSelectionModel().selectedItemProperty().addListener(
                ((observable, oldValue, newValue) -> showDockingDetails(newValue))
        );
    }

    @FXML
    private void handleLogout(){
        mainApp.showAdminLogin();
    }


    @FXML
    private void handleDeleteStation() {
        int selectedIndex = dockingTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            String dockingNameSelected = dockingTable.getItems().get(selectedIndex).getName();
            int delete = JOptionPane.showConfirmDialog(null,
                    "Delete docking station: " + dockingNameSelected,
                    "WARNING!", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
            if (delete == 0) {
                try {
                    DockingStationDAO dockingDAO = new DockingStationDAO();
                    if (dockingDAO.delDockingStation(dockingNameSelected)) {
                        JOptionPane.showMessageDialog(null, "Bike #" + dockingNameSelected + " deleted.");
                        dockingTable.getItems().remove(selectedIndex);
                        showDockingDetails(null);
                    } else {
                        JOptionPane.showMessageDialog(null,
                                "Oops, something went wrong trying to delete docking station: " + dockingNameSelected);
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(null,
                            "Oops, something went wrong trying to delete docking station: " + dockingNameSelected);
                }
            }
        }else {
            // Nothing selected.
            AlertBox.showAlertBox(mainApp.getPrimaryStage(),"No Selection", "No Docking Station Selected",
                    "Please select a docking station in the table.");
        }
    }

    @FXML
    public void handleEditDockingStation(){
        if(dockingTable.getSelectionModel().getSelectedIndex() >= 0) {
            DockingModel dockingModel = dockingTable.getSelectionModel().getSelectedItem();
            mainApp.showDockingEditDialog(dockingModel);
            dockingTable.refresh();
            showDockingDetails(dockingTable.getSelectionModel().getSelectedItem());
        } else {
            AlertBox.showAlertBox(mainApp.getPrimaryStage(), "No Selection!", "No Docking Station Selected!",
                    "Please select a docking station in the table.");
        }
    }

    @FXML
    public void handleAddDockingStation(){
        mainApp.showDockingAddDialog();
        dockingTable.refresh();
        showDockingDetails(null);
    }

    @FXML
    public void showPosition(){

        int selectedIndex = dockingTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            DockingModel selectedStation = dockingTable.getSelectionModel().getSelectedItem();
            double latitude = selectedStation.getX_coordinate();
            double longitude = selectedStation.getY_coordinate();
            String title = selectedStation.getName();
            mainApp.showPosition(latitude, longitude, title);
        }
    }


    private void showDockingDetails(DockingModel dockingModel) {
        if (dockingModel != null) {
            name.setText(dockingModel.getName());
            address.setText(dockingModel.getAddress());
            x_coordinate.setText(Double.toString(dockingModel.getX_coordinate()));
            y_coordinate.setText(Double.toString(dockingModel.getY_coordinate()));
            capacity.setText(Integer.toString(dockingModel.getMax_bikes()));
        } else {
            name.setText("");
            address.setText("");
            x_coordinate.setText("");
            y_coordinate.setText("");
            capacity.setText("");
        }
    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;

        // Add observable list data to the table
        dockingTable.setItems(mainApp.getDockingData());
    }
}
