package main.javaFX.controller;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import main.java.data.Workshop;
import main.java.db.WorkshopDAO;
import main.javaFX.MainApp;
import main.javaFX.resource.AlertBox;

import java.sql.SQLException;


public class WorkshopAddDialogController {
    private MainApp mainApp;
    private Stage dialogStage;

    @FXML
    private TextField workshopField;

    @FXML
    private TextField orgNrField;
    @FXML
    private TextField phoneNrField;

    @FXML
    private TextField eMailField;

    @FXML
    private TextField addressField;

    @FXML
    private void initialize(){
        workshopField.setPromptText("Workshop name");
        orgNrField.setPromptText("Organization number");
        phoneNrField.setPromptText("Workshop phone number");
        eMailField.setPromptText("Workshop E-mail");
        addressField.setPromptText("Workshop address");
    }

    public WorkshopAddDialogController(){

    }

    public void setMainApp(MainApp mainApp){
        this.mainApp = mainApp;
    }

    public void setDialogStage(Stage dialogStage){
        this.dialogStage = dialogStage;
    }

    @FXML
    private void handleCancel(){
        dialogStage.close();
    }

    @FXML
    private void handleConfirm(){
        if(isValueOK()){
            try {
                //int orgNr, String orgName, int phoneNr, String eMail, String address
                WorkshopDAO workshopDAO = new WorkshopDAO();
                workshopDAO.addWorkshop(new Workshop(
                        Integer.parseInt(orgNrField.getText()),
                        workshopField.getText(),
                        Integer.parseInt(phoneNrField.getText()),
                        eMailField.getText(),
                        addressField.getText()
                ));
                AlertBox.showInformationBox(dialogStage,
                        "Workshop",
                        "Workshop added",
                        "Workshop successfully added");
                dialogStage.close();
            }catch(SQLException e){
                AlertBox.showAlertBox(dialogStage,
                        "Database error!",
                        "Database error",
                        "Something went wrong connecting to the database");

            }
        }
    }

    private boolean isValueOK(){
        String error = "";
        if(workshopField.getText() == null ||
                workshopField.getText().trim().length() == 0){
            error += "No workshop name found!";
        }
        if(orgNrField.getText() == null ||
                orgNrField.getText().trim().length() == 0){
            error += "No organization number found!";
        }else{
            try {
                Integer.parseInt(orgNrField.getText());
            }catch(NumberFormatException e){
                error += "Organization number must be an integer!";
            }
        }
        if(phoneNrField.getText() == null ||
                phoneNrField.getText().trim().length() == 0){
            error += "Phone number not found!";
        }else{
            try {
                Integer.parseInt(phoneNrField.getText());
            }catch(NumberFormatException e){
                error += "Phone number must be an integer";
            }
        }

        if(eMailField.getText() == null ||
                eMailField.getText().trim().length() == 0){
            error += "E-mail not found!";
        }

        if(addressField.getText() == null ||
                addressField.getText().trim().length() == 0){
            error += "Address not found!";
        }

        if(error.trim().length() == 0){
            return true;
        }else{
            AlertBox.showAlertBox(dialogStage,
                    "Warning!",
                    "Incorrect values!",
                    error);
            return true;
        }
    }

}
