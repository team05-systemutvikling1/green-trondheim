package main.javaFX.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import main.java.data.Bike;
import main.java.db.BikeDAO;
import main.javaFX.MainApp;
import main.javaFX.model.BikeModel;
import main.javaFX.model.DockingModel;

import java.time.LocalDate;

public class BikeEditDialogController {

    MainApp mainApp;
    ObservableList<String> dockingNames = FXCollections.observableArrayList();
    ObservableList<String> bikeTypes = FXCollections.observableArrayList();

    public Stage dialogStage;
    public BikeModel bike;
    public boolean okClicked;


    @FXML
    public ChoiceBox<String> editDockingName;
    @FXML
    public ChoiceBox<String> typeCBox;
    @FXML
    public TextField editMake;
    @FXML
    public TextField editPrice;
    @FXML
    public DatePicker editDateBought;
    @FXML
    public CheckBox editMaintenance;



    public BikeEditDialogController(){}

    @FXML
    public void initialize(){
    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    @FXML
    public void handleOk(){
        if(isInputValid()){
            bike.setName(editDockingName.getValue());
            bike.setType(typeCBox.getValue());
            bike.setMake(editMake.getText());
            bike.setPrice(Double.parseDouble(editPrice.getText()));
            bike.setDate_bought(editDateBought.getValue());
            bike.setNeed_maintenance(editMaintenance.isSelected());

            try {
                BikeDAO bikeDAO = new BikeDAO();
                Bike b = new Bike(bike.getName(),bike.isNeed_maintenance(),bike.getType(),
                        bike.getPrice(), bike.getDate_bought(), bike.getCharge(), bike.getMake());
                b.setBikeId(bike.getBike_id());
                bikeDAO.updateBike(bike.getBike_id(), b);
                okClicked = true;

            } catch (Exception e){
                e.printStackTrace();
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.initOwner(mainApp.getPrimaryStage());
                alert.setTitle("Oops!");
                alert.setHeaderText("Database issue");
                alert.setContentText("Seems like something went wrong when trying to add the" +
                        " bike to the database!");
                okClicked = false;
                alert.showAndWait();

            }
            dialogStage.close();
        }
    }

    @FXML
    public void handleCancel(){
        dialogStage.close();
    }

    /**
     * Returns true if the user clicked OK, false otherwise.
     *
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public void setDockingStations(ObservableList<DockingModel> dockingStations){
        for(DockingModel dm : dockingStations) {
            dockingNames.add(dm.getName());
        }
        editDockingName.setItems(dockingNames);
    }

    public void setTypes(ObservableList<String> types){
        for(String t : types){
            bikeTypes.add(t);
        }
        typeCBox.setItems(bikeTypes);
    }

    public void setBike(BikeModel bikeModel){
        this.bike = bikeModel;

        int dockingName = dockingNames.indexOf(bike.getName());
        editDockingName.getSelectionModel().select(dockingName);
        typeCBox.getSelectionModel().select(bikeModel.getType());
        editMake.setText(bike.getMake());
        editPrice.setText(Double.toString(bike.getPrice()));
        editDateBought.setValue(bike.getDate_bought());
        editMaintenance.setSelected(bike.isNeed_maintenance());
    }

    public boolean isInputValid(){
        String errorMessage = "";

        if(typeCBox.getSelectionModel().getSelectedItem() == null || typeCBox.getSelectionModel()
                .getSelectedItem().trim().length() == 0){
            errorMessage += "Invalid type!\n";
        }

        if(editMake.getText() == null || editMake.getText().trim().length() == 0){
            errorMessage += "Invalid make!\n";
        }

        if(editPrice.getText() == null || editPrice.getText().trim().length() == 0){
            errorMessage += "Invalid price!\n";
        }else {
            try{
                Double.parseDouble(editPrice.getText());
            } catch (NumberFormatException e){
                errorMessage += "Invalid price (must be an integer, or decimal separated with '.')!\n";
            }
        }

        if(editDateBought.getValue() == null || editDateBought.getValue().isAfter(LocalDate.now())){
            errorMessage += "Invalid date bought!";
        }

        if(errorMessage.length() == 0) {
            return true;
        }else {
            // Show the error message.
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Invalid Fields");
            alert.setHeaderText("Please correct invalid fields");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }

}
