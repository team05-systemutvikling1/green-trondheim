package main.javaFX.controller;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import main.java.data.User;
import main.java.db.UserDAO;
import main.javaFX.MainApp;
import main.javaFX.model.UserModel;
import main.javaFX.resource.AlertBox;


public class UserEditDialogController {
    MainApp mainApp;
    UserModel userModel;
    private Stage dialogStage;
    private boolean okClicked;

    public ObservableList<UserModel> users;

    @FXML
    private Label editEmail;
    @FXML
    private Label editNationalID;
    @FXML
    private TextField editFirstName;
    @FXML
    private TextField editLastName;
    @FXML
    private TextField editPhoneNumber;

    public UserEditDialogController(){}

    @FXML
    private void initialize(){

    }

    public void setMainApp(MainApp mainApp){
        this.mainApp = mainApp;
    }

    public boolean isOkClicked() {
        return okClicked;
    }

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public void setUser(UserModel userGiven){
        this.userModel = userGiven;

        editEmail.setText(userModel.getE_mail());
        editNationalID.setText(String.valueOf(userModel.getNational_id()));
        editFirstName.setText(userModel.getFirst_name());
        editLastName.setText(userModel.getLast_name());
        editPhoneNumber.setText(String.valueOf(userModel.getPhone_nr()));
    }

    @FXML
    public void handleOk(){
        if(isInputValid()){
            userModel.setE_mail(editEmail.getText().trim().toLowerCase());
            userModel.setNational_id(Long.parseLong(editNationalID.getText()));
            userModel.setFirst_name(editFirstName.getText().trim());
            userModel.setLast_name(editLastName.getText().trim());
            userModel.setPhone_nr(Integer.parseInt(editPhoneNumber.getText()));

            try {
                UserDAO userDAO = new UserDAO();
                User user = new User(userModel.getE_mail(), userModel.getNational_id(), userModel.getFirst_name(),
                        userModel.getLast_name(), userModel.getPhone_nr());

                userDAO.updateUser(user);
                okClicked = true;

            }catch (Exception e){
                okClicked = AlertBox.showAlertBox(dialogStage, "Database issue!", "Database issue",
                        "Seems like something went wrong when trying to update the database.");
            }
            dialogStage.close();
        }
    }

    @FXML
    public void handleCancel(){
        dialogStage.close();
    }

    public boolean isInputValid(){
        String errorMessage = "";
        System.out.println(editEmail.getText());
        if(editEmail.getText() == null || editEmail.getText().trim().length() == 0) {
            errorMessage += "Invalid Email!\n";
        } else if(!(mainApp.emailAvailable(editEmail.getText().trim().toLowerCase())) &&
                !(editEmail.getText().toLowerCase().equals(userModel.getE_mail()))){
            errorMessage += "Email already in use!\n";
        }

        if(editNationalID.getText() == null || editNationalID.getText().trim().length() == 0){
            errorMessage += "Invalid national id!\n";
        } else {
            try {
                Long.parseLong(editNationalID.getText());
            } catch (IllegalArgumentException e) {
                errorMessage += "Invalid national id (must be an integer)!\n";
            }

        }

        if(editFirstName.getText() == null || editFirstName.getText().trim().length() == 0){
            errorMessage += "Invalid first name!\n";
        }

        if(editLastName.getText() == null || editLastName.getText().trim().length() == 0){
            errorMessage += "Invalid last name!\n";
        }

        if(editPhoneNumber.getText() == null || editPhoneNumber.getText().trim().length() == 0){
            errorMessage += "Invalid phone number!\n";
        } try {
            Integer.parseInt(editPhoneNumber.getText());
        } catch (IllegalArgumentException e) {
            errorMessage += "Invalid phone number (must be an integer)!\n";
        }


        if(errorMessage.length() == 0) {
            return true;
        }else {
            return AlertBox.showAlertBox(dialogStage, "Invalid fields!", "Please correct invalid fields!", errorMessage);
        }
    }




}
