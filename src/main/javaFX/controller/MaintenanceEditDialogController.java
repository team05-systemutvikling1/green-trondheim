package main.javaFX.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import main.java.data.Maintenance;
import main.java.db.BikeDAO;
import main.java.db.MaintenanceDAO;
import main.javaFX.MainApp;
import main.javaFX.model.DockingModel;
import main.javaFX.resource.AlertBox;

import java.sql.SQLException;
import java.time.LocalDate;

public class MaintenanceEditDialogController {
    private MainApp mainApp;
    private Stage dialogStage;
    private ObservableList<String> dockingList =FXCollections.observableArrayList();
    private String documentation;
    private int repId;
    private int bikeId;

    @FXML
    ChoiceBox<String> dockingData;

    @FXML
    TextArea documentationData;

    @FXML
    private void initialize(){

    }

    public void setDockingList(ObservableList<DockingModel> newDockingList){

        for(DockingModel dockingModel : newDockingList) {
            dockingList.addAll(dockingModel.getName());
        }
        dockingData.setItems(dockingList);

    }

    public void setMainApp(MainApp mainApp){
        this.mainApp = mainApp;
    }

    public void setDialogStage(Stage dialogStage){
        this.dialogStage = dialogStage;
    }
    public void setRepId(int repId){
        this.repId = repId;
    }
    public void setBikeId(int bikeId){
        this.bikeId = bikeId;
    }

    public void setDocumentation(String documentation){
        if(documentation == null) {
            documentationData.setPromptText("Please enter the documentation text here.");
        }else {
            documentationData.setText(documentation);
        }
                this.documentation = documentation;

    }


    public MaintenanceEditDialogController(){


    }

    @FXML
    private void handleUpdate(){
        if(isEditOKInput()) {
            try {
                MaintenanceDAO maintenanceDAO = new MaintenanceDAO();
                maintenanceDAO.setDoc(repId, documentationData.getText());
                AlertBox.showInformationBox(dialogStage, "Update documentation",
                        "Documentation updated", "The documentation was successfully updated!");
            } catch (SQLException e) {
                AlertBox.showAlertBox(dialogStage, "Connection failed",
                        "Database connection failed",
                        "Something went wrong when connecting to database.");
            }
        }
    }

    @FXML
    private void handleCancel(){
        dialogStage.close();
    }

    private boolean isEditOKInput(){
        String error = "";

        int repIndex = -1;
        for(int i = 0; i<mainApp.getRepData().size(); i++){
            if(repId == mainApp.getRepData().get(i).getRepOrder()){
                repIndex = i;
            }
        }
        if(repIndex < 0) {
            error += "Maintenance not found!\n";
        }

        if(mainApp.getRepData().get(repIndex).getEndDate() != null) {
            error += "This maintenance is closed!\n";
        }
        if(error.trim().length() == 0) {
            return true;
        }else {
            AlertBox.showAlertBox(dialogStage,
                    "Maintenance not found",
                    "Maintenance not found",
                    error);
            return false;
        }
    }

    private boolean isOKInput(){
        String error = "";
        if (dockingData.getSelectionModel().getSelectedItem() == null){
            error += "No selected docking station!\n";
        }
        int dockingIndex = -1;
        for(int i = 0; i< mainApp.getDockingData().size(); i++) {
            if(mainApp.getDockingData().get(i).getName().equals(dockingData.getSelectionModel().getSelectedItem())) {
                dockingIndex = i;
            }
        }
        int repIndex = -1;
        for(int i = 0; i<mainApp.getRepData().size(); i++){
            if(repId == mainApp.getRepData().get(i).getRepOrder()){
                repIndex = i;
            }
        }
        int bikeIndex = -1;
        for(int i = 0; i < mainApp.getBikeData().size(); i++){
            if(bikeId == mainApp.getBikeData().get(i).getBike_id()){
                bikeIndex = i;
            }

        }

        if(bikeIndex < 0){
            error += "No bikes found!";
        }
        if(dockingIndex < 0) {
            error += "No docking station found!";
        }
        if(documentationData.getText() == null){
            error += "No documentation found";
        }

        if(mainApp.getDockingData().get(dockingIndex).getMax_bikes() < (mainApp.getDockingData().get(dockingIndex).getCurrentBikes()+1)) {
            error += "Docking station full, please find another one";
        }
        if(repIndex < 0){
            error += "No maintenance id found!";
        }
        if(mainApp.getRepData().get(repIndex).getEndDate() != null &&
                !mainApp.getBikeData().get(bikeIndex).isNeed_maintenance()){
            error += "Maintenance already closed";
        }

        if(error.trim().length() == 0){
            return true;
        }else{
            AlertBox.showAlertBox(dialogStage,
                    "Invalid input!",
                    "The following input is invalid",
                    error);
            return false;
        }
    }

    @FXML
    private void handleDeliver() {
        if (isOKInput()) {
            try {

                MaintenanceDAO maintenanceDAO = new MaintenanceDAO();
                maintenanceDAO.setEndDate(repId, LocalDate.now());
                maintenanceDAO.setDoc(repId, documentationData.getText());
                BikeDAO bikeDAO = new BikeDAO();
                bikeDAO.setName(bikeId, dockingData.getSelectionModel().getSelectedItem());
                AlertBox.showInformationBox(dialogStage,
                        "Maintenance closed",
                        "Maintenance closed",
                        "Maintenance successfully closed");

                dialogStage.close();
            } catch (SQLException e) {
                AlertBox.showAlertBox(dialogStage,
                        "Database error!",
                        "Database error!",
                        "Something went wrong contacting the database.");
            }

        }
    }

}
