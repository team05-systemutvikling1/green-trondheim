package main.javaFX.controller;

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.event.GMapMouseEvent;
import com.lynden.gmapsfx.javascript.event.UIEventType;
import com.lynden.gmapsfx.javascript.object.*;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.stage.Stage;
import main.javaFX.MainApp;
import main.javaFX.model.DockingModel;
import netscape.javascript.JSObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.Observable;
import java.util.ResourceBundle;

public class GetPositionController implements Initializable, MapComponentInitializedListener{
    Stage dialogStage;

    @FXML
    private GoogleMapView mapView;
    GoogleMap map;

    ArrayList<Marker> dockingMarkers = new ArrayList<>();
    ArrayList<InfoWindow> dockingInfo = new ArrayList<>();

    private double latitude;
    private double longitude;
    private String title;
    private MainApp mainApp;

    public GetPositionController(){

    }

    @Override
    public void initialize(URL url, ResourceBundle rb){
        mapView.addMapInializedListener(this);
    }

    @Override
    public void mapInitialized(){
        MapOptions mapOptions = new MapOptions();
        mapOptions.center(new LatLong(63.4325, 10.3978))
                .zoomControl(true)
                .zoom(14)
                .overviewMapControl(true)
                .mapType(MapTypeIdEnum.ROADMAP)
                .panControl(true)
                .rotateControl(false)
                .streetViewControl(false);


        map = mapView.createMap(mapOptions);

        map.addUIEventHandler(UIEventType.dblclick, (JSObject obj) -> {
            LatLong ll = new LatLong((JSObject) obj.getMember("latLng"));
            System.out.println("LatLong: lat: " + ll.getLatitude() + " lng: " + ll.getLongitude());
            mainApp.setCoordinates(ll);
            dialogStage.close();

    });
    }


    public void setMainApp(MainApp main){
        this.mainApp = main;

    }

    public void setDialogStage(Stage dialogStage){
        this.dialogStage = dialogStage;
    }

}