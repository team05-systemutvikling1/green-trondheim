package main.javaFX.controller;

import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.stage.Stage;
import main.java.db.AdminDAO;
import main.javaFX.resource.AlertBox;

import java.sql.SQLException;

public class AdminDeleteController {

    private Stage dialogStage;
    private final String userDelete;

    @FXML
    private PasswordField adminPassword;

    public AdminDeleteController(String userDelete){
        this.userDelete = userDelete;
    }

    @FXML
    private void handleDelete(){
        try {
            AdminDAO adminDAO = new AdminDAO();
            if(adminDAO.isPasswordCorrect("admin", adminPassword.getText())){
                adminDAO.delAdmin(userDelete);
                dialogStage.close();
            }else {
                AlertBox.showAlertBox(dialogStage, "Wrong password", "Wrong password!", "Wrong password for administrator: admin.");
                dialogStage.close();
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
    }


    public void setDialogStage(Stage dialogStage){
        this.dialogStage = dialogStage;
    }
}
