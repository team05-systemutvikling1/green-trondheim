package main.javaFX.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import main.java.db.BikeDAO;
import main.java.util.DateUtil;
import main.javaFX.MainApp;
import main.javaFX.model.BikeModel;
import main.javaFX.resource.AlertBox;

import javax.swing.*;
import java.sql.SQLException;

public class BikeOverviewController {
    @FXML
    private TableView<BikeModel> bikeTable;
    @FXML
    private TableColumn<BikeModel, Integer> bikeIDColumn;
    @FXML
    private TableColumn<BikeModel, String> dockingNameColumn;
    @FXML
    private TableColumn<BikeModel, Integer> chargeColumn;

    @FXML
    private Label bikeLabel;
    @FXML
    private Label dockingLabel;
    @FXML
    private Label chargeLabel;
    @FXML
    private Label maintenanceLabel;
    @FXML
    private Label typeLabel;
    @FXML
    private Label makeLabel;
    @FXML
    private Label priceLabel;
    @FXML
    private Label dateBoughtLabel;

    // Reference to the main application.
    public MainApp mainApp;

    /**
     * The constructor.
     * The constructor is called before the initialize() method.
     */
    public BikeOverviewController() {
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        // Initialize the bike table with the two columns.
        bikeIDColumn.setCellValueFactory(
                cellData -> cellData.getValue().bike_idProperty().asObject());
        dockingNameColumn.setCellValueFactory(
                cellData -> cellData.getValue().nameProperty());
        chargeColumn.setCellValueFactory(
                cellData -> cellData.getValue().chargeProperty().asObject());

        // Clear bike details.
        showBikeDetails(null);

        // Listen for selection changes and update bike details when changed.
        bikeTable.getSelectionModel().selectedItemProperty().addListener(
                ((observable, oldValue, newValue) -> showBikeDetails(newValue))
        );
    }

    @FXML
    private void handleLogout(){
        mainApp.showAdminLogin();
    }

    @FXML
    private void handleDeleteBike() {
        int selectedIndex = bikeTable.getSelectionModel().getSelectedIndex();
        if(selectedIndex >= 0) {
            int bikeIDSelected = bikeTable.getItems().get(selectedIndex).getBike_id();
            String bikeIDString = Integer.toString(bikeIDSelected);
            System.out.println(bikeIDSelected);
            int delete = JOptionPane.showConfirmDialog(null,
                    "Delete bike #" + bikeIDString,
                    "WARNING!", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
            if (delete == 0) {
                try {
                    BikeDAO bikeDAO = new BikeDAO();
                    if (bikeDAO.delBike(bikeIDSelected)) {
                        JOptionPane.showMessageDialog(null, "Bike #" + bikeIDString + " deleted.");
                        bikeTable.getItems().remove(selectedIndex);
                        showBikeDetails(null);
                    } else {
                        JOptionPane.showMessageDialog(null,
                                "Oops, something went wrong trying to delete bike #" + bikeIDString);
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(null,
                            "Oops, something went wrong trying to delete bike #" + bikeIDString);
                }
            }
        }else {
            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No Bike Selected");
            alert.setContentText("Please select a bike.");

            alert.showAndWait();
        }
    }

    @FXML
    private void handleMaintenance(){
        int index = bikeTable.getSelectionModel().getFocusedIndex();
        if(index >= 0){
            //int bikeIDSelected = bikeTable.getItems().get(index).getBike_id();
            mainApp.showBikeMaintenanceDialog();
            mainApp.updateRepData();
            mainApp.updateWSData();

        }else{
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No bike Selected");
            alert.setContentText("Please select a bike.");

            alert.showAndWait();
        }
    }

    @FXML
    public void handleEditBike(){
        int index = bikeTable.getSelectionModel().getSelectedIndex();
        if(index >= 0) {
            BikeModel b = bikeTable.getSelectionModel().getSelectedItem();
            mainApp.showBikeEditDialog(b);
            bikeTable.refresh();
            showBikeDetails(b);
        } else {
            AlertBox.showAlertBox(mainApp.getPrimaryStage(), "No Selection!", "No Bike Selected!",
                    "Please select a bike.");
        }
    }

    @FXML
    public void handleAddBike(){
        mainApp.showBikeAddDialog();
        mainApp.updateBikeData();
        bikeTable.refresh();
    }

    @FXML
    public void showPosition(){
        int selectedIndex = bikeTable.getSelectionModel().getSelectedIndex();
        if(selectedIndex >= 0){
            BikeModel selectedBike = bikeTable.getSelectionModel().getSelectedItem();
            System.out.println(Integer.toString(selectedIndex));

            double latitude = selectedBike.getX_coordinate();
            double longitude = selectedBike.getY_coordinate();
            System.out.println("Latitude: " + latitude + "\nLongitude: " + longitude);
            String title = "Bike id: " + selectedBike.getBike_id();
            mainApp.showPosition(latitude, longitude, title);
        }
    }

    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;

        // Add observable list data to the table
        bikeTable.setItems(mainApp.getBikeData());
    }

    private void showBikeDetails(BikeModel bikeModel) {
        if (bikeModel != null) {
            bikeLabel.setText(Integer.toString(bikeModel.getBike_id()));
            dockingLabel.setText(bikeModel.getName());
            chargeLabel.setText(Integer.toString(bikeModel.getCharge()));
            maintenanceLabel.setText(Boolean.toString(bikeModel.isNeed_maintenance()));
            typeLabel.setText(bikeModel.getType());
            makeLabel.setText(bikeModel.getMake());
            priceLabel.setText(Double.toString(bikeModel.getPrice()));
            dateBoughtLabel.setText(DateUtil.format(bikeModel.getDate_bought()));
        } else {
            bikeLabel.setText("");
            dockingLabel.setText("");
            chargeLabel.setText("");
            maintenanceLabel.setText("");
            typeLabel.setText("");
            makeLabel.setText("");
            priceLabel.setText("");
            dateBoughtLabel.setText("");
        }
    }




}
