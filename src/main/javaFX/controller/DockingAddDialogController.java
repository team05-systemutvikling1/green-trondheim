package main.javaFX.controller;

import com.lynden.gmapsfx.javascript.object.LatLong;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import main.java.data.DockingStation;
import main.java.db.DockingStationDAO;
import main.javaFX.MainApp;
import main.javaFX.model.DockingModel;
import main.javaFX.resource.AlertBox;
import java.util.Locale;

public class DockingAddDialogController {

    MainApp mainApp;
    private Stage dialogStage;
    private boolean okClicked;
    DockingStation dockingModel;

    @FXML
    private TextField addName;
    @FXML
    private TextField addAddress;
    @FXML
    private TextField addX_coordinate;
    @FXML
    private TextField addY_coordinate;
    @FXML
    private TextField addCapacity;

    public DockingAddDialogController(){}

    public void setMainApp(MainApp mainApp){
        this.mainApp = mainApp;
    }

    public void setDialogStage(Stage dialogStage){
        this.dialogStage = dialogStage;
    }

    public boolean isOkClicked(){
        return okClicked;
    }

    @FXML
    private void handleOk(){
        if(isInputValid()){
            try {
                DockingStationDAO dockingStationDAO = new DockingStationDAO();
                DockingStation dockingStation = new DockingStation(addName.getText(), addAddress.getText(), Double.parseDouble(addX_coordinate.getText()),
                        Double.parseDouble(addY_coordinate.getText()), Integer.parseInt(addCapacity.getText()));
                dockingStationDAO.addDockingStation(dockingStation);
                mainApp.updateDockingData();
                okClicked = true;
            } catch (Exception e){
                e.printStackTrace();
                okClicked = AlertBox.showAlertBox(dialogStage, "Database issue!", "Database issue",
                        "Seems like something went wrong when trying to update the database.");
            }
            dialogStage.close();
        }
    }

    @FXML
    public void handleCancel(){
        dialogStage.close();
    }

    @FXML
    public void getPosition(){
        LatLong coordinates = mainApp.getPosition();
        if(coordinates != null){
            addX_coordinate.setText(String.format(Locale.ROOT, "%.6f", coordinates.getLatitude()));
            addY_coordinate.setText(String.format(Locale.ROOT, "%.6f", coordinates.getLongitude()));
            mainApp.setCoordinates(null);
        }
    }

    private boolean isInputValid(){
        String errorMessage = "";

        if(addName.getText() == null || addName.getText().trim().length() == 0){
            errorMessage += "Invalid name!\n";
        }else if(!(nameAvailable(addName.getText().trim()))){
            errorMessage += "Name is already taken, please try another one!\n";
        }

        if(addAddress.getText() == null || addAddress.getText().trim().length() == 0){
            errorMessage += "Invalid address!\n";
        }

        if(addX_coordinate.getText() == null || addX_coordinate.getText().trim().length() == 0
                || addX_coordinate.getText().trim().length() > 9){
            errorMessage += "Invalid x-coordinate (cannot be longer than 8 digits!\n";
        }else {
            try{
                 Double.parseDouble(addX_coordinate.getText());
            } catch (IllegalArgumentException e){
                errorMessage += "Invalid x-coordinate (must be an integer)!\n";
            }
        }

        if(addY_coordinate.getText() == null || addY_coordinate.getText().trim().length() == 0
                || addY_coordinate.getText().trim().length() > 9){
            errorMessage += "Invalid y-coordinate (cannot be longer than 8 digits!\n";
        }else {
            try{
                Double.parseDouble(addY_coordinate.getText());
            } catch (IllegalArgumentException e){
                errorMessage += "Invalid y-coordinate (must be an integer)!\n";
            }
        }

        if(errorMessage.length() == 0){
            return true;
        } else {
            return AlertBox.showAlertBox(this.dialogStage, "Invalid Input!", "Please correct invalid fields!", errorMessage);
        }

    }

    private boolean nameAvailable(String nameInput){

        for (DockingModel mod : mainApp.getDockingData()){
            if(nameInput.equals(mod.getName())){
                return false;
            }
        }
        return true;
    }
}
