package main.javaFX.controller;

import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import main.java.db.AdminDAO;
import main.javaFX.MainApp;
import main.javaFX.resource.AlertBox;

import java.sql.SQLException;


public class AdminLoginController {

    MainApp mainApp;

    @FXML
    private TextField adminEmail;
    @FXML
    private PasswordField adminPassword;


    public AdminLoginController(){}

    public void setMainApp(MainApp mainApp){
        this.mainApp = mainApp;
    }

    @FXML
    public void handleLogIn(){
        try {
            AdminDAO adminDAO = new AdminDAO();
            if(adminDAO.isPasswordCorrect(adminEmail.getText().trim().toLowerCase(),
                    adminPassword.getText().trim())){
                mainApp.setLoginName(adminEmail.getText().trim().toLowerCase());
                mainApp.showBikeOverview();
                mainApp.getRootLayoutController().showMenu(true);
            } else {
                AlertBox.showAlertBox(mainApp.getPrimaryStage(), "Log in failed!", "Log in failed!", "Username and/or password incorrect!");
                adminPassword.setText("");
            }

        } catch(SQLException e){
            e.printStackTrace();
        }
    }
}
