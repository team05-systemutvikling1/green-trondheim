package main.javaFX.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import main.java.db.UserDAO;
import main.javaFX.MainApp;
import main.javaFX.model.UserModel;
import main.javaFX.resource.AlertBox;

import javax.swing.*;
import java.sql.SQLException;

public class UserOverviewController {
    private MainApp mainApp;

    @FXML
    private TableView<UserModel> userTable;
    @FXML
    private TableColumn<UserModel, String> lastNameColumn;
    @FXML
    private TableColumn<UserModel, String> emailColumn;

    @FXML
    private Label email;
    @FXML
    private Label nationalID;
    @FXML
    private Label firstName;
    @FXML
    private Label lastName;
    @FXML
    private Label phoneNumber;



    public UserOverviewController(){}

    @FXML
    private void initialize(){
        // Initialize the user table with the two columns.
        lastNameColumn.setCellValueFactory(
                cellData -> cellData.getValue().last_nameProperty());
        emailColumn.setCellValueFactory(
                cellData -> cellData.getValue().e_mailProperty());

        // Clear userTable
        showUserDetails(null);

        // Listen for selection changes and update user details when changed.
        userTable.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldvalue, newvalue) -> showUserDetails(newvalue));
    }

    @FXML
    private void handleLogout(){
        mainApp.showAdminLogin();
    }

    @FXML
    private void handleDeleteUser() {
        int selectedIndex = userTable.getSelectionModel().getSelectedIndex();
        if(selectedIndex >= 0) {
            String userEmail = userTable.getItems().get(selectedIndex).getE_mail();
            int delete = JOptionPane.showConfirmDialog(null,
                    "Delete user: " + userEmail,
                    "WARNING!", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
            if (delete == 0) {
                try {
                    UserDAO user = new UserDAO();
                    if (user.delUser(userEmail)) {
                        JOptionPane.showMessageDialog(null, "User: " + userEmail + " deleted.");
                        userTable.getItems().remove(selectedIndex);
                        showUserDetails(null);
                    } else {
                        JOptionPane.showMessageDialog(null,
                                "Oops, something went wrong trying to delete user: " + userEmail);
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(null,
                            "Oops, something went wrong trying to delete user: " + userEmail);
                }
            }
        } else {
            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No User Selected");
            alert.setContentText("Please select a user in the table.");

            alert.showAndWait();
            AlertBox.showAlertBox(mainApp.getPrimaryStage(), "No Selection", "No User Selected",
                    "Please select a user in the table.");
        }
    }

    @FXML
    private void handleEditUser() {
        int selectedIndex = userTable.getSelectionModel().getSelectedIndex();
        if(selectedIndex >= 0){
            UserModel userModel = userTable.getSelectionModel().getSelectedItem();
            mainApp.showUserEditDialog(userModel);
            userTable.refresh();
            showUserDetails(userTable.getSelectionModel().getSelectedItem());
        } else {
            AlertBox.showAlertBox(mainApp.getPrimaryStage(), "No Selection!", "No User Selected!",
                    "Please select a user in the table.");
        }
    }

    @FXML
    private void handleAddUser() {
        mainApp.showUserAddDialog();
    }

    private void showUserDetails(UserModel userModel) {
        if (userModel != null) {
            email.setText(userModel.getE_mail());
            nationalID.setText(Long.toString(userModel.getNational_id()));
            firstName.setText(userModel.getFirst_name());
            lastName.setText(userModel.getLast_name());
            phoneNumber.setText(Integer.toString(userModel.getPhone_nr()));
        } else {
            email.setText("");
            nationalID.setText("");
            firstName.setText("");
            lastName.setText("");
            phoneNumber.setText("");
        }
    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;

        // Add observable list data to the table
        userTable.setItems(mainApp.getUserData());
    }
}
