package main.javaFX.controller;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import main.java.data.Type;
import main.java.db.TypeDAO;
import main.javaFX.MainApp;
import main.javaFX.resource.AlertBox;

import java.sql.SQLException;

public class AddBikeTypeController {

    private MainApp mainApp;
    private Stage dialogStage;

    @FXML
    private TextField newType;

    public AddBikeTypeController(){}

    @FXML
    private void addType(){
        if(checkTypeAvailable(newType.getText().trim())) {
            try {
                TypeDAO typeDAO = new TypeDAO();
                typeDAO.addType(new Type(newType.getText().trim()));
                mainApp.updateTypeData();
                AlertBox.showConfirmationBox(dialogStage, "Type added!",
                        "Type added!","");
                newType.setText("");
            } catch (SQLException e){
                e.printStackTrace();
                AlertBox.showAlertBox(dialogStage, "Database issue!", "Database issue",
                        "Seems like something went wrong when trying to update the database.");
            }
        }else{
            AlertBox.showAlertBox(dialogStage, "Type exists!",
                    "Type already exists!", "");
        }
    }

    public void setMainApp(MainApp mainApp){
        this.mainApp = mainApp;
    }

    public void setDialogStage(Stage dialogStage){
        this.dialogStage = dialogStage;
    }

    private boolean checkTypeAvailable(String newType){
        for(String type : mainApp.getTypeData()){
            if(type.equals(newType)){
                return false;
            }
        }
        return true;
    }
}
