package main.javaFX.controller;

import com.lynden.gmapsfx.javascript.object.LatLong;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import main.java.data.DockingStation;
import main.java.db.DockingStationDAO;
import main.javaFX.MainApp;
import main.javaFX.model.DockingModel;
import main.javaFX.resource.AlertBox;

import java.util.Locale;

public class DockingEditDialogController {

    public MainApp mainApp;

    public Stage dialogStage;
    public DockingModel dockingModel;
    public boolean okClicked;

    @FXML
    public Label editName;
    @FXML
    public TextField editAddress;
    @FXML
    public TextField editX_coordinate;
    @FXML
    public TextField editY_coordinate;
    @FXML
    public TextField editCapacity;

    public DockingEditDialogController(){}

    @FXML
    public void initialize(){
    }

    @FXML
    public void getPosition(){
        LatLong coordinates = mainApp.getPosition();
        if(coordinates != null){
            editX_coordinate.setText(String.format(Locale.ROOT, "%.6f", coordinates.getLatitude()));
            editY_coordinate.setText(String.format(Locale.ROOT, "%.6f", coordinates.getLongitude()));
            mainApp.setCoordinates(null);
        }
    }

    @FXML
    public void handleOk(){
        if(isInputValid()){
            dockingModel.setName(editName.getText());
            dockingModel.setAddress(editAddress.getText());
            dockingModel.setX_coordinate(Double.parseDouble(editX_coordinate.getText()));
            dockingModel.setY_coordinate(Double.parseDouble(editY_coordinate.getText()));
            dockingModel.setMax_bikes(Integer.parseInt(editCapacity.getText()));

            try {
                DockingStationDAO dockingDAO = new DockingStationDAO();
                DockingStation ds = new DockingStation(editName.getText(),
                        editAddress.getText().trim(), Double.parseDouble(editX_coordinate.getText()),
                        Double.parseDouble(editY_coordinate.getText()), Integer.parseInt(editCapacity.getText()));

                if(dockingDAO.updateDockingStation(ds.getName(), ds)) {
                    mainApp.updateDockingData();
                    okClicked = true;
                }else{
                    okClicked = false;
                }

            }catch (Exception e){
                e.printStackTrace();
                okClicked = AlertBox.showAlertBox(dialogStage, "Database issue!", "Database issue",
                        "Seems like something went wrong when trying to update the database.");
            }
            dialogStage.close();
        }
    }
    @FXML
    public void handleCancel(){
        dialogStage.close();
    }

    public boolean isOkClicked() {
        return okClicked;
    }

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public void setDockingStation(DockingModel dockingModel){
        this.dockingModel = dockingModel;

        editName.setText(dockingModel.getName());
        editAddress.setText(dockingModel.getAddress());
        editX_coordinate.setText(String.valueOf(dockingModel.getX_coordinate()));
        editY_coordinate.setText(String.valueOf(dockingModel.getY_coordinate()));
        editCapacity.setText(String.valueOf(dockingModel.getMax_bikes()));
    }


    public boolean isInputValid(){
        String errorMessage = "";

        if(editName.getText() == null || editName.getText().trim().length() == 0) {
            errorMessage += "Invalid name!\n";
        }

        if(editAddress.getText() == null || editAddress.getText().trim().length() == 0){
            errorMessage += "Invalid address!\n";
        }

        if(editX_coordinate.getText() == null || editX_coordinate.getText().trim().length() == 0 ||
                editX_coordinate.getText().trim().length() > 9){
            errorMessage += "Invalid X_coordinate (cannot be more than 8 digits)!\n";
        } try {
            Double.parseDouble(editX_coordinate.getText());
        } catch (IllegalArgumentException e) {
            errorMessage += "Invalid X_coordinate (must be a decimal separated by '.')!\n";
        }

        if(editY_coordinate.getText() == null || editY_coordinate.getText().trim().length() == 0 ||
                editY_coordinate.getText().trim().length() > 9){
            errorMessage += "Invalid Y_coordinate (cannot be more than 8 digits)!\n";
        } try {
            Double.parseDouble(editY_coordinate.getText());
        } catch (IllegalArgumentException e) {
            errorMessage += "Invalid Y_coordinate (must be a decimal separated by '.')!\n";
        }

        if(editCapacity.getText() == null || editCapacity.getText().trim().length() == 0){
            errorMessage += "Invalid capacity!\n";
        } try {
            Integer.parseInt(editCapacity.getText());
            if(Integer.parseInt(editCapacity.getText()) < 1){
                errorMessage += "Invalid capacity (cannot be less than 1)!\n";
            }
        } catch (IllegalArgumentException e) {
            errorMessage += "Invalid capacity (must be an integer)!\n";
        }


        if(errorMessage.length() == 0) {
            return true;
        }else {
            return AlertBox.showAlertBox(dialogStage, "Invalid fields!", "Please correct invalid fields!", errorMessage);
        }
    }
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }


}
