package main.javaFX.controller;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import main.java.data.CardInfo;
import main.java.data.User;
import main.java.db.CardInfoDAO;
import main.java.db.UserDAO;
import main.javaFX.MainApp;
import main.javaFX.resource.AlertBox;


import java.sql.SQLException;

public class UserAddDialogController {

    MainApp mainApp;
    Stage dialogStage;
    boolean okClicked = false;

    @FXML
    private TextField addEmail;
    @FXML
    private TextField addNationalID;
    @FXML
    private TextField addFirstName;
    @FXML
    private TextField addLastName;
    @FXML
    private TextField addPhone;

    @FXML
    private TextField addCardNumber;
    @FXML
    private TextField addExpiration;
    @FXML
    private TextField addCardFirstName;
    @FXML
    private TextField addCardLastName;

    public UserAddDialogController(){}

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    public void setDialogStage(Stage dialogStage){
        this.dialogStage = dialogStage;
    }

    public boolean isOkClicked(){
        return okClicked;
    }

    public void handleOk(){
        if(isInputValid()){
            boolean cardInfoAdded = false;
            boolean userAdded = false;
            try {
                // Try adding the card information first, as national id is a primary key in card_info.
                CardInfo newCardInfo = new CardInfo(addFirstName.getText().trim(), addLastName.getText().trim(),
                        Long.parseLong(addNationalID.getText()), Long.parseLong(addCardNumber.getText()),
                        Integer.parseInt(addExpiration.getText()));
                CardInfoDAO cardInfoDAO = new CardInfoDAO();
                cardInfoDAO.addCardInfo(newCardInfo);
                cardInfoAdded = true;

                // Try adding the user, since adding cardInfo was successful.
                User newUser = new User();
                newUser.seteMail(addEmail.getText().trim().toLowerCase());
                newUser.setNationalId(Long.parseLong(addNationalID.getText()));
                newUser.setFirstName(addFirstName.getText().trim());
                newUser.setLastName(addLastName.getText().trim());
                newUser.setPhoneNr(Integer.parseInt(addPhone.getText()));
                UserDAO userDAO = new UserDAO();
                userDAO.addUser(newUser);
                mainApp.updateUserData();
                AlertBox.showConfirmationBox(dialogStage, "User Added", "Success!",
                        "The user was successfully added to the database.");
                userAdded = true;
            } catch (SQLException e){
                e.printStackTrace();
                okClicked = AlertBox.showAlertBox(dialogStage, "Database issue!", "Database issue",
                        "Seems like something went wrong when trying to update the database.");
            }
            if(cardInfoAdded && !(userAdded)){
                try {
                    CardInfoDAO cardInfoDAO = new CardInfoDAO();
                    cardInfoDAO.delCardInfo(Long.parseLong(addNationalID.getText()));
                } catch (SQLException f){
                    f.printStackTrace();
                    AlertBox.showAlertBox(dialogStage, "Database issue!", "Delete card info",
                            "Seems like something went wrong when trying" +
                                    " to auto-delete card-info from database.");
                }
            }
            dialogStage.close();
        }
    }

    public void handleCancel(){
        dialogStage.close();
    }

    private boolean isInputValid(){
        String errorMessage = "";

        if(addNationalID.getText() == null || addNationalID.getText().trim().length() == 0){
            errorMessage += "Invalid national id!\n";
        } else {
            try {
                Long.parseLong(addNationalID.getText());
            } catch (IllegalArgumentException e) {
                errorMessage += "Invalid national id (must be an integer)!\n";
            }
        }

        if(nationalIDAvailable(Long.parseLong(addNationalID.getText())) == -1){
            errorMessage += "National id already in use!\n";
        } else if(nationalIDAvailable(Long.parseLong(addNationalID.getText())) == 0){
            return AlertBox.showAlertBox(dialogStage, "Database issue!", "Database issue",
                    "Seems like something went wrong when trying to update the database.");
        }

        if(addCardFirstName.getText() == null || addCardFirstName.getText().trim().length() == 0){
            errorMessage += "Invalid Cardholder First Name!\n";
        }

        if(addCardLastName.getText() == null || addCardLastName.getText().trim().length() == 0){
            errorMessage += "Invalid cardholder last name!\n";
        }

        if(addCardNumber.getText() == null || addCardNumber.getText().trim().length() == 0 ||
                addCardNumber.getText().trim().length() != 16){
            errorMessage += "Invalid card cumber (must be 16 digits)!\n";
        } else {
            try {
                Long.parseLong(addCardNumber.getText());
            } catch (IllegalArgumentException e) {
                errorMessage += "Invalid card number (must be an integer)!\n";
            }
        }

        if(addExpiration.getText() == null || addExpiration.getText().trim().length() < 3 ||
                addExpiration.getText().trim().length() > 4){
            errorMessage += "Invalid expiration date (must be 3 or 4 digits)!\n";
        }

        if(addEmail.getText() == null || addEmail.getText().trim().length() == 0) {
            errorMessage += "Invalid Email!\n";
        }else if(!(mainApp.emailAvailable(addEmail.getText().trim().toLowerCase()))){
            errorMessage += "Email already in use!\n";
        }


        if(addFirstName.getText() == null || addFirstName.getText().trim().length() == 0){
            errorMessage += "Invalid first name!\n";
        }

        if(addLastName.getText() == null || addLastName.getText().trim().length() == 0){
            errorMessage += "Invalid last name!\n";
        }

        if(addPhone.getText() == null || addPhone.getText().trim().length() == 0){
            errorMessage += "Invalid phone number!\n";
        } try {
            Integer.parseInt(addPhone.getText());
        } catch (IllegalArgumentException e) {
            errorMessage += "Invalid phone number (must be an integer)!\n";
        }


        if(errorMessage.length() == 0) {
            return true;
        }else {
            return AlertBox.showAlertBox(dialogStage, "Invalid fields!", "Please correct invalid fields!", errorMessage);
        }
    }

    public int nationalIDAvailable(long idGiven){
        try {
            CardInfoDAO cardInfoDAO = new CardInfoDAO();
            if(cardInfoDAO.getCardInfo(idGiven) == null){
                return 1;
            }else{
                return -1;
            }
        } catch (SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
}
