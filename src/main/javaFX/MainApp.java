package main.javaFX;

import com.lynden.gmapsfx.javascript.object.LatLong;
import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import main.java.data.*;
import main.java.db.*;
import main.javaFX.controller.*;
import main.javaFX.model.*;
import main.javaFX.resource.AlertBox;
import sun.applet.Main;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

public class MainApp extends Application {

    private Stage primaryStage;
    private BorderPane rootLayout;
    public RootLayoutController rootLayoutController;
    private String loginName = null;

    private ObservableList<BikeModel> bikeData = FXCollections.observableArrayList();
    private ObservableList<DockingModel> dockingData = FXCollections.observableArrayList();
    private ObservableList<UserModel> userData = FXCollections.observableArrayList();
    private ObservableList<String> typeData = FXCollections.observableArrayList();
    private ObservableList<MaintenanceModel> repData = FXCollections.observableArrayList();
    private ObservableList<WorkshopModel>  wsData = FXCollections.observableArrayList();
    private ObservableList<String>  usernames = FXCollections.observableArrayList();

    private LatLong coordinates;

    public MainApp() {

        // Fill up bikeData list with bikes from the database.
        updateBikeData();

        // Fill up dockingData list with docking stations from database.
        updateDockingData();

        // Fill up userData list with users from database.
        updateUserData();

        // Fill up typeData list with types from database.
        updateTypeData();

        updateWSData();

        updateRepData();

        // Fill up usernames list from database.
        updateUsernames();

    }

    public void setLoginName(String loginName){
        this.loginName = loginName;
    }

    public String getLoginName(){
        return loginName;
    }

    public void updateBikeData(){
        try{
            // Fill up bikeData list with bikes from the database.
            BikeDAO bikeDAO = new BikeDAO();

            ArrayList<Bike> allBikes = bikeDAO.getAllBikes();
            if(bikeData.size() != 0){
                bikeData.clear();
            }
            for (Bike bike : allBikes) {
                BikeModel bikeModel = new BikeModel(bike.getBikeId(), bike.getName(), bike.isNeedMaintenance(),
                        bike.getType(), bike.getPrice(), bike.getDateBought().toLocalDate(), bike.getCharge(),
                        bike.getxCoordinate(), bike.getyCoordinate(), bike.getMake());
                bikeData.add(bikeModel);
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void updateDockingData(){
        try {
            DockingStationDAO dockingDAO = new DockingStationDAO();
            ArrayList<DockingStation> allDockingStations = dockingDAO.getAllDockingStations();
            if(dockingData.size() != 0){
                dockingData.clear();
            }
            int i = 0;
            for (DockingStation ds : allDockingStations) {
                DockingModel dockingModel = new DockingModel(ds.getName(), ds.getAddress(),
                        ds.getxCoordinate(), ds.getyCoordinate(), ds.getMaxBikes(), dockingDAO.getDockingBikes(ds.getName()).size());
                dockingData.add(dockingModel);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void updateUserData(){
        try {
            UserDAO userDAO = new UserDAO();
            ArrayList<User> allUsers = userDAO.getAllUsers();
            if(userData.size() != 0){
                userData.clear();
            }
            for (User usr : allUsers) {
                UserModel userModel = new UserModel(usr.geteMail(), usr.getNationalId(), usr.getFirstName(),
                        usr.getLastName(), usr.getPhoneNr());
                userData.add(userModel);
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public  void updateTypeData(){
        try {
            TypeDAO typeDAO = new TypeDAO();
            ArrayList<Type> allTypes = typeDAO.getAllTypes();
            if(typeData.size() != 0){
                typeData.clear();
            }
            for(Type type : allTypes){
                typeData.add(type.getType());
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void updateRepData(){
        try{
            MaintenanceDAO maintenanceDAO = new MaintenanceDAO();
            ArrayList<Maintenance> allMaintenance = maintenanceDAO.getAllMaintenances();
            if(repData.size() != 0){
                repData.clear();
            }
            for(Maintenance maintenance : allMaintenance){
                //int orgNr,int repOrder, double price, String documentation, LocalDate startDate, LocalDate endDate
                int currentWS = -1;
                for(int i = 0; i < wsData.size(); i++) {
                    if(wsData.get(i).getOrgNr() == maintenance.getOrgNr()){
                        currentWS = i;
                    }
                }
                if(currentWS >= 0) {
                    repData.add(new MaintenanceModel(maintenance.getOrgNr(), maintenance.getRepOrder(), maintenance.getPrice(),
                            maintenance.getDoc(), maintenance.getLocalStartDate(), maintenance.getLocalEndDate(), maintenance.getBikeId(), wsData.get(currentWS)));
                }else{
                    AlertBox.showAlertBox(getPrimaryStage(), "Invalid workshop", "Organisation number not valid!",
                            "Please check the organisation number.");
                }
            }

        }catch (SQLException e){
            e.printStackTrace();

        }

    }

    public void updateWSData(){
        try{
            WorkshopDAO workshopDAO = new WorkshopDAO();
            ArrayList<Workshop> allWorkshops = workshopDAO.getAllWorkshops();

            if(wsData.size() != 0){
                wsData.clear();
            }

            for(Workshop workshop : allWorkshops){
                wsData.add(new WorkshopModel(workshop.getOrgName(), workshop.getPhoneNr(), workshop.geteMail(), workshop.getAddress(), workshop.getOrgNr()));
            }

        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    public void updateUsernames(){
        try{
            AdminDAO adminDAO = new AdminDAO();
            ArrayList<Admin> allAdmins = adminDAO.getAllAdmins();

            if(usernames.size() != 0){
                usernames.clear();
            }

            for (Admin admin : allAdmins){
                usernames.add(admin.getUsername());
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    /**
     * Returns all bikes as an observable list.
     * @return
     */
    public ObservableList<BikeModel> getBikeData() {
        return bikeData;
    }

    /**
     * Returns all docking stations as an observable list.
     * @return
     */
    public ObservableList<DockingModel> getDockingData() {
        return dockingData;
    }

    /**
     * Returns all users as an observable list.
     * @return
     */
    public ObservableList<UserModel> getUserData() { return userData; }

    public ObservableList<String> getTypeData() { return typeData; }

    public ObservableList<MaintenanceModel> getRepData() {
        return repData;
    }

    public ObservableList<WorkshopModel> getWsData() {
        return wsData;
    }

    public ObservableList<String> getUsernames() {
        return usernames;
    }

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Administration - Green Trondheim");
        this.primaryStage.setMinWidth(1200);
        this.primaryStage.setMinHeight(600);

        initRootLayout();

        //primaryStage.getIcons().add(new Image("/main/javafx/media/bikeicon.png"));

    }

    /**
     * Initializes the root layout.
     */
    public void initRootLayout(){
        try{
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/main/javaFX/view/RootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();
            RootLayoutController controller = loader.getController();
            rootLayoutController = controller;
            controller.setMainApp(this);

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent t) {
                    System.out.println("Close window clicked.");
                    Platform.exit();
                    System.exit(0);
                }
            });
            primaryStage.show();
            showAdminLogin();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showAdminLogin() {
        try {
            // Load admin log in page.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/main/javaFX/view/AdminLogin.fxml"));
            AnchorPane adminLoginLayout = (AnchorPane) loader.load();

            // Set adminLoginLayout into the center of root layout.
            rootLayout.setCenter(adminLoginLayout);
            rootLayoutController.showMenu(false);
            setLoginName(null);

            // Give the controller access to the main app.
            AdminLoginController controller = loader.getController();
            controller.setMainApp(this);



        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showPosition(double latitude, double longitude, String title){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/main/javaFX/view/ShowPosition.fxml"));
            ShowPositionController controller = new ShowPositionController(latitude, longitude, title);
            controller.setMainApp(this);
            loader.setController(controller);
            AnchorPane showPositionLayout = loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle(title);
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(showPositionLayout);
            dialogStage.setScene(scene);



            dialogStage.showAndWait();


        }catch (IOException e){
            e.printStackTrace();

        }
    }

    public LatLong getPosition() {
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/main/javaFX/view/GetPosition.fxml"));
            AnchorPane showPositionLayout = loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Click!");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(showPositionLayout);
            dialogStage.setScene(scene);

            // Give the controller access to the main app.
            GetPositionController controller = loader.getController();
            controller.setMainApp(this);
            controller.setDialogStage(dialogStage);

            dialogStage.showAndWait();

            return coordinates;
        }catch (IOException e){
            e.printStackTrace();
            return null;
        }

    }

    public void setCoordinates(LatLong coordinates) {
        this.coordinates = coordinates;
    }

    /**
     * Shows the person overview inside the root layout.
     */
    public void showBikeOverview() {
        try {
            // Load bike overview.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/main/javaFX/view/BikeOverview.fxml"));
            AnchorPane bikeOverview = (AnchorPane) loader.load();

            // Set bike overview into the center of root layout.
            rootLayout.setCenter(bikeOverview);

            // Give the controller access to the main app.
            BikeOverviewController controller = loader.getController();
            controller.setMainApp(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public boolean showBikeEditDialog(BikeModel bike) {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/main/javaFX/view/BikeEditDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edit Bike");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the person into the controller.
            BikeEditDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setMainApp(this);
            controller.setDockingStations(dockingData);
            controller.setTypes(typeData);
            controller.setBike(bike);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean showDockingEditDialog(DockingModel dockingModel) {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/main/javaFX/view/DockingEditDialog.fxml"));
            AnchorPane page = loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edit docking station");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the docking names and specific docking station into the controller.
            DockingEditDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setMainApp(this);
            controller.setDockingStation(dockingModel);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean showUserEditDialog(UserModel userModel) {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/main/javaFX/view/UserEditDialog.fxml"));
            AnchorPane page = loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edit user");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the user into the controller.
            UserEditDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setMainApp(this);
            controller.setUser(userModel);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean showBikeAddDialog() {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/main/javaFX/view/BikeAddDialog.fxml"));
            AnchorPane page = loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Add Bike");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the docking names into the controller.
            BikeAddDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setMainApp(this);
            controller.setDockingNames(dockingData);
            controller.setTypes(typeData);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();
            boolean bikeAdded = controller.isOkClicked();
            if (bikeAdded){
                updateBikeData();
            }
            return bikeAdded;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean showWorkshopAddDialog(){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/main/javaFX/view/WorkshopAddDialog.fxml"));
            AnchorPane page = loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("New workshop");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            WorkshopAddDialogController controller = loader.getController();
            controller.setMainApp(this);
            controller.setDialogStage(dialogStage);

            dialogStage.showAndWait();
            return true;

        }catch(IOException e){
            e.printStackTrace();
            return false;
        }
    }

    public boolean showWorkshopEditDialog(){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/main/javaFX/view/WorkshopEditDialog.fxml"));
            AnchorPane page = loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edit workshop");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            WorkshopEditController controller = loader.getController();
            controller.setMainApp(this);
            controller.setDialogstage(dialogStage);
            controller.setWsList(wsData);

            dialogStage.showAndWait();
            return true;
        }catch(IOException e){
            e.printStackTrace();
            return false;
        }
    }

    public boolean showBikeMaintenanceDialog(){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/main/javaFX/view/MaintenanceOverview.fxml"));
            AnchorPane page = loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Send to maintenance");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            MaintenanceOverviewController controller = loader.getController();
            controller.setDialogstage(dialogStage);
            controller.setMainApp(this);

            Scene scene = new Scene(page);
            dialogStage.setScene(scene);
            dialogStage.showAndWait();
            updateWSData();
            updateRepData();
            return true;

        }catch(IOException e){
            e.printStackTrace();
            return false;
        }

    }

    public boolean showMaintenanceAddDialog(Stage dialogStage){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/main/javaFX/view/MaintenanceAddDialog.fxml"));
            AnchorPane page = loader.load();

            Stage addDialogStage = new Stage();
            addDialogStage.setTitle("Add maintenance for bike");
            addDialogStage.initModality(Modality.WINDOW_MODAL);
            addDialogStage.initOwner(dialogStage);

            MaintenanceAddDialogController controller = loader.getController();
            controller.setDialogStage(addDialogStage);
            controller.setMainApp(this);
            controller.setBikeList(bikeData);
            controller.setWorkshopList(wsData);

            Scene scene = new Scene(page);
            addDialogStage.setScene(scene);
            addDialogStage.showAndWait();
            return true;

        }catch(IOException e){
            e.printStackTrace();
            return false;
        }
    }

    public boolean showMaintenanceEditDialog(Stage dialogStage, int repId, int bikeId, String documentation){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/main/javaFX/view/MaintenanceEditDialog.fxml"));
            AnchorPane page = loader.load();

            Stage editDialogStage = new Stage();
            editDialogStage.setTitle("Edit maintenance for bike");
            editDialogStage.initModality(Modality.WINDOW_MODAL);
            editDialogStage.initOwner(dialogStage);

            MaintenanceEditDialogController controller = loader.getController();
            controller.setDialogStage(editDialogStage);
            controller.setMainApp(this);
            controller.setDockingList(dockingData);
            controller.setRepId(repId);
            controller.setBikeId(bikeId);
            controller.setDocumentation(documentation);

            Scene scene = new Scene(page);
            editDialogStage.setScene(scene);
            editDialogStage.showAndWait();
            return true;

        }catch(IOException e){
            e.printStackTrace();
            return false;
        }
    }

    public boolean showDockingAddDialog() {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/main/javaFX/view/DockingAddDialog.fxml"));
            AnchorPane page = loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Add Docking Station");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the mainApp  into the controller.
            DockingAddDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setMainApp(this);


            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            boolean stationAdded = controller.isOkClicked();
            if(stationAdded){
                updateDockingData();
                return true;
            }
            else return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean showUserAddDialog() {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/main/javaFX/view/UserAddDialog.fxml"));
            AnchorPane page = loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Add User");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the mainApp  into the controller.
            UserAddDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setMainApp(this);


            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            boolean userAdded = controller.isOkClicked();
            if(userAdded){
                updateUserData();
                return true;
            }
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean showAdminWindow() {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/main/javaFX/view/AdminOverview.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Admin overview");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Connect controller with data and mainApp.
            AdminOverviewController controller = loader.getController();
            controller.setMainApp(this);
            controller.setUsernames(usernames);


            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean showAddBikeType(){
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/main/javaFX/view/AddBikeType.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Admin overview");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Connect controller with data and mainApp.
            AddBikeTypeController controller = loader.getController();
            controller.setMainApp(this);
            controller.setDialogStage(dialogStage);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();
            return true;
        }catch (IOException e){
            e.printStackTrace();
            return false;
        }
    }

    public BorderPane getRootLayout() {
        return rootLayout;
    }

    /**
     * Returns the main stage.
     * @return
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }
    public RootLayoutController getRootLayoutController(){
        return rootLayoutController;
    }

    public boolean emailAvailable(String otherEmail) {
        for(UserModel userModel : userData){
            if(userModel.getE_mail().equals(otherEmail)){
                return false;
            }
        }
        return true;
    }


    public static void main(String[] args) {
        if (args.length == 2) {
            Db.setNewDB(args[0], args[1]);
        } else if (args.length == 3) {
            Db.setNewDB(args[0], args[1], args[2]);
        }

        launch(args);
    }
}
