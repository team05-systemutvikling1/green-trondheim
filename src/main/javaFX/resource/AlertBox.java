package main.javaFX.resource;

import javafx.scene.control.Alert;
import javafx.stage.Stage;

public class AlertBox {

    public static final boolean showAlertBox(Stage dialogStage, String title, String headerText, String errorMessage){
        // Show the error message.
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.getDialogPane().setMinHeight(100);
        alert.initOwner(dialogStage);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(errorMessage);

        alert.showAndWait();

        return false;
    }

    public static final void showConfirmationBox(Stage dialogStage, String title, String headerText, String message){
        // Show the conformation message.
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.getDialogPane().setMinHeight(100);
        alert.initOwner(dialogStage);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(message);

        alert.showAndWait();
    }
    public static final void showInformationBox(Stage dialogStage, String title, String headerText, String message){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.getDialogPane().setMinHeight(100);
        alert.initOwner(dialogStage);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(message);

        alert.showAndWait();
    }
}
