package main.javaFX.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class WorkshopModel {
    private IntegerProperty orgNr;
    private StringProperty orgName;
    private IntegerProperty phoneNr;
    private StringProperty eMail;
    private StringProperty adress;

    public int getOrgNr() {
        return orgNr.get();
    }

    public IntegerProperty orgNrProperty() {
        return orgNr;
    }

    public void setOrgNr(int orgNr) {
        this.orgNr.set(orgNr);
    }

    public WorkshopModel(String orgName, int phoneNr, String eMail, String adress, int orgNr){
        this.orgNr = new SimpleIntegerProperty(orgNr);

        this.orgName = new SimpleStringProperty(orgName);
        this.phoneNr = new SimpleIntegerProperty(phoneNr);
        this.eMail = new SimpleStringProperty(eMail);
        this.adress = new SimpleStringProperty(adress);

    }


    public String getOrgName() {
        return orgName.get();
    }

    public StringProperty orgNameProperty() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName.set(orgName);
    }

    public int getPhoneNr() {
        return phoneNr.get();
    }

    public IntegerProperty phoneNrProperty() {
        return phoneNr;
    }

    public void setPhoneNr(int phoneNr) {
        this.phoneNr.set(phoneNr);
    }

    public String geteMail() {
        return eMail.get();
    }

    public StringProperty eMailProperty() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail.set(eMail);
    }

    public String getAdress() {
        return adress.get();
    }

    public StringProperty adressProperty() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress.set(adress);
    }
}
