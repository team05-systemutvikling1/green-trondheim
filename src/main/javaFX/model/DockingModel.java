package main.javaFX.model;

import javafx.beans.property.*;


/**
 * Class for DockingModel object
 *
 * @author Ørjan
 */
public class DockingModel {
    private StringProperty name;
    private StringProperty address;
    private DoubleProperty x_coordinate;
    private DoubleProperty y_coordinate;
    private IntegerProperty max_bikes;
    private IntegerProperty currentBikes;

    public DockingModel(String name, String address, double x_coordinate,
                        double y_coordinate, int max_bikes, int currentBikes)
    {
        this.name = new SimpleStringProperty(name);
        this.address = new SimpleStringProperty(address);
        this.x_coordinate = new SimpleDoubleProperty(x_coordinate);
        this.y_coordinate = new SimpleDoubleProperty(y_coordinate);
        this.max_bikes = new SimpleIntegerProperty(max_bikes);
        this.currentBikes = new SimpleIntegerProperty(currentBikes);
    }

    public DockingModel() {}

    public StringProperty nameProperty() {
        return name;
    }

    public String getName(){
        return name.get();
    }

    public void setName(String name){
        this.name.set(name);
    }

    public StringProperty addressProperty(){
        return address;
    }

    public String getAddress(){
        return address.get();
    }

    public void setAddress(String address){
        this.address.set(address);
    }

    public double getX_coordinate() { return x_coordinate.get(); }

    public void setX_coordinate(double x_coordinate) {
        this.x_coordinate.set(x_coordinate);
    }

    public DoubleProperty x_coordinateProperty() {
        return x_coordinate;
    }

    public double getY_coordinate() {
        return y_coordinate.get();
    }

    public void setY_coordinate(double y_coordinate) {
        this.y_coordinate.set(y_coordinate);
    }

    public DoubleProperty y_coordinateProperty() {
        return y_coordinate;
    }

    public IntegerProperty max_bikesProperty() {
        return max_bikes;
    }

    public int getMax_bikes(){
        return max_bikes.get();
    }

    public void setMax_bikes(int max_bikes){
        this.max_bikes.set(max_bikes);
    }

    public void setCurrentBikes(int currentBikes) {
        this.currentBikes.set(currentBikes);
    }

    public IntegerProperty currentBikesProperty() {
        return currentBikes;
    }

    public int getCurrentBikes(){
        return currentBikes.get();
    }

    @Override
    public boolean equals(Object stationcheck){
        if (stationcheck instanceof main.java.data.DockingStation){
            main.java.data.DockingStation s = (main.java.data.DockingStation) stationcheck;
            return name.get() == s.getName() && address.get() == s.getAddress()
                    && x_coordinate.get() == s.getxCoordinate()
                    && y_coordinate.get() == s.getyCoordinate()
                    && max_bikes.get() == s.getMaxBikes();
        }
        return false;

    }

    @Override
    public String toString(){
        return "Name: " + name + ", address: " + address
                + ", x-coordinate: " + x_coordinate
                + ", y-coordinate: " + y_coordinate
                + ", maximum bike capacity: " + max_bikes;
    }


}
