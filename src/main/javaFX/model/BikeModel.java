package main.javaFX.model;

import javafx.beans.property.*;

import java.time.LocalDate;
import java.sql.Date;

/**
 * Class for BikeModel object
 *
 * @author Ørjan
 */
public class BikeModel {
    private IntegerProperty bike_id;
    private StringProperty name;
    private BooleanProperty need_maintenance;
    private StringProperty type;
    private DoubleProperty price;
    private ObjectProperty<LocalDate> date_bought;
    private IntegerProperty charge;
    private DoubleProperty x_coordinate;
    private DoubleProperty y_coordinate;
    private StringProperty make;

    public BikeModel(int bike_id, String name, boolean need_maintenance, String type, double price,
                     LocalDate date_bought, int charge, double x_coordinate, double y_coordinate, String make) {
        this.bike_id = new SimpleIntegerProperty(bike_id);
        this.name = new SimpleStringProperty(name);
        this.need_maintenance = new SimpleBooleanProperty(need_maintenance);
        this.type = new SimpleStringProperty(type);
        this.price = new SimpleDoubleProperty(price);
        this.date_bought = new SimpleObjectProperty<LocalDate>(date_bought);
        this.charge = new SimpleIntegerProperty(charge);
        this.x_coordinate = new SimpleDoubleProperty(x_coordinate);
        this.y_coordinate = new SimpleDoubleProperty(y_coordinate);
        this.make = new SimpleStringProperty(make);
    }

    public BikeModel() {
    }

    public int getBike_id() {
        return bike_id.get();
    }

    public void setBike_id(int bike_id) {
        this.bike_id.set(bike_id);
    }

    public IntegerProperty bike_idProperty(){return bike_id;}

    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public StringProperty nameProperty(){
        return name;
    }

    public boolean isNeed_maintenance() {
        return need_maintenance.get();
    }

    public void setNeed_maintenance(boolean need_maintenance) {
        this.need_maintenance.set(need_maintenance);
    }

    public String getType() {
        return type.get();
    }

    public void setType(String type) {
        this.type.set(type);
    }

    public StringProperty typeProperty() { return type; }

    public double getPrice() {
        return price.get();
    }

    public void setPrice(double price) {
        this.price.set(price);
    }

    public LocalDate getDate_bought() {
        return date_bought.get();
    }

    public void setDate_bought(Date date_bought) {
        this.date_bought.set(date_bought.toLocalDate());
    }

    public void setDate_bought(LocalDate date_bought) { this.date_bought.set(date_bought); }

    public int getCharge() {
        return charge.get();
    }

    public void setCharge(int charge) {
        this.charge.set(charge);
    }

    public IntegerProperty chargeProperty(){ return charge; };

    public double getX_coordinate() {
        return x_coordinate.get();
    }

    public void setX_coordinate(double x_coordinate) {
        this.x_coordinate.set(x_coordinate);
    }

    public DoubleProperty x_coordinateProperty() { return x_coordinate; }

    public double getY_coordinate() {
        return y_coordinate.get();
    }

    public void setY_coordinate(double y_coordinate) {
        this.y_coordinate.set(y_coordinate);
    }

    public DoubleProperty y_coordinateProperty() {
        return y_coordinate;
    }

    public String getMake(){ return make.get(); }

    public void setMake(String make) { this.make.set(make); }

    public StringProperty makeProperty() { return make; }

    /**
     * Equals method to check if one object is equal to a Bike object
     *
     * @param bikecheck Object that should be checked if equals
     * @return true if equals, and false if not
     */
    @Override
    public boolean equals(Object bikecheck) {
        if (bikecheck instanceof BikeModel) {
            BikeModel b = (BikeModel) bikecheck;
            return bike_id.get() == b.getBike_id() && name.get() == b.getName() &&
                    need_maintenance.get() == b.isNeed_maintenance() && type.equals(b.getType()) &&
                    price.get() == b.getPrice() && date_bought.equals(b.date_bought);
        }
        return false;
    }

    @Override
    public String toString() {
        return "Bike id: " + bike_id + ", Name: " + name + ", Need maintenance: " + need_maintenance +
                ", Type: " + type  + ", Price: " + price  + ", Date purchased: " + date_bought;
    }
}