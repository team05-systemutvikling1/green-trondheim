package main.javaFX.model;

import javafx.beans.property.*;


/**
 * Class for UserModel object
 *
 * @author Ørjan
 */
public class UserModel {
    private StringProperty e_mail;
    private LongProperty national_id;
    private StringProperty first_name;
    private StringProperty last_name;
    private IntegerProperty phone_nr;
    // String password; Denne må vel lagres hashed, med salt, legger ikke inn foreløpig
    // String salt; Brukes til hashingen

    public UserModel(String e_mail, long national_id, String first_name, String last_name, int phone_nr) {
        this.e_mail = new SimpleStringProperty(e_mail);
        this.national_id = new SimpleLongProperty(national_id);
        this.first_name = new SimpleStringProperty(first_name);
        this.last_name = new SimpleStringProperty(last_name);
        this.phone_nr = new SimpleIntegerProperty(phone_nr);
    }

    public UserModel() {}

    public String getE_mail() {
        return e_mail.get();
    }

    public void setE_mail(String e_mail) { this.e_mail.set(e_mail); }

    public StringProperty e_mailProperty() { return e_mail; }

    public long getNational_id() {
        return national_id.get();
    }

    public void setNational_id(long national_id) {
        this.national_id.set(national_id);
    }

    public LongProperty national_idProperty() { return national_id; }

    public String getFirst_name() {
        return first_name.get();
    }

    public void setFirst_name(String first_name) { this.first_name.set(first_name); }

    public StringProperty first_nameProperty() { return first_name; }

    public String getLast_name() {
        return last_name.get();
    }

    public void setLast_name(String last_name) { this.last_name.set(last_name); }

    public StringProperty last_nameProperty() { return last_name; }

    public int getPhone_nr() {
        return phone_nr.get();
    }

    public void setPhone_nr(int phone_nr) {
        this.phone_nr.set(phone_nr);
    }

    public IntegerProperty phone_nrProperty() { return phone_nr; }

    /**
     * Equals method to check if one object is equal to a UserModel object
     *
     * @param userModelCheck Object that should be checked if equals
     * @return true if equals, and false if not
     */
    @Override
    public boolean equals(Object userModelCheck) {
        if (userModelCheck instanceof main.java.data.User) {
            UserModel usr = (UserModel) userModelCheck;
            return e_mail.get().equals(usr.getE_mail()) && national_id.get() == usr.getNational_id() &&
                    first_name.get().equals(usr.getFirst_name()) && last_name.get().equals(usr.getLast_name()) &&
                    phone_nr.get() == usr.getPhone_nr();

        }
        return false;
    }

    @Override
    public String toString() {
        return e_mail + ", " + national_id + ", " + first_name + ", " + last_name + ", " + phone_nr;
    }
}