package main.javaFX.model;

import javafx.beans.property.*;

import java.time.LocalDate;

public class MaintenanceModel {
    public int getRepOrder() {
        return repOrder.get();
    }

    public IntegerProperty repOrderProperty() {
        return repOrder;
    }

    public void setRepOrder(int repOrder) {
        this.repOrder.set(repOrder);
    }

    public double getPrice() {
        return price.get();
    }

    public DoubleProperty priceProperty() {
        return price;
    }

    public void setPrice(double price) {
        this.price.set(price);
    }

    public String getDocumentation() {
        return documentation.get();
    }

    public StringProperty documentationProperty() {
        return documentation;
    }

    public void setDocumentation(String documentation) {
        this.documentation.set(documentation);
    }

    public LocalDate getStartDate() {
        return startDate.get();
    }

    public ObjectProperty<LocalDate> startDateProperty() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate.set(startDate);
    }

    public LocalDate getEndDate() {
        return endDate.get();
    }

    public ObjectProperty<LocalDate> endDateProperty() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate.set(endDate);
    }

    private IntegerProperty repOrder;

    public int getOrgNr() {
        return orgNr.get();
    }

    public IntegerProperty orgNrProperty() {
        return orgNr;
    }

    public void setOrgNr(int orgNr) {
        this.orgNr.set(orgNr);
    }

    private IntegerProperty orgNr;
    private DoubleProperty price;
    private StringProperty documentation;
    private ObjectProperty<LocalDate> startDate;
    private ObjectProperty<LocalDate> endDate;
    private IntegerProperty bikeId;
    private StringProperty orgName;
    private IntegerProperty phoneNr;
    private StringProperty eMail;
    private StringProperty adress;


    public int getBikeId() {
        return bikeId.get();
    }

    public IntegerProperty bikeIdProperty() {
        return bikeId;
    }

    public void setBikeId(int bikeId) {
        this.bikeId.set(bikeId);
    }

    public String getOrgName() {
        return orgName.get();
    }

    public StringProperty orgNameProperty() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName.set(orgName);
    }

    public int getPhoneNr() {
        return phoneNr.get();
    }

    public IntegerProperty phoneNrProperty() {
        return phoneNr;
    }

    public void setPhoneNr(int phoneNr) {
        this.phoneNr.set(phoneNr);
    }

    public String geteMail() {
        return eMail.get();
    }

    public StringProperty eMailProperty() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail.set(eMail);
    }

    public String getAdress() {
        return adress.get();
    }

    public StringProperty adressProperty() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress.set(adress);
    }

    public MaintenanceModel(int orgNr, int repOrder, double price, String documentation, LocalDate startDate, LocalDate endDate, int bikeId, WorkshopModel wsModel){
        this.orgNr = new SimpleIntegerProperty(orgNr);

        this.repOrder = new SimpleIntegerProperty(repOrder);
        this.price = new SimpleDoubleProperty(price);
        try{this.documentation = new SimpleStringProperty(documentation);}
        catch (NullPointerException e){this.documentation.set(null);}
        this.startDate = new SimpleObjectProperty<LocalDate>(startDate);
        try{ this.endDate = new SimpleObjectProperty<LocalDate>(endDate);}
        catch(NullPointerException e){this.endDate.set(null);}
        this.bikeId = new SimpleIntegerProperty(bikeId);

        this.orgName = new SimpleStringProperty(wsModel.getOrgName());
        this.phoneNr = new SimpleIntegerProperty(wsModel.getPhoneNr());
        this.eMail = new SimpleStringProperty(wsModel.geteMail());
        this.adress = new SimpleStringProperty(wsModel.getAdress());

    }
}
