package main.tests;

import main.java.data.Type;
import main.java.db.TypeDAO;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Class to test the TypeDAO class
 * @author Rune Vedøy
 */
public class TypeDAOTest {

    private TypeDAO typetest;

    // Sets up an connection to be used for the entire test and prepares the typeDAO object
    @Before
    public void setUp() throws SQLException {
        typetest = new TypeDAO();
    }

    /*------------------getType tests----------------*/

    // Testing basic getType
    @Test
    public void getTypeTest()throws SQLException{
        assertEquals(new Type("Adult"),typetest.getType("Adult"));
    }

    // Testing basic getAllTypes
    @Test
    public void getAllTypesTest()throws SQLException{
        ArrayList<Type> test = new ArrayList<>();
        Type t1 = new Type("Adult");
        Type t2 = new Type("Child");
        Type t3 = new Type("Lady");

        test.add(t1);
        test.add(t2);
        test.add(t3);

        assertEquals(test,typetest.getAllTypes());
    }

    /*------------------addType and delType test----------------*/

    //Testing basic addType and del
    @Test
    public void addDelTypeTest()throws SQLException{
        Type tadd = new Type("Tandem");
        assertTrue(typetest.addType(tadd));
        assertTrue(typetest.delType("Tandem"));
    }

    /*------------------updateType test----------------*/
    @Test
    public void updateTypeTest()throws SQLException{
        Type tadd = new Type("Tandem");
        typetest.addType(tadd);

        assertTrue(typetest.updateType("Tandem", "SuperRacer"));
        assertTrue(typetest.delType("SuperRacer"));
    }

}
