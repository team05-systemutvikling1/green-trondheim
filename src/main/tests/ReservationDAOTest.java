import main.java.data.Reservation;
import main.java.db.ReservationDAO;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Class to test the ReservationDAO class
 * @author Rune Vedøy
 */
public class ReservationDAOTest {
    ReservationDAO reservationTest;

    // Sets up an connection to be used for the entire test and prepares the tripDAO object
    @Before
    public void setUp() throws SQLException {
        reservationTest = new ReservationDAO();
    }

    /*------------------getReservation tests----------------*/

    //Testing basic getReservation
    @Test
    public void getReservationTest()throws SQLException{
        Reservation r = new Reservation("karihansen@hotmail.com",18,"Jernbanestasjonen",
                LocalDateTime.of(2018,Month.APRIL,9,21,00,00),
                LocalDateTime.of(2018,Month.APRIL,9,22,00,00));

        r.setReservationId(1);

        assertEquals(r,reservationTest.getReservation(1));
    }

    //Testing basic getAllReservations
    @Test
    public void getAllReservationsTest()throws SQLException{
        ArrayList<Reservation> rlist = new ArrayList<>();
        Reservation r = new Reservation("karihansen@hotmail.com",18,"Jernbanestasjonen",
                LocalDateTime.of(2018,Month.APRIL,9,21,00,00),
                LocalDateTime.of(2018,Month.APRIL,9,22,00,00));

        r.setReservationId(1);

        rlist.add(r);

        assertEquals(rlist,reservationTest.getAllReservations());
    }

    /*------------------add-, end- and delReservation tests----------------*/

    @Test
    public void addEditDelReservationTest()throws SQLException{
        Reservation r = new Reservation();
        r.setEmail("karihansen@hotmail.com");
        r.setTripId(18);
        r.setName("Pirbadet");
        r.setReservationId(2);
        r.setFromTime(Timestamp.valueOf("2018-04-21 18:00:00"));

        assertTrue(reservationTest.addReservation(r));

        assertTrue(reservationTest.endReservation(2,
                LocalDateTime.of(2018,Month.APRIL,21,19,00,00)));

        assertTrue(reservationTest.delReservation(2));
    }


}
