package main.tests;

import main.java.db.*;
import main.java.data.User;
import org.junit.Test;

import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class UserDAOTest {
    private String e_mail;

    //--------------------CONNECTION TESTS-------------------
    //Testing if there is a connection

    @Test
    public void dbToClient(){
        Db connection = Db.instance();
        assertNotNull(connection);
    }

    //------------------------getUser(String) TEST------------------------
    //Testing if base getUser function works
    @Test
    public void getUserTest()throws SQLException{
        UserDAO admin = new UserDAO();
        e_mail = "karihansen@hotmail.com";
        assertEquals(new User("karihansen@hotmail.com", 22119054321L, "Kari", "Hansen", 87654321), admin.getUser(e_mail));
    }

    //Testing if base getUser function works even with whitespace
    @Test
    public void getUserWhiteSpaceTest()throws SQLException{
        UserDAO admin = new UserDAO();
        e_mail = " karihansen@hotmail.com ";
        assertEquals(new User("karihansen@hotmail.com", 22119054321L, "Kari", "Hansen", 87654321), admin.getUser(e_mail));
    }
    //Testing if base getUser function works even with whitespace and is not case sensitivity
    @Test
    public void getUserWhiteSpacecaseSensitivityTest()throws SQLException{
        UserDAO admin = new UserDAO();
        e_mail = " KaRihAnSen@hotmail.com ";
        assertEquals(new User("karihansen@hotmail.com", 22119054321L, "Kari", "Hansen", 87654321), admin.getUser(e_mail));
    }

    //Testing if base getUser function works
    @Test
    public void getUserSQLInjectionTest()throws SQLException{
        UserDAO admin = new UserDAO();
        e_mail = "karihansen@hotmail.com); CREATE TABLE injectionserikkesikkret(you VARCHAR(11) PRIMARY KEY, done INTEGER, goofed BOOLEAN); ";
        assertNull(admin.getUser(e_mail));
    }

    //Testing if empty string gives null
    @Test
    public void getUserEmptTest()throws SQLException{
        UserDAO admin = new UserDAO();
        e_mail = "";

        assertNull(admin.getUser(e_mail));
    }

    //Testing if int might crash the system somewhere
    @Test
    public void getUserIntTest()throws SQLException{
        UserDAO admin = new UserDAO();
        e_mail = "1234";
        assertNull(admin.getUser(e_mail));
    }

    //Testing if æøå will crash the system
    @Test
    public void getUserNordernInputTest()throws SQLException{
        UserDAO admin = new UserDAO();
        e_mail = "æøå";

        assertNull(admin.getUser(e_mail));
    }

    //Remember to test if we can actually recognise a user with northern letters!!

    //Testing if any foreign input will crash the system
    @Test
    public void getUserForeignInputTest()throws SQLException{
        UserDAO admin = new UserDAO();
        e_mail = "私はケビンです.　これはもう楽します！";

        assertNull(admin.getUser(e_mail));
    }

    //-----------------------getAllUsers() TEST----------------------
    @Test
    public void getAllUsersTest()throws SQLException{
        UserDAO admin = new UserDAO();
        ArrayList<User> testList = new ArrayList<>();
        testList.add(new User("karihansen@hotmail.com",	22119054321L,"Kari", "Hansen",87654321));
        testList.add(new User("olanordmann@gmail.com", 24129712345L, "Ola",	"Nordmann",	12345678));

        assertEquals(testList, admin.getAllUsers());
    }

    //----------------------addAdmin(User) and delUser TEST---------------------
    //Basic test to see if the two functions work in the first place. Test the two functions separately
    @Test
    public void addUserTest()throws SQLException{
        UserDAO admin = new UserDAO();

        assertTrue(admin.addUser(new User("totally.nott@fake.net", 14048832846L, "Edward", "Snowden", 48609823)));
    }

    @Test
    public void delUserTest()throws SQLException{
        UserDAO admin = new UserDAO();

        assertTrue(admin.delUser("per.Olsen@live.no"));
    }

}
