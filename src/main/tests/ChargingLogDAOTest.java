package main.tests;

import main.java.data.ChargingLog;
import main.java.db.ChargingLogDAO;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

public class ChargingLogDAOTest {
    private ChargingLogDAO chargingLogDAO;
    @Before
    public void buildRecources()throws SQLException{
        chargingLogDAO = new ChargingLogDAO();
    }
    //public boolean addChargingLog(String name, int powerConsumption) throws SQLException


    //public ArrayList<ChargingLog> getChargingLog(String name) throws SQLException {
    @Test
    public void getChargeLogTest()throws SQLException{
        ArrayList<ChargingLog> chargingLogs = new ArrayList<>();
        //public ChargingLog(int powerConsumption, String name, LocalDateTime time)
        //2018-04-22 17:38:18
        chargingLogs.add(new ChargingLog(200, "Jernnanestasjonen", LocalDateTime.of(2018, Month.APRIL, 22, 17, 38, 18)));
        //Jernbanestasjonen	0	2018-04-22 17:38:24
        chargingLogs.add(new ChargingLog(0, "Jernbanestasjonen", LocalDateTime.of(2018, Month.APRIL, 22, 17, 38,24)));
        assertEquals(chargingLogs, chargingLogDAO.getChargingLog("Jernbanestasjonen"));
    }

    @Test
    public void setChargeLogTest()throws SQLException{
        assertTrue(chargingLogDAO.addChargingLog("Gløshaugen", 300));
    }
}
