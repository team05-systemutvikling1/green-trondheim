package main.tests;

import main.java.data.Workshop;
import main.java.db.WorkshopDAO;
import org.junit.Test;

import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class WorkshopDAOTest {


    //getWorkshop tests --->
    @Test
    public void getWorkshopTest()throws SQLException{
        //1	Bike Fixers	11223344	bike@fixers.com	Kongens gate 1
        WorkshopDAO wsAdmin = new WorkshopDAO();
        assertEquals(new Workshop(1, "Bike Fixers", 11223344, "bike@fixers.com", "Kongens gate 1"),
                wsAdmin.getWorkshop(1));
    }

    //testing the getWorkshop method with a netative input
    @Test
    public void getWSNegIntTest() throws SQLException{
        WorkshopDAO wsAdmin = new WorkshopDAO();

        assertNull(wsAdmin.getWorkshop(-16));
    }

    //test if getWorkshop method with a 0 value
    @Test
    public void getWSzeroTest() throws SQLException{
        WorkshopDAO wsAdmin = new WorkshopDAO();

        assertNull(wsAdmin.getWorkshop(0));

    }

    //getAllWorkshop test --->
    @Test
    public void getAllWorkshopTest()throws SQLException{
        WorkshopDAO wsAdmin = new WorkshopDAO();
        ArrayList<Workshop> workshops = new ArrayList<>();
        //1	Bike Fixers	11223344	bike@fixers.com	Kongens gate 1
        //2	Bicycle Repairs	55667788	bicycle@repairs.com	Prinsens gate 2
        workshops.add(new Workshop(1, "Bike Fixers", 11223344, "bike@fixers.com", "Kongens gate 1"));
        workshops.add(new Workshop(2, "Bicycle Repairs", 55667788, "bicycle@repairs.com", "Prinsens gate 2"));

        assertEquals(workshops, wsAdmin.getAllWorkshops());
    }

    // add and delete method test --->
    @Test
    public void addDelTest()throws SQLException{

        WorkshopDAO wsAdmin = new WorkshopDAO();
        assertTrue(wsAdmin.addWorkshop(new Workshop(4, "Fixit", 9632147, "order@fixit.pl", "cechian st. 65")));

        assertEquals(new Workshop(4, "Fixit", 9632147, "order@fixit.pl", "cechian st. 65"),
                wsAdmin.getWorkshop(4));

        assertTrue(wsAdmin.delWorkshop(4));

    }

    //Testing injections for the add method --->
    @Test
    public void addDelInjectionTest() throws SQLException{

        WorkshopDAO wsAdmin = new WorkshopDAO();
        assertTrue(wsAdmin.addWorkshop(new Workshop(4,
                "); CREATE TABLE injection();",
                9632147,
                "); CREATE TABLE injection();",
                "); CREATE TABLE injection();")));

        assertEquals(new Workshop(4,
                "); CREATE TABLE injection();",
                9632147,
                "); CREATE TABLE injection();",
                "); CREATE TABLE injection();"),
                wsAdmin.getWorkshop(4));

        assertTrue(wsAdmin.delWorkshop(4));
    }

    //Test for deleting unwanted rows. Not usually used.
    //D@Test
    public void delCleanup() throws SQLException{
        WorkshopDAO wsAdmin = new WorkshopDAO();
        assertTrue(wsAdmin.delWorkshop(-10));
        assertTrue(wsAdmin.delWorkshop(0));
        assertTrue(wsAdmin.delWorkshop(8));
        assertTrue(wsAdmin.delWorkshop(22));
    }

}
