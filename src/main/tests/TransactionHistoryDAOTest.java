package main.tests;

import main.java.data.TransactionHistory;
import main.java.db.TransactionHistoryDAO;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static junit.framework.TestCase.assertNull;

public class TransactionHistoryDAOTest {

    private TransactionHistoryDAO transactionHistoryTest;

    @Before
    public void setUp() throws SQLException {
        transactionHistoryTest = new TransactionHistoryDAO();
    }



    @Test
    public void getTransactionHistoryTest() throws SQLException {
        TransactionHistory tH = new TransactionHistory(22119054321L, LocalDateTime.of(LocalDate.of(2018,4,11),LocalTime.of(10,36)), 42.54);
        tH.setTransactionNr(1);
        assertEquals(tH, transactionHistoryTest.getTransactionHistory(1));
    }

    @Test
    public void getTHNegIntTest() throws SQLException {
        assertNull(transactionHistoryTest.getTransactionHistory(-5));
    }

    @Test
    public void getTHZeroTest() throws SQLException {
        assertNull(transactionHistoryTest.getTransactionHistory(0));
    }

    @Test
    public void getAllTransactionHistories() throws SQLException {
        ArrayList<TransactionHistory> transactionHistories = new ArrayList<>();
        transactionHistories.add(new TransactionHistory(22119094329L, LocalDateTime.of(LocalDate.of(2018,4,11),LocalTime.of(10,36)), 50.54));
        transactionHistories.add(new TransactionHistory(25419094329L, LocalDateTime.of(LocalDate.of(2018,5,12),LocalTime.of(11,50)), 42.00));

        assertEquals(transactionHistories, transactionHistoryTest.getAllTransactionHistory());
    }

    @Test
    public void addDelTest() throws SQLException {
        assertTrue(transactionHistoryTest.addTransactionHistory(new TransactionHistory(353453453L, LocalDateTime.of(LocalDate.of(2018,4,11),LocalTime.of(10,50)), 55.55)));
        assertEquals(new TransactionHistory(353453453L, LocalDateTime.of(LocalDate.of(2018,4,11),LocalTime.of(10,50)), 55.55), transactionHistoryTest.getTransactionHistory(4));
        assertTrue(transactionHistoryTest.delTransactionHistory(4));
    }

    @Test
    public void delCleanUp() throws SQLException {
        assertTrue(transactionHistoryTest.delTransactionHistory(1));
        assertTrue(transactionHistoryTest.delTransactionHistory(-5));
        assertTrue(transactionHistoryTest.delTransactionHistory(2));
        assertTrue(transactionHistoryTest.delTransactionHistory(3));
        assertTrue(transactionHistoryTest.delTransactionHistory(4));
    }
}
