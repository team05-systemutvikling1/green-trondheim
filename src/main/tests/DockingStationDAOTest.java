package main.tests;

import main.java.data.DockingStation;
import main.java.db.DockingStationDAO;
import main.java.data.Bike;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
/**
 * Class to test the DockingStationDAO class
 * @author Rune Vedøy
 */
public class DockingStationDAOTest {
    private DockingStationDAO dockingTest;

    // Setting up the docking station DAO before each test
    @Before
    public void setUp()throws SQLException {
        dockingTest = new DockingStationDAO();
    }

    /*------------------get docking tests----------------
    * Testing if the basic getDockingStation works
    */
    @Test
    public void getDockingStationTest()throws SQLException{
        assertEquals(new DockingStation("Jernbanestasjonen","Toggata",0,0,200),
                dockingTest.getDockingStation("Jernbanestasjonen"));
    }

    // Testing if getDockingStation works with whitespace
    @Test
    public void getDockingStationWhiteSpaceTest()throws SQLException{
        assertEquals(new DockingStation(" Jernbanestasjonen","Toggata",0,0,200),
                dockingTest.getDockingStation("Jernbanestasjonen"));
    }


    //Testing basic getAllDockingStations
    @Test
    public void getAllDockingStationsTest()throws SQLException{
        ArrayList<DockingStation> dockingStations = new ArrayList<>();
        dockingStations.add(new DockingStation("Jernbanestasjonen","Toggata",0,0,200));
        dockingStations.add(new DockingStation("Kalvskinnet", "Kalvskinnet",0,0,100));

        assertEquals(dockingStations,dockingTest.getAllDockingStations());
    }

    /* Testing basic getDockingBikes
    @Test
    public void getDockingBikesTest()throws SQLException{
        ArrayList<Bike> bikeArrayList = new ArrayList<>();
        Bike bike1 = new Bike("Jernbanestasjonen",false,"Adult",4000,
                LocalDate.of(2018,03,15));
        Bike bike2 = new Bike("Jernbanestasjonen",false,"Adult",4000,
                LocalDate.of(2018,03,13));
        Bike bike3 = new Bike("Jernbanestasjonen",false,"Adult",4000,
                LocalDate.of(2018,03,14));
        Bike bike4 = new Bike("Jernbanestasjonen",false,"Children",4000,
                LocalDate.of(2018,01,01));
        Bike bike5 = new Bike("Jernbanestasjonen",false,"Adult",4000,
                LocalDate.of(2018,03,12));

        bike1.setBikeId(1);
        bike2.setBikeId(2);
        bike3.setBikeId(3);
        bike4.setBikeId(5);
        bike5.setBikeId(6);

        bikeArrayList.add(bike1);
        bikeArrayList.add(bike2);
        bikeArrayList.add(bike3);
        bikeArrayList.add(bike4);
        bikeArrayList.add(bike5);

        assertEquals(bikeArrayList,dockingTest.getDockingBikes("Jernbanestasjonen"));
    }

    /*------------------add and delete docking tests----------------
     * Testing if the basic addDockingStation works
     */

    // Testing the basic addDockingStation
    @Test
    public void addDockingStationTest() throws SQLException{
        DockingStation dockingStation = new DockingStation("Sykehus","Sykehusgata",0,0, 100);
        assertTrue(dockingTest.addDockingStation(dockingStation));
        assertTrue(dockingTest.delDockingStation("Sykehus"));
    }

    /*------------------Update tests----------------
     * Testing if the basic UpdateDockingStation works
     */
    @Test
    public void updateDockingStationTest() throws SQLException{
        DockingStation dockingStation = new DockingStation("Sykehus","Sykehusgata",0,0, 100);
        dockingTest.addDockingStation(dockingStation);
        DockingStation dockingStation2 = new DockingStation("Gløshaugen","Sykehusgata",0,0,300);

        assertTrue(dockingTest.updateDockingStation("Sykehus", dockingStation2));
    }

}
