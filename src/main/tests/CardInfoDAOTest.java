package main.tests;


import main.java.data.CardInfo;
import main.java.db.CardInfoDAO;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.util.ArrayList;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static junit.framework.TestCase.assertNull;

/**
 * Class to test CardInfoDAO class
 *
 * @author Jan-Marius
 */
public class CardInfoDAOTest {
    private CardInfoDAO cardInfoTest;

    @Before
    public void setUp() throws SQLException {
        cardInfoTest = new CardInfoDAO();
    }

    /**
     * Testing the method getCardInfo()
     *
     * @throws SQLException
     */
    @Test
    public void getCardInfoTest() throws SQLException {
        assertEquals(new CardInfo(
                "Ola",
                "Nordmann",
                24129712345L,
                234L,
                234234),
                cardInfoTest.getCardInfo(24129712345L));
    }

    /**
     * Testing the method getAllCardInfo()
     *
     * @throws SQLException
     */
    @Test
    public void getAllCardInfo() throws SQLException {
        ArrayList<CardInfo> cardInfos = new ArrayList<>();

        cardInfos.add(new CardInfo(
                "Per",
                "Olsen",
                15106067890L,
                12341234123412344L,
                12));

        cardInfos.add(new CardInfo(
                "Kari",
                "Hansen",
                22119054321L,
                23L,
                23234));

        cardInfos.add(new CardInfo(
                "Ola",
                "Nordmann",
                24129712345L,
                234L,
                234234));

        assertEquals(cardInfos,cardInfoTest.getAllCardInfo());
    }

    /**
     * Testing addCardInfo and delCardInfo()
     *
     * @throws SQLException
     */
    @Test
    public void addDelTest() throws SQLException {

        assertTrue(cardInfoTest.addCardInfo(new CardInfo(
                "Petter",
                "Brattveg",
                121212L,
                444L,
                2020)));

        assertEquals(new CardInfo(
                "Petter",
                "Brattveg",
                121212L,
                444L,
                2020),
                cardInfoTest.getCardInfo(121212L));

        assertTrue(cardInfoTest.delCardInfo(121212L));

    }

    /**
     * Testing if non existing object returns null in method getCardInfo()
     *
     * @throws SQLException
     */
    @Test
    public void getCIzeroTest() throws SQLException {
        assertNull(cardInfoTest.getCardInfo(0L));
    }


}
