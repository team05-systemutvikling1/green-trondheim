

package main.tests;

import main.java.data.Trip;
import main.java.db.Db;
import main.java.db.TripDAO;
import org.junit.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Class to test the TripDAO class
 * @author Kevin
 */
public class TripDAOTest {
    private  TripDAO tripTest;
    /**
     * A method to get the AUTO_INCREMENT value
     *
     * @return the next AUTO_INCREMENT value to be added
     * @throws SQLException if the query fails
     */
    // Sets up an connection to be used for the entire test and prepares the tripDAO object
    @Before
    public void setUp() throws SQLException{
        tripTest = new TripDAO();
    }

    /**
     * A method to get the AUTO_INCREMENT value
     *
     * @return the next AUTO_INCREMENT value to be added
     * @throws SQLException if the query fails
     */
    // Retrieves the highest trip_id and assigns it +1 when a new trip is added.
    private int autoID() throws SQLException {
        Connection connection;
        ResultSet rs = null;
        PreparedStatement ps = null;
        try{
            connection = Db.instance().getConnection();
            ps = connection.prepareStatement("SELECT AUTO_INCREMENT FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'kevinah' AND TABLE_NAME = 'trip'");
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }

            return -1;
        }finally{
            try{ps.close();}catch(NullPointerException e){}
            try{rs.close();}catch(NullPointerException e){}
        }
    }


    /**------------------getTrip test----------------
     /**
     * Testing if the basic getTrip works with no weird input
     * @throws SQLException If a connection issue should occur
     */
    @Test
    public void getTripTest()throws SQLException{
        assertEquals(new Trip(
                1,
                "karihansen@hotmail.com",
                1,
                0,
                LocalDateTime.of(2018, Month.FEBRUARY, 9, 16, 3, 39),
                LocalDateTime.of(2018, Month.FEBRUARY, 9, 16, 26, 16)), tripTest.getTrip(1));

    }

    //Testing if getTrip works even with octal integers, such as 01 instead of 1
    @Test
    public void getTripTestOctalInteger() throws SQLException{
        assertEquals(new Trip(1,"karihansen@hotmail.com",1,0,
                        LocalDateTime.of(2018, Month.FEBRUARY, 9, 16, 3, 39),
                        LocalDateTime.of(2018, Month.FEBRUARY, 9, 16, 26, 16)),
                tripTest.getTrip( 001));
    }

    //Testing if getTrip works when an entry in the database has whitespace in the email
    @Test
    public void getTripWhiteSpace() throws SQLException{
        assertEquals(new Trip(4," olanordmann@gmail.com",5,0,
                        LocalDateTime.of(2018, Month.FEBRUARY, 9, 16, 3, 39),
                        LocalDateTime.of(2018, Month.FEBRUARY, 9, 16, 26, 16)),
                tripTest.getTrip( 4));
    }

    //Testing if getTrip works when an entry in the database has mixed upper- and lowercase letters
    @Test
    public void getTripCaseSensitive() throws SQLException{
        assertEquals(new Trip(4,"oLanordmann@gmail.com",5,0,
                        LocalDateTime.of(2018, Month.FEBRUARY, 9, 16, 3, 39),
                        LocalDateTime.of(2018, Month.FEBRUARY, 9, 16, 26, 16)),
                tripTest.getTrip( 4));
    }


    /**------------------getAllTrip tests----------------
     * Testing basic add and delete
     */

    @Test
    public void getAllTrips() throws SQLException {
        ArrayList<Trip> trips = new ArrayList<>();
        // karihansen@hotmail.com	1		2018-04-02 10:47:12
        trips.add(new Trip(
                1,
                "karihansen@hotmail.com",
                1,
                0,
                LocalDateTime.of(2018, Month.FEBRUARY, 9, 16, 3, 39),
                LocalDateTime.of(2018, Month.FEBRUARY, 9, 16, 26, 16)));

        //2	olanordmann@gmail.com	3	1.2	2018-02-09 16:03:39	2018-02-09 16:26:16
        trips.add(new Trip(
                2,
                "olanordmann@gmail.com",
                3,
                1.2,
                LocalDateTime.of(2018, Month.FEBRUARY, 9, 16, 3, 39),
                LocalDateTime.of(2018, Month.FEBRUARY, 9, 16, 26, 16)));

        //3	karihansen@hotmail.com	2		2018-02-09 16:03:39
        trips.add(new Trip(
                3,
                "karihansen@hotmail.com",
                2,
                0,
                LocalDateTime.of(2018, Month.FEBRUARY, 9, 16, 3, 39),
                LocalDateTime.of(2018, Month.FEBRUARY, 9, 16, 15, 39)));

        assertEquals(trips, tripTest.getAllTrips());
    }

    /**------------------addTrip and delete test----------------
     * Testing basic add and delete
     */
    //int trip_id, String e_mail, int bike_id, double length, LocalDateTime timeStart, LocalDateTime timeEnd
    @Test
    public void addDelTripTest() throws SQLException {
        int autoID = autoID();

        assertTrue(tripTest.addTrip(new Trip(autoID,
                "olanordmann@gmail.com",
                1,
                0,
                LocalDateTime.of(2018, Month.APRIL, 1, 15, 8, 21),
                null)));

        assertEquals(new Trip(autoID,
                "olanordmann@gmail.com",
                1,
                0,
                LocalDateTime.of(2018, Month.APRIL, 1, 15, 8, 21),
                null), tripTest.getTrip(autoID));

        assertTrue(tripTest.delTrip(autoID));
    }

    // Testing addTrip() with norwegian special letters, then delete it

    @Test
    public void addDelTripTestNO() throws SQLException{
        int autoID = autoID();

        assertTrue(tripTest.addTrip(new Trip(autoID,
                "olanørdmann@gmail.com",
                5,
                0,
                LocalDateTime.of(2018, Month.APRIL, 1, 15, 8, 21),
                null)));

        assertEquals(new Trip(autoID,
                "olanørdmann@gmail.com",
                5,
                0,
                LocalDateTime.of(2018, Month.APRIL, 1, 15, 8, 21),
                null), tripTest.getTrip(autoID));

        assertTrue(tripTest.delTrip(autoID));

    }

}
