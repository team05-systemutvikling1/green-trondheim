package main.tests;
import main.java.data.Admin;
import main.java.db.AdminDAO;
import main.java.util.HashingUtil;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Class to test the TypeDAO class
 * @author Rune Vedøy
 */
public class AdminDAOTest {
    AdminDAO adminTest;

    // Sets up an connection to be used for the entire test and prepares the AdminDAO object
    @Before
    public void setUp()throws SQLException{
        adminTest = new AdminDAO();
    }

    /*------------------getAdmin tests----------------*/

    // Testing basic getAdmin
    @Test
    public void getAdminTest()throws SQLException{
        assertEquals(readPropertiesFile("login.properties","username","password","salt"),adminTest.getAdmin("admin"));
    }

    // Testing basic getAllAdmins
    @Test
    public void getAllAdminsTest()throws SQLException{
        ArrayList<Admin> a = new ArrayList<>();
        a.add(readPropertiesFile("login.properties","username","password","salt"));
        a.add(readPropertiesFile("login.properties","username1","password1","salt1"));
        a.add(readPropertiesFile("login.properties","username2","password2","salt2"));

        assertEquals(a,adminTest.getAllAdmins());
    }

    /*------------------addAdmin tests----------------*/
    // Testing basic addAdmin users
    @Test
    public void addAdminTest()throws SQLException{
        assertTrue(adminTest.addAdmin("r.vedoy@gmail.com"));
    }

    // Testing addAdminWithoutMail
    @Test
    public void addAdminWithoutMailTest()throws SQLException{
        assertTrue(adminTest.addAdminWithoutMail(new Admin("r.vedoy","123")));
        assertTrue(adminTest.delAdmin("r.vedoy"));
    }

    /*------------------delAdmin tests----------------*/
    // Testing basic delAdmin
    @Test
    public void delAdminTest()throws SQLException{
        assertTrue(adminTest.delAdmin("r.vedoy@gmail.com"));
    }

    /*------------------Password tests----------------*/

    // Testing setting a new password
    @Test
    public void setPasswordTest()throws SQLException{
        adminTest.addAdminWithoutMail(new Admin("r.vedoy","123"));
        assertTrue(adminTest.setPassword("r.vedoy","1234"));
    }

    // Testing isPasswordCorrect()
    @Test
    public void isPasswordCorrectTest()throws SQLException{
        assertTrue(adminTest.isPasswordCorrect("r.vedoy","1234"));
        adminTest.delAdmin("r.vedoy");
    }

    /*------------------countAdmin test----------------*/
    @Test
    public void countAdminsTest()throws SQLException{
        int nr = 6;
        assertEquals(nr,adminTest.countAdmins());
    }

    private static Admin readPropertiesFile(String filename, String user, String passw,String s){
        Properties prop = new Properties();
        InputStream input = null;

        try {
            input = new FileInputStream(filename);
            // Load a properties file
            prop.load(input);

            // Reading from the .properties file
            String username = prop.getProperty(user);
            String password = prop.getProperty(passw);
            String salt = prop.getProperty(s);

            // Hasing values read
            HashingUtil hash = new HashingUtil();
            String hashed = hash.getHash(password,salt);

            // Creating returnable admin object
            Admin a = new Admin(username,hashed,salt);

            return a;

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return new Admin();
    }
}
