package main.tests;

import main.java.data.BikeLog;
import main.java.data.Position;
import main.java.db.BikeLogDAO;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BikeLogDAOTest {
    private BikeLogDAO bikeTest;

    /**
     * Class to test the BikeLogDAO class
     * @author Rune Vedøy
     */
    @Before
    public void setUp()throws SQLException {
        bikeTest = new BikeLogDAO();
    }

    /*------------------getBikeLog test----------------*/
    @Test
    public void getBikeLogTest()throws SQLException{
        ArrayList<BikeLog> barray= new ArrayList<>();

        BikeLog bl1 = new BikeLog(6,100,50.123456,50.654321,
                LocalDateTime.of(2018,Month.APRIL,18,14,55,05));
        BikeLog bl2 = new BikeLog(6,70,60.123456,60.654321,
                LocalDateTime.of(2018,Month.APRIL,18,14,55,05));
        BikeLog bl3 = new BikeLog(7,79,61.123456,51.123456,
                LocalDateTime.of(2018,Month.APRIL,19,22,9,01));
        BikeLog bl4 = new BikeLog(7,78,62.123456,52.123456,
                LocalDateTime.of(2018,Month.APRIL,19,22,10,01));

        bl1.setLogId(1);
        bl2.setLogId(2);

        barray.add(bl1);
        barray.add(bl2);
        assertEquals(barray,bikeTest.getBikeLog(6));
    }
    /*------------------addBikeLog test----------------*/
    @Test
    public void addBikeLogTest()throws SQLException{
        Position p = new Position(64.123456,54.123456);
        assertTrue(bikeTest.addBikeLog(1,100,p));
    }

}
