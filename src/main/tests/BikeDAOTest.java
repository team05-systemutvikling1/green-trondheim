package main.tests;

import main.java.data.Bike;
import main.java.data.Position;
import main.java.db.BikeDAO;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Class to test the BikeDAO class
 * @author Rune Vedøy
 */
public class BikeDAOTest {
    private BikeDAO bikeTest;

    // Sets up an connection to be used for the entire test and prepares the BikeDAO object
    @Before
    public void setUp() throws SQLException{
        bikeTest = new BikeDAO();
    }

    /**------------------getBike and getAllBikes test----------------
     */
    //Testing basic get method
    @Test
    public void getBikeTest()throws SQLException{
        Bike bike = new Bike("Jernbanestasjonen", false,"Adult",10000,
                LocalDate.of(2018,04,10),100,56.023233,45.959595,"DBS");

        bike.setBikeId(1);
        assertEquals(bike,
                bikeTest.getBike(1));
    }

    @Test
    public void getAllBikesTest()throws SQLException{
        ArrayList<Bike> bikeArrayList = new ArrayList<>();
        Bike bike1 = new Bike("Jernbanestasjonen", false,"Adult",10000,
                LocalDate.of(2018,04,10),100,56.023233,45.959595,"DBS");

        Bike bike2 = new Bike("Pirbadet", false,"Adult",10000,
                LocalDate.of(2018,04,10),100,0,0,"DBS");

        bike1.setBikeId(1);
        bike2.setBikeId(2);

        bikeArrayList.add(bike1);
        bikeArrayList.add(bike2);

        assertEquals(bikeArrayList,bikeTest.getAllBikes());
    }

    // Testing basic getAllBrokenBikes()
    @Test
    public void getAllBrokenBikesTest()throws SQLException{
        ArrayList<Bike> brokenBikes = new ArrayList<>();

        Bike bike1 = new Bike("Pirbadet", true,"Adult",7000,
                LocalDate.of(2018,04,10),0,0,0,"DBS");

        Bike bike2 = new Bike("Pirbadet", true,"Adult",6000,
                LocalDate.of(2018,01,07),0,0,0,"DBS");

        bike1.setBikeId(3);
        bike2.setBikeId(11);

        brokenBikes.add(bike1);
        brokenBikes.add(bike2);

        assertEquals(brokenBikes,bikeTest.getAllBrokenBikes());
    }

    // Testing getAmountBikesCharging
    @Test
    public void getAmountBikesChargingTest()throws SQLException{
        int amount = 7;
        assertEquals(amount,bikeTest.getAmountBikesCharging("Jernbanestasjonen"));
    }

    // Testing getName = getting the name of a docking from a bikeId
    @Test
    public void getNameTest()throws SQLException{
        String name = "Jernbanestasjonen";
        assertEquals(name,bikeTest.getName(22));
    }

    // Testing getCharge()
    @Test
    public void getChargeTest()throws SQLException{
        int charge = 80;
        assertEquals(charge,bikeTest.getCharge(7));
    }

    // Testing getPosition()
    @Test
    public void getPositionTest()throws SQLException{
        Position p = new Position(56.023233,45.959595);
        assertEquals(p,bikeTest.getPosition(1));
    }

    /**------------------addbike() and delBike() test----------------
     */
    @Test
    public void addBikeTest()throws SQLException{
        Bike testBike = new Bike("Pirbadet", false,"Child", 5000,
                LocalDate.of(2018,04,19),0,65.92929,49.093939,"DBS");

        assertTrue(bikeTest.addBike(testBike));
    }

    @Test
    public void delBikeTest()throws SQLException{
        assertTrue(bikeTest.delBike(23));
    }

    @Test
    public void updateBikeTest()throws SQLException{
        Bike testBike = new Bike("Pirbadet", false,"Child", 5000,
                LocalDate.of(2018,04,19),0,65.92929,49.093939,"DBS");

        bikeTest.addBike(testBike);

        Bike updateBike = new Bike("Pirbadet", false,"Lady", 12000,
                LocalDate.of(2018,04,18),0,65.92929,49.093939,"DBS");

        assertTrue(bikeTest.updateBike(23,updateBike));

    }

    /**------------------set methods test----------------
     */

    // Testing setName, changing name of docking station for bike
    @Test
    public void setNameTest()throws SQLException{
        assertTrue(bikeTest.setName(22,"Pirbadet"));
    }

    // Testing setCharge
    @Test
    public void setChargeTest()throws SQLException{
        assertTrue(bikeTest.setCharge(2,100));
    }

    // Testing setPosition()
    @Test
    public void setPositionTest()throws SQLException{
        assertTrue(bikeTest.setPosition(7,new Position(56.023233,45.959595)));
    }


}
