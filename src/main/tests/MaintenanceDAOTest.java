package main.tests;

import main.java.data.Maintenance;
import main.java.db.MaintenanceDAO;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

/**
 * Class to test the MaintenanceDAO
 *
 * @author Kevin
 */
public class MaintenanceDAOTest {
    private MaintenanceDAO maint;

    @Before
    public void instanceCreation() throws SQLException{
        maint = new MaintenanceDAO();

    }

    @Test
    public void getMaintenanceTest() throws SQLException{
        //int repOrder, int orgNr, int bikeId, double price, String doc, LocalDate startDate, LocalDate endDate
        //6	2	2	599.9	Replaced tire, and checked battery. Battery was Ok, and will probably last for another few montha.	2018-04-02	2018-04-09
        assertEquals(new Maintenance(6,
                2,
                2,
                599.9,
                "Replaced tire, and checked battery. Battery was Ok, and will probably last for another few montha.",
                LocalDate.of(2018, Month.APRIL, 2),
                LocalDate.of(2018, Month.APRIL, 9)), maint.getMaintenance(6));
    }

    @Test
    public void getAllMaintenanceTest()throws SQLException{
        ArrayList<Maintenance> maintenances = new ArrayList<>();
        /*
        2	1	1	289.54	Repainting after vandalization. Now comes in marine blue.	2018-04-01	2018-04-03
        6	2	2	599.9	Replaced tire, and checked battery. Battery was Ok, and will probably last for another few montha.	2018-04-02	2018-04-09
        11	2	2	799.9	Replaced battery, waiting for new order. Battery should arrive 13.04.2018. Awaiting order until then.	2018-04-10
        16	1	3	299.9

         */
        maintenances.add(new Maintenance(2,
                1,
                1,
                289.54,
                "Repainting after vandalization. Now comes in marine blue.",
                LocalDate.of(2018, Month.APRIL, 1),
                LocalDate.of(2018, Month.APRIL, 3)));

        maintenances.add(new Maintenance(6,
                2,
                2,
                599.9,
                "Replaced tire, and checked battery. Battery was Ok, and will probably last for another few montha.",
                LocalDate.of(2018, Month.APRIL, 2),
                LocalDate.of(2018, Month.APRIL, 9)));

        maintenances.add(new Maintenance(11,
                2,
                2,
                799.9,
                "Replaced battery, waiting for new order. Battery should arrive 13.04.2018. Awaiting order until then.",
                LocalDate.of(2018, Month.APRIL, 10),
                null));

        maintenances.add(new Maintenance(16, 1, 3, 299.9));

        assertEquals(maintenances, maint.getAllMaintenances());

    }

    @Test
    public void getAllNotStartedTest() throws SQLException{
        ArrayList<Maintenance> maintenances = new ArrayList<>();
        /*
        2	1	1	289.54	Repainting after vandalization. Now comes in marine blue.	2018-04-01	2018-04-03
        6	2	2	599.9	Replaced tire, and checked battery. Battery was Ok, and will probably last for another few montha.	2018-04-02	2018-04-09
        11	2	2	799.9	Replaced battery, waiting for new order. Battery should arrive 13.04.2018. Awaiting order until then.	2018-04-10
        16	1	3	299.9
         */

        maintenances.add(new Maintenance(16, 1, 3, 299.9));

        assertEquals(maintenances, maint.getAllNotStarted());

    }

    @Test
    public void getAllStarted()throws SQLException{
        ArrayList<Maintenance> maintenances = new ArrayList<>();
        /*
        2	1	1	289.54	Repainting after vandalization. Now comes in marine blue.	2018-04-01	2018-04-03
        6	2	2	599.9	Replaced tire, and checked battery. Battery was Ok, and will probably last for another few montha.	2018-04-02	2018-04-09
        11	2	2	799.9	Replaced battery, waiting for new order. Battery should arrive 13.04.2018. Awaiting order until then.	2018-04-10
        16	1	3	299.9

         */

        maintenances.add(new Maintenance(11,
                2,
                2,
                799.9,
                "Replaced battery, waiting for new order. Battery should arrive 13.04.2018. Awaiting order until then.",
                LocalDate.of(2018, Month.APRIL, 10),
                null));


        assertEquals(maintenances, maint.getAllStarted());

    }

    @Test
    public void getAllFinishedTest()throws SQLException{
        ArrayList<Maintenance> maintenances = new ArrayList<>();
        /*
        2	1	1	289.54	Repainting after vandalization. Now comes in marine blue.	2018-04-01	2018-04-03
        6	2	2	599.9	Replaced tire, and checked battery. Battery was Ok, and will probably last for another few montha.	2018-04-02	2018-04-09
        11	2	2	799.9	Replaced battery, waiting for new order. Battery should arrive 13.04.2018. Awaiting order until then.	2018-04-10
        16	1	3	299.9

         */
        maintenances.add(new Maintenance(2,
                1,
                1,
                289.54,
                "Repainting after vandalization. Now comes in marine blue.",
                LocalDate.of(2018, Month.APRIL, 1),
                LocalDate.of(2018, Month.APRIL, 3)));

        maintenances.add(new Maintenance(6,
                2,
                2,
                599.9,
                "Replaced tire, and checked battery. Battery was Ok, and will probably last for another few montha.",
                LocalDate.of(2018, Month.APRIL, 2),
                LocalDate.of(2018, Month.APRIL, 9)));

        assertEquals(maintenances, maint.getAllFinished());

    }

    @Test
    public void addDelMaintenanceTest() throws SQLException{
        assertTrue(maint.addMaintenance(new Maintenance(18,
                2,
                2,
                599.9,
                "Replaced tire, and checked battery. Battery was Ok, and will probably last for another few montha.",
                LocalDate.of(2018, Month.APRIL, 2),
                LocalDate.of(2018, Month.APRIL, 9))));

        assertEquals(new Maintenance(18,
                2,
                2,
                599.9,
                "Replaced tire, and checked battery. Battery was Ok, and will probably last for another few montha.",
                LocalDate.of(2018, Month.APRIL, 2),
                LocalDate.of(2018, Month.APRIL, 9)), maint.getMaintenance(6));

        assertTrue(maint.delMaintenance(18));

    }

    //D@Test
    public void delCleanup() throws SQLException{
        assertTrue(maint.delMaintenance(18));
    }
}
